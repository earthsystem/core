<?php

return [
    'recursive' => 'Recursive',
    'path' => 'Path',
    'edit_template' => 'Edit template',

    'delete' => 'Delete the template ":name"?',
    'deleted' => 'Template ":name" was deleted.',
    'not_deleted' => 'Template ":name" was NOT deleted.',
    
    'template_stored' => 'Template has been created.',
    'error_storing_template' => 'Error creating the template.',

    'error_updating_template' => 'Error updating the template.',
    'template_updated' => 'Template has been updated.',

    'add_existing' => 'Add existing template',
    'template_not_added_to_element' => 'Template NOT added to the element.',
    'template_added_to_element' => 'Template successfully added to the element.',

    'error_sorting_template' => 'Error sorting the template.',
    'template_sorted' => 'Template sorted.',
];