<?php

return [

    'submit' => 'Submit',
    'this_field_is_required' => 'This field is required.',

    'name' => 'Name',
    'email' => 'Email',
    'title' => 'Title',
    'slug' => 'Slug',
    'locale' => 'Translation',
    'active' => 'Active',
    'create' => 'Create',
    'cancel' => 'Cancel',
    'save' => 'Save',

];