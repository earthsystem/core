<?php

return [

    'no_name' => '(no name)',

    'switch_user' => 'Switch user',
    'change_password' => 'Change password',
    'pick_a_user' => 'Pick a user',
    'error_switching_user' => 'Error switching user.',
    'user_not_found' => 'User was not found in database.',
    'user_switched' => 'User successfully switched. Redirecting now.',
    'new_password' => 'New password',
    'confirm_new_password' => 'Confirm new password',
    'error_changing_password' => 'Error changing password.',
    'passwords_not_matching' => 'Passwords do not match. Try again.',
    'password_changed' => 'Password was changed. Closing popup now.',

    'title' => 'Users',
    'create' => 'Create a new user',

    'full_name' => 'Full name',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'email' => 'Email',
    'username' => 'Username',
    'type' => 'Type',
    'password' => 'Password',
    'pick_a_type' => 'Pick a type',

    'error_storing_user' => 'Error creating the user.',
    'user_stored' => 'User has been created.',

    'error_updating_user' => 'Error updating the user.',
    'user_updated' => 'User has been updated.',

    'delete' => 'Delete user ":name"?',
    'deleted' => 'User ":name" was deleted.',
    'not_deleted' => 'User ":name" was NOT deleted.',

    'add_role' => 'Add role',
    'user_not_added_to_role' => 'User NOT added to the role.',
    'user_added_to_role' => 'User successfully added to the role.',
    'delete_role' => 'Delete role',

    'delete_role_title' => 'Delete role ":role" from user ":name"?',
    'deleted_role' => 'Role ":role" was deleted from the user ":name.',
    'not_deleted_role' => 'Role ":role" was NOT deleted from the user ":name.',
];