<?php

return [

    'browse' => 'Browse',
    'clear_queue' => 'Clear queue',
    'refresh_list' => 'Refresh list',
    'drop_files_here' => 'Drop files here.',

    'delete' => 'Delete attachment ":title"?',
    'deleted' => 'Attachment ":title" was deleted.',
    'not_deleted' => 'Attachment ":title" was NOT deleted.',

    'attachment_stored' => 'Attachment has been stored.',
    'error_storing_attachment' => 'There was an error storing the attachment.',

    'error_sorting_attachment' => 'Error sorting the attachment.',
    'attachment_sorted' => 'Attachment sorted.',

    'upload_a_file' => 'Upload a file',
    'upload' => 'Upload',
    'file_not_uploaded' => 'File not uploaded.',
    'file_uploaded' => 'File uploaded.',

];