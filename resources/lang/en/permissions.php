<?php

return [

    'permissions_updated' => 'Permissions have been updated.',
    'error_updating_permissions' => 'Error updating permissions.',

];