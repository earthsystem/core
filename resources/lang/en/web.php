<?php

return [
    'error_title' => 'Oops!',
    'error_desc' => 'There was an error processing this page.',
    'gallery' => 'Gallery',
    'files' => 'Files',
    'archive' => 'Archive',

    'months' => [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ],

    'showing_page_of' => 'Showing page :current of :total',
    'read_more' => 'Read more',
    'created_by' => 'Created by :name',

    'name' => 'Name',
    'email' => 'E-mail',
    'message' => 'Message',
    'submit' => 'Submit',
    'contact_form_not_sent' => 'Contact form has not been sent. Try again.',
    'contact_form_sent' => 'Contact form has been sent.',
    'new_message_from_the_website' => 'Hi,<br>There was a new message sent from the website.',
    'have_a_good_day' => 'Have a good day.',
];