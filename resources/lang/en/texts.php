<?php

return [

    'quick_text' => 'Quick content',
    'element_id' => 'Element',

    'title' => 'Content',
    'create' => 'Create new content',
    'create_new' => 'Create',
    'add' => 'Add',

    'error_sorting_text' => 'Error sorting the content.',
    'text_sorted' => 'Content sorted.',

    'subtitle' => 'Subtitle',
    'content' => 'Content',
    'summary' => 'Summary',
    'type' => 'Type',

    'excerpts' => 'Excerpts (subtitle, summary)',
    'dates' => 'Dates (publish dates, created at)',

    'error_storing_text' => 'Error creating the content.',
    'text_stored' => 'Content has been created.',

    'error_updating_text' => 'Error updating the content.',
    'text_updated' => 'Content has been updated.',

    'delete' => 'Delete content ":name"?',
    'deleted' => 'Content ":name" was deleted.',
    'not_deleted' => 'Content ":name" was NOT deleted.',

    'text_added_to_element' => 'Content added to the element.',
    'text_not_added_to_element' => 'Content NOT added to the element.',
];