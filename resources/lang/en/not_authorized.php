<?php

return [
    'title' => "Oops!",
    'paragraph' => "You're not supposed to be here.",
    'paragraph_detail' => "You don't have sufficient permissions to visit this part of the app. <br> Select something from the menu or login as a different user.",
    'paragraph_detail_full' => "You don't have sufficient permissions to visit this app. <br> Select another app from the list below.",
    'list_of_apps' => 'Applications',
];