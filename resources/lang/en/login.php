<?php

return [
    
    'title' => '<i class="fa fa-globe"></i> earth',
    'missing_arguments' => 'The email and password fields are not filled out. Try again.',
    'error_processing_sign_in' => 'Error signing in. Try again.',
    'invalid_login' => 'The credentials you provided are not valid. Try again.',
    'email_title' => 'Email address',
    'password_title' => 'Password',
    'sign_in_normal' => 'Sign in',
    'sign_in_loading' => '<i class="fa fa-spinner fa-spin"></i> Signing in',
    'sign_in_successful' => 'Signed in. Redirecting now...',
    
];