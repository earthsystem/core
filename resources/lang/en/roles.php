<?php

return [

    'name' => 'Name',
    'title' => 'Roles',
    'create' => 'Create a new role',

    'error_storing_role' => 'Error creating the role.',
    'role_stored' => 'Role has been created.',

    'error_updating_role' => 'Error updating the role.',
    'role_updated' => 'Role has been updated.',

    'delete' => 'Delete role ":name"?',
    'deleted' => 'Role ":name" was deleted.',
    'not_deleted' => 'Role ":name" was NOT deleted.',

    'add_user' => 'Add user',
    'role_not_added_to_user' => 'Role NOT added to the user.',
    'role_added_to_user' => 'Role successfully added to the user.',
    'delete_user' => 'Delete user',

    'delete_user_title' => 'Delete user ":user" from role ":name"?',
    'deleted_user' => 'Role ":name" was deleted from the user ":user.',
    'not_deleted_user' => 'Role ":name" was NOT deleted from the user ":user.',

];