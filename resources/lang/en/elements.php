<?php

return [

    'add_new_in_this_element' => 'New',

    'title' => 'Elements',
    'create' => 'Create a new element',

    'error_sorting_element' => 'Error sorting the element.',
    'element_sorted' => 'Element sorted.',

    'add_text' => 'Add content',

    'name' => 'Name',
    'data' => 'Data',
    'type' => 'Type',
    'parent_id' => 'Parent',
    'icon' => 'Icon',
    'comment' => 'Comment',
    'locale' => 'Language',

    'delete' => 'Delete the element ":name"?',
    'deleted' => 'Element ":name" was deleted.',
    'not_deleted' => 'Element ":name" was NOT deleted.',

    'element_stored' => 'Element has been created.',
    'error_storing_element' => 'Error creating the element.',

    'error_updating_element' => 'Error updating the element.',
    'element_updated' => 'Element has been updated.',

    'create_new_template' => 'Create',
    'create_new_template_title' => 'Create a new template (position: :position)',
    'add_existing_template' => 'Add',

    'delete_text' => 'Delete',
    'delete_text_title' => 'Delete the content ":name" from the element?',
    'deleted_text' => 'Content ":name" was remove from the element.',
    'not_deleted_text' => 'Content ":name" was NOT removed from the element.',
];