@extends( earthView("boilerplate.app", true) )

@section('content')

    <div class="row">

        <div class="col-lg-push-6 col-lg-6 col-md-push-6 col-md-6 col-sm-12 col-xs-12">

            @if ($canCreateTexts)
                {!! Form::open(['url' => earthRoute('texts.postStore') . earthRequestVars(), 'method' => 'post', 'class' => 'ajax-form', 'files' => true]) !!}
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            {{earthLang('texts.quick_text')}}
                        </h4>
                    </div>

                    <div class="panel-body">

                        {!! formRules(\Earth\Core\Entities\Text\Text::$formRules) !!}
                        <span class="meta-data" data-refresh-uri="{{earthRoute('home')}}"></span>

                        {!! Form::hidden('active', 1) !!}

                        <div class="form-group">
                            <label for="title" class="sr-only">{{earthLang('form.title')}}</label>
                            {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => earthLang('form.title')]) !!}
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="type">{{earthLang('texts.type')}}</label>
                                    {!! Form::select('type', $types, null, ['data-placeholder' => earthLang('texts.type'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="type">{{earthLang('base.locale')}}</label>
                                    {!! Form::select('locale', $locales, config('earth.system_locale'), ['data-placeholder' => earthLang('base.locale'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="image">{{earthLang('base.image')}}</label>
                                    {!! Form::file('image', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="parent_id">{{earthLang('texts.element_id')}}</label>
                                    {!! Form::select('parent_id', $options + ['' => ''], null, ['data-placeholder' => earthLang('elements.parent_id'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {!! Form::textarea('content', null, ['class' => 'form-control wysiwyg mb-20', 'rows' => 10]) !!}
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success">{{earthLang('form.create')}}</button>
                    </div>

                </div>
                {!! Form::close() !!}
            @endif

        </div>

        <div class="col-lg-pull-6 col-lg-6 col-md-pull-6 col-md-6 col-sm-12 col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        {{earthLang('base.latest_activities')}} ({{$latestActivities->count()}})
                    </h4>
                </div>

                <!-- List group -->
                <ul class="list-group">
                    @foreach($latestActivities as $activity)
                        <li class="list-group-item" data-tooltip="true" title="{{$activity->text}}">
                            <p class="shortable">{{$activity->text}}</p>
                            <small>
                                <span class="mr-10">
                                    <i class="fa fa-user"></i>
                                    @if ( isset($activity->user) )
                                        {{$activity->user->full_name}}
                                    @else
                                        {{earthLang('users.no_name')}}
                                    @endif
                                </span>

                                <span class="mr-10">
                                    <i class="fa fa-calendar"></i>
                                    {{$activity->created_at->format("d.m.Y H:i:s")}}
                                </span>

                                <span class="mr-10">
                                    <i class="fa fa-laptop"></i>
                                    {{$activity->ip_address}}
                                </span>
                            </small>
                        </li>
                    @endforeach
                </ul>

            </div>

        </div>

    </div>

@stop