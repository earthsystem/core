<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle pull-right btn btn-default collapsed collapse-main-sidebar" data-toggle="collapse" data-target="#earth-navbar-collapse" aria-expanded="false">
                <i class="fa fa-bars fa-fw"></i> <span class="hidden-xs">Menu</span>
            </button>

            <a href="#" class="toggle-sidebar btn btn-default hidden-lg hidden-md pull-right">
                <i class="fa fa-bars fa-fw"></i> {{earthLang('base.toggle_sidebar')}}
            </a>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{earthRoute('home')}}">
                <i class="fa fa-fw fa-globe"></i> earth
            </a>
        </div>

        <div class="collapse navbar-collapse" id="earth-navbar-collapse">

            <ul class="navbar-nav nav navbar-left">

                <li class="@if(isCurrentRoute("home")) active @endif">
                    <a href="{{earthRoute('home')}}">
                        <span class="icon"><i class="fa fa-fw fa-home"></i></span>
                        {{earthLang('base.home')}}
                    </a>
                </li>

                {!! navigation( $items, $routePrefix ) !!}

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav nav navbar-right">

                @if (isset($availableApps) and $availableApps->count())
                <!-- apps -->
                <li class="dropdown">
                    <a href="#"
                       class="dropdown-toggle"
                       data-toggle="dropdown"
                       role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        <span class="icon"><i class="fa fa-fw fa-cubes"></i></span>
                        {{earthLang('base.apps')}}
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        @foreach($availableApps as $app)
                            <li>
                                <a href="/{{$app->data}}">
                                    <i class="fa fa-{{$app->icon}}"></i>
                                    {{$app->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <!-- // apps -->
                @endif

                <li class="dropdown">
                    <a href="#" data-toggle="dropdown">
                        <span class="icon"><i class="fa fa-fw fa-user"></i></span>

                        {{$loggedInUser->full_name}}

                        @if (isAliasUser())
                            ({{session('aliasUser')->full_name}})
                        @endif

                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">

                        @if (isAdminUser() or isUserInAdminRole())
                        <li>
                            <a href="{{earthRoute('getSwitchUser')}}"
                               data-open-in-modal="true"
                               data-refresh-type="hard">
                                <span class="icon"><i class="fa fa-fw fa-retweet"></i></span>
                                {{earthLang('users.switch_user')}}
                            </a>
                        </li>
                        @endif

                        <li>
                            <a href="{{earthRoute('getChangePassword')}}"
                                data-open-in-modal="true"
                                data-refresh="none">
                                <span class="icon"><i class="fa fa-fw fa-lock"></i></span>
                                {{earthLang('users.change_password')}}
                            </a>
                        </li>

                        <li>
                            <a href="{{earthRoute('getLogout', 'earth')}}">
                                <span class="icon"><i class="fa fa-fw fa-power-off"></i></span>
                                {{earthLang('base.sign_out')}}
                            </a>
                        </li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>