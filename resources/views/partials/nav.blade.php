@if (isset($items))

    @foreach ($items as $item)
        <li class="@if ($item->children->count()) {{ $level == 0 ? 'dropdown' : 'dropdown-submenu' }} @endif ">
            <a @if ($item->children->count())
                    href="#" @if ($level == 0) class="dropdown-toggle" data-toggle="dropdown" @endif
                @else
                    href="@if (earthRouteExists($item->data.".getIndex", $routePrefix)) {{earthRoute($item->data.".getIndex", $routePrefix)}} @else # @endif"
                @endif>
                @if (!empty($item->icon)) <span class="icon"><i class="fa fa-fw fa-{{$item->icon}}"></i></span> @endif
                {{$item->name}}
                @if ($item->children->count() and $level == 0) <span class="caret"></span> @endif
            </a>

            @if ($item->children->count())
                <ul class="dropdown-menu @if ($level == 0) multi-level @endif">
                    {!! navigation($item->children, $routePrefix, $level + 1) !!}
                </ul>
            @endif
        </li>
    @endforeach

@endif