@extends( earthView("layout", true) )

@section('app')
    <div class="app">
        @yield('content')
    </div>
@stop