@if ($paginator->lastPage() > 1)
    <nav>
        <ul class="pager">
            <li class="previous {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url($paginator->currentPage()-1) }}"
                   data-toggle="content"
                   @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
                   @if (isset($pushState)) data-push-state="{{$pushState}}" @endif
                   @if (isset($loadDynamics)) data-load-dynamics="{{$loadDynamics}}" @endif>
                    <span aria-hidden="true">&larr;</span> {{earthLang('base.previous')}}
                </a>
            </li>
            <li class="next">
                <a href="{{ ($paginator->currentPage() == $paginator->lastPage()) ? $paginator->url($paginator->currentPage()) : $paginator->url($paginator->currentPage()+1) }}"
                   data-toggle="content"
                   @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
                   @if (isset($pushState)) data-push-state="{{$pushState}}" @endif
                   @if (isset($loadDynamics)) data-load-dynamics="{{$loadDynamics}}" @endif>
                    {{earthLang('base.next')}} <span aria-hidden="true">&rarr;</span>
                </a>
            </li>
        </ul>
    </nav>
@endif