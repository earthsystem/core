@if ($dontShowTabs)
    <li role="presentation">
        <a href="{{earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ) . earthRequestVars( request()->except(['rel_id', 'rel_key', 'rel_route']) )}}"
           data-tooltip="true"
           data-placement="top"
           title="{{earthLang('base.back')}}"
           data-toggle="content">
            <i class="fa fa-arrow-circle-left fa-fw"></i>
            {{earthLang('base.back')}}
        </a>
    </li>
@else
    <li role="presentation">
        <a href="{{isset($customListUri) ? $customListUri : $listUri.earthRequestVars()}}"
           data-tooltip="true"
           data-placement="top"
           title="{{earthLang('base.back')}}"
           data-toggle="content">
            <i class="fa fa-arrow-circle-left fa-fw"></i>
            {{earthLang('base.back')}}
        </a>
    </li>
@endif

@if (!$dontShowTabs)
<li role="presentation" @if (!isset($tab)) class="active" @endif>
    <a href="{{earthRouteWithVars($route.'.getShow', [$item->id]) . earthRequestVars()}}"
       data-tooltip="true"
       data-placement="top"
       data-toggle="content"
        title="{{earthLang('base.view')}}">
        <i class="fa fa-eye fa-fw"></i>
        {{earthLang('base.view')}}
    </a>
</li>
@endif

@if (canWrite())
    <li role="presentation" @if (isset($tab) and $tab == "edit") class="active" @endif>
        <a href="{{earthRouteWithVars($route.'.getEdit', [$item->id]) . earthRequestVars()}}"
           data-tooltip="true"
           data-placement="top"
           data-toggle="content"
            title="{{earthLang('base.edit')}}">
            <i class="fa fa-edit fa-fw"></i>
            {{earthLang('base.edit')}}
        </a>
    </li>
    @if (!$dontShowTabs)
        @foreach($links as $index => $link)
            <li role="presentation" class="{{$link->isActive($tab) ? 'active' : ''}}">
                @if (get_class($link) == \Earth\Core\Entities\ModalTab::class)
                    <a href="{{$link->url . earthRequestVars()}}"
                       @if (isset($link->refreshType)) data-refresh-type="{{$link->refreshType}}" @endif
                       data-refresh-uri="{{$link->refreshUri . earthRequestVars() }}"
                       data-open-in-modal="true"
                       @if (isset($link->modalSize)) data-modal-size="{{$link->modalSize}}" @endif
                       data-tooltip="true"
                       data-placement="top"
                       title="{{$link->name}}">
                        <i class="fa fa-fw fa-{{$link->icon}}"></i>
                        @if ($link->isActive($tab))
                            {{$link->name}}
                        @else
                            <span class="hidden-lg hidden-md hidden-sm">{{$link->name}}</span>
                        @endif
                    </a>
                @else
                    <a href="{{$link->url . earthRequestVars()}}"
                       data-toggle="content"
                       data-tooltip="true"
                       data-placement="top"
                       title="{{$link->name}}">
                        <i class="fa fa-fw fa-{{$link->icon}}"></i>
                        @if ($link->isActive($tab))
                            {{$link->name}}
                        @else
                            <span class="hidden-lg hidden-md hidden-sm">{{$link->name}}</span>
                        @endif
                    </a>
                @endif
            </li>
        @endforeach
    @endif
@endif