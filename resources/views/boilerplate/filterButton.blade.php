<!-- Single button -->
<div class="btn-group btn-block">
    <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{$title}} <span class="caret"></span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
        @foreach($filterFields as $key => $field)
            <li>
                <a href="{{$baseRoute . earthRequestVars( request()->except(['type', 'page']) + ['type' => $key ] )}}"
                   data-toggle="content">
                    {{$field}}
                </a>
            </li>
        @endforeach
    </ul>
</div>