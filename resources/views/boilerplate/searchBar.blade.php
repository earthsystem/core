<form action="{{ $baseRoute . earthRequestVars( isset($requestVars) ? $requestVars : request()->except(['q', 'page']) )}}" method="get" class="ajax-form">
    @if (isset($refreshContainer) or isset($pushState))
        <span class="meta-data"
              @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
              @if (isset($pushState)) data-push-state="{{$pushState}}" @endif></span>
    @endif
    <div class="input-group input-search">
        <input type="text" class="form-control" name="q" placeholder="{{$placeholder or earthLang('base.search_for')}}" value="{{$qValue or request()->get('q')}}">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit" data-loading-text="{{$loadingTitle or earthLang('base.searching')}}">{{$title or earthLang('base.search')}}</button>
        </span>
    </div><!-- /input-group -->
</form>