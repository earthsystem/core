@if ($paginator->lastPage() > 1)
    <ul class="pagination lv-pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url(1) }}"
               data-toggle="content"
               @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
               @if (isset($pushState)) data-push-state="{{$pushState}}" @endif>
                &laquo;
            </a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                <a href="{{ $paginator->url($i) }}"
                   data-toggle="content"
                   @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
                   @if (isset($pushState)) data-push-state="{{$pushState}}" @endif>
                    {{ $i }}
                </a>
            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a href="{{ ($paginator->currentPage() == $paginator->lastPage()) ? $paginator->url($paginator->currentPage()) : $paginator->url($paginator->currentPage()+1) }}"
               data-toggle="content"
               @if (isset($refreshContainer)) data-refresh-container="{{$refreshContainer}}" @endif
               @if (isset($pushState)) data-push-state="{{$pushState}}" @endif>
                &raquo;
            </a>
        </li>
    </ul>
@endif