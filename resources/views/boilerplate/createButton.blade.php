@if (canWrite())
    <ul class="mfb-component--br mfb-slidein" data-mfb-toggle="hover">
        <li class="mfb-component__wrap">
            <a href="{{$createUri . earthRequestVars()}}"
               data-mfb-label="{{$createLabel}}"
               class="mfb-component__button--main createButton"
               data-toggle="content">
                <i class="mfb-component__main-icon--resting ion-plus-round"></i>
                <i class="mfb-component__main-icon--active ion-plus-round"></i>
            </a>
        </li>
    </ul>
@endif