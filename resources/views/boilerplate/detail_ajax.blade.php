@if (canWrite())
    @include( earthView('boilerplate.createButton', true), ['createUri' => isset($customCreateUri) ? $customCreateUri : $createUri, 'createLabel' => isset($createLabel) ? $createLabel : earthLang('base.create')])
@endif

<div class="row">

    <div class="col-lg-{{$sidebarLgWidth or 3}} col-md-{{$sidebarMdWidth or 3}} col-sm-12 col-xs-12">
        <div class="sidebar toggleable hidden-sm hidden-xs">
            @include( earthView('boilerplate.searchBar', true), ['baseRoute' => earthRouteWithVars($route.'.getShow', [$item->id])])
            @include( isset($customSidebar) ? $customSidebar : earthView($route.'.sidebar', true))
        </div>
    </div>

    <div class="col-lg-{{$contentLgWidth or 9}} col-md-{{$contentMdWidth or 9}} col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs">
                <ul class="nav nav-tabs">
                    @include( earthView('boilerplate.pills', true) )
                </ul>
            </div>
            <div class="col-xs-12 visible-xs">
                <ul class="nav nav-tabs nav-stacked">
                    @include( earthView('boilerplate.pills', true) )
                </ul>
            </div>
        </div>

        @include($view)
    </div>

</div>