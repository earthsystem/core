@extends( earthView('boilerplate.app', true) )

@section('content')
    @if (isset($customView))
        @include($customView)
    @else
        @include( earthView('boilerplate.list_ajax', true) )
    @endif
@stop