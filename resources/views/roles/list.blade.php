@include( earthView('boilerplate.createButton', true), ['createUri' => $createUri, 'createLabel' => earthLang('roles.create') ] )

<!-- search and filters -->
<div class="row">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
        @include( earthView('boilerplate.searchBar', true), ['baseRoute' => earthRoute('roles.getIndex')])
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        @include( earthView('boilerplate.sortButton', true), [
            'title' => earthLang('base.sort'),
            'baseRoute' => earthRoute('roles.getIndex'),
            'sortFields' => [
                ['title' => earthLang('base.created_at'), 'value' => 'created_at-desc'],
                ['title' => earthLang('base.name'), 'value' => 'name'],
                ['title' => earthLang('base.last_modified'), 'value' => 'updated_at-desc'],
            ]
        ])
    </div>

</div>

@include( earthView('boilerplate.pagination', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )

<div class="list-group list-view-pf mt-20">
    @foreach($items as $item)
        <div class="list-group-item">
            <div class="list-view-pf-actions">
                @if (canWrite() and $item->can_modify)
                    <div class="dropdown pull-right dropdown-kebab-pf">
                        <button class="btn btn-link dropdown-toggle" type="button" id="dropdownKebabRight{{$item->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="fa fa-ellipsis-v"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownKebabRight{{$item->id}}">
                            @if (canWrite() and $item->can_modify)
                                <li>
                                    <a href="{{earthRouteWithVars('roles.getEdit', [$item->id]) . earthRequestVars()}}" data-toggle="content">
                                        <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{earthRouteWithVars('roles.users.getCreate', [$item->id])}}"
                                       data-open-in-modal="true"
                                       data-refresh-type="soft"
                                       data-refresh-uri="{{earthRouteWithVars('roles.getIndex').earthRequestVars()}}">
                                        <i class="fa fa-fw fa-plus"></i>
                                        {{earthLang('roles.add_user')}}
                                    </a>
                                </li>
                            @endif

                            @if (canDelete() and $item->can_modify)
                                <li>
                                    <a href="{{earthRouteWithVars('roles.delete', [$item->id]) . earthRequestVars()}}"
                                       data-csrf-token="{{csrf_token()}}"
                                       data-refresh-uri="{{$listUri.earthRequestVars()}}"
                                       data-title="{{earthLangWithVars('roles.delete', ['name' => $item->name])}}"
                                       data-deleted-title="{{earthLangWithVars('roles.deleted', ['name' => $item->name])}}"
                                       data-not-deleted-title="{{earthLangWithVars('roles.not_deleted', ['name' => $item->name])}}"
                                       class="delete">
                                        <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                @endif
            </div>
            <div class="list-view-pf-main-info">
                <div class="list-view-pf-left">
                    {{$item->id}}.
                </div>
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-heading">
                            <a href="{{earthRouteWithVars($route.'.getShow', [$item->id]) . earthRequestVars()}}" title="{{$item->name}}" data-toggle="content">
                                {{$item->name}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@include( earthView('boilerplate.pagination', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )