<div class="list-group mt-20 mt-sm-0">
    @foreach($items as $sidebarItem)
        <a href="{{earthRouteWithVars('roles.getShow', [$sidebarItem->id]) . earthRequestVars()}}"
           class="list-group-item @if($sidebarItem->id == $item->id) active @endif"
           data-toggle="content">
            <p class="shortable" title="{{$sidebarItem->name}}">{{$sidebarItem->id}}. {{$sidebarItem->name}}</p>
        </a>
    @endforeach
</div>

@include( earthView('boilerplate.paginationSimple', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )