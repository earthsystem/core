@extends( $extendView )

@section('title', earthLang('users.add_role'))
@section('app')
    <div class="attach_container">
        @include( earthView('roles.attach.list', true) )
    </div>
@stop
@section('footer')
    <a href="#" class="btn btn-link close-modal">{{earthLang('base.modal_close')}}</a>
@stop