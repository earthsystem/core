@if (isset($message) and isset($type))
    <div class="alert alert-dismissible alert-{{$type}}">
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
        {{$message}}
    </div>
@endif

@include( earthView('boilerplate.searchBar', true), [
    'baseRoute' => earthRouteWithVars('users.roles.getCreate', [$id]),
    'refreshContainer' => '.attach_container',
    'pushState' => 'false',
    'requestVars' => request()->except(['role_id', 'q', 'page'])
])

<div class="panel panel-default mt-20">
    <!-- Default panel contents -->
    <div class="panel-heading">
        {{earthLang('base.no_of_results')}}: {{$items->total()}},
        {{earthLang('base.showing_page')}}: {{$items->currentPage()}} / {{$items->lastPage()}}
    </div>

    <!-- List group -->
    <ul class="list-group">
        @foreach($items as $item)
            <li class="list-group-item">
                <div class="pull-left">
                    <a href="{{earthRouteWithVars('users.roles.getCreate', [$id]) . earthRequestVars( request()->except(['role_id']) + ['role_id' => $item->id] ) }}"
                        data-toggle="content"
                        data-refresh-container=".attach_container"
                        data-push-state="false">
                        {{$item->name}}
                    </a>
                </div>

                <div class="pull-right">
                    <a href="{{earthRouteWithVars('users.roles.getCreate', [$id]) . earthRequestVars( request()->except(['role_id']) + ['role_id' => $item->id] ) }}"
                       class="btn btn-xs btn-default"
                       data-toggle="content"
                       data-refresh-container=".attach_container"
                       data-push-state="false">
                        <i class="fa fa-fw fa-plus"></i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </li>
        @endforeach
    </ul>
</div>

@include( earthView('boilerplate.paginationSimple', true), [
    'paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ),
    'refreshContainer' => '.attach_container',
    'pushState' => 'false'
] )