<div class="panel panel-default mt-20">
    <div class="panel-heading">
        <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
    </div>

    <!-- List group -->
    <ul class="list-group">
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('roles.name')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->name}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.created_at')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->created_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.last_modified')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->updated_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
    </ul>

    <div class="panel-footer">
        @if (canWrite() and $item->can_modify)
            <a href="{{earthRouteWithVars('roles.getEdit', [$item->id]) . earthRequestVars()}}"
               data-toggle="content"
               class="btn btn-primary">
                <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
            </a>

            @if (canDelete() and $item->can_modify)
                <a href="{{earthRouteWithVars('roles.delete', [$item->id]) . earthRequestVars()}}"
                   data-csrf-token="{{csrf_token()}}"
                   data-refresh-uri="{{$listUri.earthRequestVars()}}"
                   data-title="{{earthLangWithVars('roles.delete', ['name' => $item->name])}}"
                   data-deleted-title="{{earthLangWithVars('roles.deleted', ['name' => $item->name])}}"
                   data-not-deleted-title="{{earthLangWithVars('roles.not_deleted', ['name' => $item->name])}}"
                   class="delete btn btn-danger">
                    <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                </a>
            @endif
        @endif
    </div>
</div>