{!! Form::model($item, ['url' => earthRouteWithVars('roles.putUpdate', [$item->id]) . earthRequestVars(), 'method' => 'put', 'class' => 'ajax-form']) !!}

    <div class="panel panel-default mt-20">
        <div class="panel-heading">
            <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
        </div>
        <div class="panel-body">

            @if ($dontShowTabs)
                <span class="meta-data" data-refresh-uri="{{earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] )}}"></span>
            @else
                <span class="meta-data" data-refresh-uri="{{$listUri.earthRequestVars()}}"></span>
            @endif
            {!! formRules(\Earth\Core\Entities\Role\Role::$formRules) !!}

            <div class="form-group">
                <label for="first_name">{{earthLang('roles.name')}}</label>
                {!! Form::text('name', null, ['placeholder' => earthLang('roles.name'), 'class' => 'form-control']) !!}
            </div>

        </div>

        <div class="panel-footer">
            {!! editButtons(earthRouteWithVars('roles.getShow', [$item->id]).earthRequestVars()) !!}
        </div>
    </div>

{!! Form::close() !!}