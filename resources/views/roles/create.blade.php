{!! Form::open(['url' => earthRoute('roles.postStore') . earthRequestVars(), 'method' => 'post', 'class' => 'ajax-form']) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <!-- title and back button -->
                {!! backButton($listUri.earthRequestVars()) !!}

                {{earthLang('roles.create')}}
            </h3>
        </div>
        <div class="panel-body">
            <span class="meta-data" data-refresh-uri="{{$listUri.earthRequestVars()}}"></span>
            {!! formRules(\Earth\Core\Entities\Role\Role::$formRules) !!}

            <div class="form-group">
                <label for="name">{{earthLang('roles.name')}}</label>
                {!! Form::text('name', null, ['placeholder' => earthLang('roles.name'), 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="panel-footer">
            {!! createButtons($listUri.earthRequestVars()) !!}
        </div>
    </div>
{!! Form::close() !!}