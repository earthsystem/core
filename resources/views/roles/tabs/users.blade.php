<div class="panel panel-default mt-20">
    <div class="panel-heading">

        <div class="pull-right">
            <a href="{{earthRouteWithVars('roles.users.getCreate', [$item->id])}}"
               class="btn btn-success btn-xs"
               data-open-in-modal="true"
               data-refresh-type="soft"
               data-refresh-uri="{{earthRouteWithVars('roles.getShow', [$item->id, "users"]).earthRequestVars()}}">
                <i class="fa fa-fw fa-plus"></i>
                {{earthLang('roles.add_user')}}
            </a>
        </div>

        <h3 class="panel-title">{{earthLang('base.users')}} ({{$item->id}}. {{$item->name}})</h3>
    </div>
    <table class="table">
        <thead>
        <tr>
            @if (canDelete() and $item->can_modify)
                <th>
                    <input type="checkbox" class="toggleCheckedInNearestTable">
                </th>
            @endif
            <th>{{earthLang('users.full_name')}}</th>
            <th class="text-right">
                @if (canDelete() and $item->can_modify)
                    <a href="{{earthRouteWithVars('roles.users.delete', [$item->id, ":id_placeholder"]) . earthRequestVars()}}"
                       data-csrf-token="{{csrf_token()}}"
                       data-deleted-title="{{earthLang('base.items_deleted')}}"
                       data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                       data-refresh-uri="{{earthRouteWithVars("roles.getShow", [$item->id, "users"]) . earthRequestVars()}}"
                       class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                        <i class="fa fa-trash-o"></i>
                        {{earthLang('base.delete_all')}}
                    </a>
                @endif
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($item->users()->get() as $user)
            <tr>
                @if (canDelete() and $item->can_modify)
                    <td>
                        <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$user->id}}">
                    </td>
                @endif
                <td>{{$user->full_name}}</td>
                <td class="text-right">
                    @if (canWrite() and $user->can_modify)
                        <a href="{{earthRouteWithVars('users.getEdit', [$user->id]) . earthRequestVars( request()->all() + ["rel_id" => $item->id, "rel_route" => "roles.getShow", "rel_key" => "users"] )}}"
                            class="btn btn-xs btn-default"
                            data-toggle="content">
                            <i class="fa fa-fw fa-edit"></i>
                            {{earthLang('base.edit')}}
                        </a>
                    @endif
                    @if (canDelete() and !(!$user->can_modify and !$item->can_modify) )
                        <a href="{{earthRouteWithVars('roles.users.delete', [$item->id, $user->id]) . earthRequestVars()}}"
                           data-csrf-token="{{csrf_token()}}"
                           data-refresh-uri="{{earthRouteWithVars('roles.getShow', [$item->id, "users"]) . earthRequestVars()}}"
                           data-title="{{earthLangWithVars('roles.delete_user_title', ['name' => $item->name, 'user' => $user->full_name])}}"
                           data-deleted-title="{{earthLangWithVars('roles.deleted_user', ['name' => $item->name, 'user' => $user->full_name])}}"
                           data-not-deleted-title="{{earthLangWithVars('roles.not_deleted_user', ['name' => $item->name, 'user' => $user->full_name])}}"
                           class="delete btn btn-xs btn-danger">
                            <i class="fa fa-fw fa-trash-o"></i> {{earthLang('roles.delete_user')}}
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>