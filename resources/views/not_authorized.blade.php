<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="token" content="{{csrf_token()}}">
    <title>{{$pageTitle or ''}}</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{earthAsset('css/main.css')}}">

@yield('system_layout_css')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
            <div class="jumbotron">
                <h1><i class="fa fa-exclamation-circle"></i> {{earthLang('not_authorized.title')}}</h1>
                <p>{{earthLang('not_authorized.paragraph')}}</p>
                <p><small>{!! earthLang('not_authorized.paragraph_detail_full') !!}</small></p>

                @if($listOfApps->count())
                    <h3>{{earthLang('not_authorized.list_of_apps')}}</h3>
                    <ul class="list-group">
                        @foreach($listOfApps as $app)
                            <li class="list-group-item">
                                @if ( earthRouteExists("home", $app->data) )
                                    <a href="{{earthRoute("home", $app->data)}}">
                                        <i class="fa fa-{{$app->icon}}"></i>
                                        {{$app->name}}
                                    </a>
                                @else
                                    <a href="#">
                                        <i class="fa fa-{{$app->icon}}"></i>
                                        {{$app->name}}
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@yield('system_layout_js')
</body>

</html>