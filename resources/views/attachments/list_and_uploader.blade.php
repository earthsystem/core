<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div id="attachments_container">
            <div id="file_list"></div>
            <div id="drop_area" class="well text-center text-primary" style="width: 100%; min-height: 120px">
                <i class="fa fa-arrow-down drop-files"></i>
                <strong>{{earthLang('attachments.drop_files_here')}}</strong>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="list" id="attachments_list">
            @include( earthView('attachments.list', true) )
        </div>
    </div>
</div>

@include( earthView('attachments.js', true) )