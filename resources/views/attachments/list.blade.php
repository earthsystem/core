@if ($attachments = $item->attachments()->whereType($type)->with('media')->orderBy('position')->get()
     and $attachments->count())
    <table class="table table-hover">
        <thead>
            <tr>
                @if (canDelete())
                    <th>
                        <input type="checkbox" class="toggleCheckedInNearestTable">
                    </th>
                @endif
                <th>{{earthLang('form.name')}}</th>
                <th class="text-right">
                    @if (canDelete())
                        <a href="{{earthRouteWithVars('attachments.delete', [":id_placeholder"]) . earthRequestVars()}}"
                           data-csrf-token="{{csrf_token()}}"
                           data-deleted-title="{{earthLang('base.items_deleted')}}"
                           data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                           data-refresh-uri="{{earthRouteWithVars("attachments.getShow", [$id, $class, $type]) . earthRequestVars( request()->except(["list"]) + ["list" => true])}}"
                           data-refresh-container="#attachments_list"
                           data-push-state="false"
                           class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                            <i class="fa fa-trash-o"></i>
                            {{earthLang('base.delete_all')}}
                        </a>
                    @endif
                </th>
            </tr>
        </thead>

        <tbody class="@if (canWrite()) sortable @endif">
            @foreach($attachments as $attachment)
                <tr @if (canWrite())
                        data-id="{{$attachment->id}}"
                        data-href="{{earthRouteWithVars('attachments.putSort', [$attachment->id])}}"
                        data-csrf-token="{{csrf_token()}}"
                    @endif>
                    @if (canDelete())
                        <td>
                            <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$attachment->id}}">
                        </td>
                    @endif
                    <td>
                        @if ($type == "gallery")
                            <a href="{{$attachment->media->file->url('large')}}" class="popup">
                                {{$attachment->media->file->originalFileName()}}
                            </a>
                        @else
                            <a href="{{$attachment->media->file->url()}}" target="_blank">
                                {{$attachment->media->file->originalFileName()}}
                            </a>
                        @endif
                    </td>
                    <td class="text-right">
                        @if (canDelete())
                            <a href="{{earthRouteWithVars('attachments.delete', [$attachment->id]) . earthRequestVars()}}"
                               data-csrf-token="{{csrf_token()}}"
                               data-refresh-uri="{{earthRouteWithVars("attachments.getShow", [$id, $class, $type]) . earthRequestVars( request()->except(["list"]) + ["list" => true])}}"
                               data-refresh-container="#attachments_list"
                               data-title="{{earthLangWithVars('attachments.delete', ['title' => $attachment->media->file->originalFileName()])}}"
                               data-deleted-title="{{earthLangWithVars('attachments.deleted', ['title' => $attachment->media->file->originalFileName()])}}"
                               data-not-deleted-title="{{earthLangWithVars('attachments.not_deleted', ['title' => $attachment->media->file->originalFileName()])}}"
                               data-push-state="false"
                               class="delete btn btn-xs btn-danger">
                                <i class="fa fa-trash-o"></i>
                                {{earthLang('base.delete')}}
                            </a>
                        @endif
                        @if (canWrite())
                            <a href="{{earthRouteWithVars('attachments.locales.getIndex', [$id, $class, $attachment->id]) . earthRequestVars()}}"
                                data-refresh-container="#earthModal .modal-body"
                                data-toggle="content"
                                data-push-state="false"
                                class="btn btn-success btn-xs">
                                <i class="fa fa-globe"></i>
                                {{earthLang('base.locales')}}
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif