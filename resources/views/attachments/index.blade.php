@extends( earthView('ajax', true) )

@section('title', $title)

@section('app')
    @include( earthView('attachments.list_and_uploader', true) )
@stop

@section('footer')
    <button type="button" id="attachments_close" class="btn btn-link pull-right" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>

    <div class="pull-left">

        <button id="browse_files" class="btn btn-primary">
            <i class="fa fa-folder-open"></i>
            {{earthLang('attachments.browse')}}
        </button>

        <button id="clear_queue" class="btn btn-default">
            <i class="fa fa-trash-o"></i>
            {{earthLang('attachments.clear_queue')}}
        </button>

        <a href="{{earthRouteWithVars('attachments.getShow', [$id, $class, $type]) . earthRequestVars( request()->all() + ["list" => true] )}}" id="refresh_list"
           class="btn btn-default"
           data-toggle="content"
           data-refresh-container="#attachments_list"
           data-push-state="false">
            <i class="fa fa-refresh"></i>
            {{earthLang('attachments.refresh_list')}}
        </a>

    </div>
@stop