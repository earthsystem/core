<a href="{{earthRouteWithVars('attachments.getShow', [$attachableId, $attachableClass, $attachment->type]) . earthRequestVars( request()->except(["list_and_upload", "list"]) + ["list_and_upload" => true])}}"
   data-refresh-container="#earthModal .modal-body"
   data-toggle="content"
   data-push-state="false"
   class="btn btn-default">
    <i class="fa fa-arrow-circle-left fa-fw"></i>
    {{earthLang('base.back')}}
</a>

<a href="{{earthRouteWithVars('attachments.locales.getIndex', [$attachableId, $attachableClass, $attachment->id]) . earthRequestVars()}}"
   data-refresh-container="#earthModal .modal-body"
   data-toggle="content"
   data-push-state="false"
   class="btn btn-default">
    <i class="fa fa-refresh fa-fw"></i>
    {{earthLang('attachments.refresh_list')}}
</a>


<div class="row mt-20">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        @foreach( array_chunk($locales->all(), 2) as $localeGroup)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    @foreach($localeGroup as $locale)

                        @if ($locale->attachment_locale)
                            {!! Form::open([
                                'url' => earthRouteWithVars('attachments.locales.putUpdate', [$locale->attachment_locale->id]) . earthRequestVars(),
                                'method' => 'put',
                                'class' => 'ajax-form'
                            ]) !!}
                        @else
                            {!! Form::open([
                                'url' => earthRouteWithVars('attachments.locales.postStore', [$attachableId, $attachableClass, $attachmentId]) . earthRequestVars(),
                                'method' => 'post',
                                'class' => 'ajax-form'
                            ]) !!}
                        @endif

                            <span class="meta-data"
                                data-refresh-container="#earthModal .modal-body"
                                data-refresh-uri="{{earthRouteWithVars('attachments.locales.getIndex', [$attachableId, $attachableClass, $attachment->id]) . earthRequestVars()}}"
                                data-push-state="false"></span>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    {{$locale->locale}}
                                    <input type="hidden" name="locale" value="{{$locale->locale}}">
                                </span>
                                <input type="text"
                                       class="form-control"
                                       name="title"
                                       @if ($locale->attachment_locale) value="{{$locale->attachment_locale->title}}" @endif />
                                <span class="input-group-btn">
                                    <button class="btn btn-success">{{earthLang('form.save')}}</button>
                                </span>
                            </div>

                        {!! Form::close() !!}

                    @endforeach

                </div>
            </div>
        @endforeach

    </div>
</div>