<script src="{{asset('vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js')}}"></script>

<script>
    var attachments_uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button: 'browse_files',
        container: 'attachments_container',
        drop_element: 'drop_area',
        max_file_size : '1gb',
        url: '{{earthRouteWithVars('attachments.postUpload', [$id, $class, $type])}}'
    });

    attachments_uploader.init();
    attachments_uploader.bind('FilesAdded', function(up, files) {
        $("#attachments_close").prop("disabled", true);

        $.each(files, function(i, file) {
            $("#file_list").append(
                '<div id="'+file.id+'" class="well well-sm file_list_item">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <strong></strong>' +
                '</div>'
            );
            attachments_uploader.start();
        });
    });

    attachments_uploader.bind('UploadComplete', function(up, files) {
        $("#refresh_list").click();
        $("#attachments_close").prop("disabled", false);
        setTimeout(function(){
            $("#clear_queue").click();
        }, 2000);
    });

    attachments_uploader.bind('UploadProgress', function(up, file) {
        $('#' + file.id + " strong").html('<span class="label label-default">'+file.percent+'%</span>');
    });

    attachments_uploader.bind('FileUploaded', function(up, file) {
        $('#' + file.id + " strong").html('<span class="label label-success">{{earthLang('base.done')}}</span>');
    });

    attachments_uploader.bind('Error', function(up, err) {
        $('#file_list').append("<div class='error_item'>Error: " + err.code +
                ", Message: " + err.message +
                (err.file ? ", File: " + err.file.name : "") +
                "</div>"
        );

        up.refresh(); // Reposition Flash/Silverlight
    });

    $("#clear_queue").click(function(e){
        e.preventDefault();
        $(".file_list_item").remove();
        $(".error_item").remove();
    });
</script>