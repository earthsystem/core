<div class="modal-header">
    <h4 class="modal-title">{{earthLang('attachments.upload_a_file')}}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            {!! Form::open(['method' => 'post', 'url' => earthRoute('attachments.file-manager.postUpload'), 'files' => 'true']) !!}
                <input type="hidden" value="{{$type}}" name="type">
                {!! Form::file('file', ['class' => 'fileInput']) !!}
            {!! Form::close() !!}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div id="somethingRandom">
                @include( earthView('attachments.manager.list', true) )
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-success">{{earthLang('attachments.upload')}}</button>
    <a href="#" class="btn btn-link" data-dismiss="modal">{{earthLang("base.modal_close")}}</a>
</div>