{{earthLang('base.no_of_results')}}: {{$media->total()}},
{{earthLang('base.showing_page')}}: {{$media->currentPage()}} / {{$media->lastPage()}}
<br><br>

<ul class="list-group">
    @foreach($media as $item)
        <li class="list-group-item">
            <a href="{{$item->file->url()}}" class="select-file" data-field-name="{{$field_name}}" data-load-dynamics="false">
                {{$item->file->originalFileName()}}
            </a>
        </li>
    @endforeach
</ul>

@include( earthView('boilerplate.paginationSimple', true), [
    'paginator' => $media->appends( request()->except(['page', 'ajax', 'ajax_process']) + ["list_only" => true] ),
    'refreshContainer' => '#somethingRandom',
    'pushState' => 'false',
    'loadDynamics' => 'false'
] )