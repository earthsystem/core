<div class="panel-group mt-20" id="permissionsCollapse" role="tablist" aria-multiselectable="true">
    @foreach($fields as $field => $data)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-{{$field}}">
                <h4 class="panel-title">
                    <a href="#{{$field}}Collapse"
                       role="button"
                       data-toggle="collapse"
                       data-parent="#permissionsCollapse"
                       aria-expanded="true"
                       aria-controls="{{$field}}Collapse">
                        {{ucfirst($field)}}
                    </a>
                </h4>
            </div>

            <div id="{{$field}}Collapse"
                 class="panel-collapse collapse
                 @if( request()->has('permission') and request()->get("permission") == $field) ) in @endif
                 @if( !request()->has("permission") and $field == "read") in @endif"
                 role="tabpanel">
                <div class="panel-body">

                    {!! Form::open(['url' => earthRoute("permissions.postStore"), 'class' => 'ajax-form', 'method' => 'post' ]) !!}
                        <span class="meta-data" data-refresh-uri="{{$fullRoute . earthRequestVars( request()->except(["permission"]) + ["permission" => $field] )}}"></span>
                        {!! Form::hidden('field', $field) !!}
                        {!! Form::hidden('object_id', $item->id) !!}
                        {!! Form::hidden('object_type', get_class($item)) !!}

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="permissions">{{earthLang('base.roles')}}</label>
                                    <select multiple="multiple" name="permissionsRoles[]" id="permissionsRoles{{$field}}" data-selectize="true" class="form-control">
                                        @foreach($data['roles'] as $role)
                                            <option value="{{get_class($role)}};{{$role->id}}" @if ($role->hasPermission) selected @endif>
                                                {{$role->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="permissions">{{earthLang('base.users')}}</label>
                                    <select multiple="multiple" name="permissionsUsers[]" id="permissionsUsers{{$field}}" data-selectize="true" class="form-control">
                                        @foreach($data['users'] as $user)
                                            <option value="{{get_class($user)}};{{$user->id}}" @if ($user->hasPermission) selected @endif>
                                                {{$user->full_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> {{earthLang('form.save')}}
                        </button>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    @endforeach
</div>