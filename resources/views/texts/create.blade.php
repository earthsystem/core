{!! Form::open(['url' => earthRoute('texts.postStore') . earthRequestVars(), 'method' => 'post', 'class' => 'ajax-form', 'files' => true]) !!}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <!-- title and back button -->
            {!! backButton(isset($customListUri) ? $customListUri : $listUri.earthRequestVars()) !!}

            {{earthLang('texts.create')}}
        </h3>
    </div>
    <div class="panel-body">

        {!! formRules(\Earth\Core\Entities\Text\Text::$formRules) !!}
        <span class="meta-data" data-refresh-uri="{{isset($customListUri) ? $customListUri : $listUri.earthRequestVars()}}"></span>

        <div class="row">

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="title" class="sr-only">{{earthLang('form.title')}}</label>
                    {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => earthLang('form.title')]) !!}
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        {!! Form::textarea('content', null, ['class' => 'form-control wysiwyg mb-20', 'rows' => 15]) !!}
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                <div class="form-group">
                    <label for="active" class="checkbox-inline">
                        {!!Form::checkbox('active', 1, true, ['class' => 'bsToggle'])!!}
                        {{earthLang('form.active')}}
                    </label>
                </div>

                <div class="form-group">
                    <label for="type">{{earthLang('texts.type')}}</label>
                    {!! Form::select('type', $types, null, ['data-placeholder' => earthLang('texts.type'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                </div>

                <div class="form-group">
                    <label for="type">{{earthLang('base.locale')}}</label>
                    {!! Form::select('locale', $locales, config('earth.system_locale'), ['data-placeholder' => earthLang('base.locale'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                </div>

                <div class="form-group">
                    <label for="image">{{earthLang('base.image')}}</label>
                    {!! Form::file('image', ['class' => 'fileInput']) !!}
                </div>
                
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                <div class="panel-group" id="excerpts" role="tablist">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="excerptHeading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#excerpts" href="#excerptsCollapse" aria-expanded="true" aria-controls="excerptCollapse">
                                    {{earthLang('texts.excerpts')}}
                                </a>
                            </h4>
                        </div>

                        <div id="excerptsCollapse" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">

                                <div class="form-group">
                                    <label for="subtitle">{{earthLang('texts.subtitle')}}</label>
                                    {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => earthLang('texts.subtitle')]) !!}
                                </div>

                                <div class="form-group">
                                    <h5><b>{{earthLang('texts.summary')}}</b></h5>
                                    <div class="wysiwygSummary" data-name="summary"></div>
                                    {!! Form::hidden('summary', null) !!}
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel-group" id="dates" role="tablist">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="datesHeading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#dates" href="#datesCollapse" aria-expanded="true" aria-controls="excerptCollapse">
                                    {{earthLang('texts.dates')}}
                                </a>
                            </h4>
                        </div>

                        <div id="datesCollapse" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="created_at">{{earthLang('base.created_at')}}</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control date-picker" name="created_at" placeholder="{{earthLang('base.created_at')}}">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="created_at">{{earthLang('base.publish_from')}}</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control date-picker" name="publish_from" placeholder="{{earthLang('base.publish_from')}}">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label for="created_at">{{earthLang('base.publish_to')}}</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control date-picker" name="publish_to" placeholder="{{earthLang('base.publish_to')}}">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="panel-footer">
        {!! createButtons(isset($customListUri) ? $customListUri : $listUri.earthRequestVars()) !!}
    </div>
</div>

{!! Form::close() !!}