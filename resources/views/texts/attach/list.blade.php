@if (isset($message) and isset($type))
    <div class="alert alert-dismissible alert-{{$type}}">
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
        {{$message}}
    </div>
@endif

@include( earthView('boilerplate.searchBar', true), [
    'baseRoute' => earthRouteWithVars($route.'.texts.getAttach', [$id]),
    'refreshContainer' => '.attach_container',
    'pushState' => 'false',
    'requestVars' => request()->except(['text_id', 'q', 'page'])
])

<div class="panel panel-default mt-20">
    <!-- Default panel contents -->
    <div class="panel-heading">
        {{earthLang('base.no_of_results')}}: {{$items->total()}},
        {{earthLang('base.showing_page')}}: {{$items->currentPage()}} / {{$items->lastPage()}}
    </div>

    <!-- List group -->
    <ul class="list-group">
        @foreach($items as $item)
            <li class="list-group-item">
                <div class="pull-left">
                    <a href="{{earthRouteWithVars($route.'.texts.getAttach', [$id]) . earthRequestVars( request()->except(['text_id']) + ['text_id' => $item->id] ) }}"
                        data-toggle="content"
                        data-refresh-container=".attach_container"
                        data-push-state="false">
                        <p class="shortable" title="{{$item->name}}">
                            {{\Illuminate\Support\Str::limit($item->name, 40)}}
                        </p>
                    </a>
                    <span class="mr-10">
                        <small><i class="fa fa-calendar"></i> {{$item->created_at->format("d.m.Y")}}</small>
                    </span>
                    <span class="mr-10">
                        <small>{{earthLang('texts.type')}}: {{$item->type}}</small>
                    </span>
                </div>

                <div class="pull-right">
                    <a href="{{earthRouteWithVars($route.'.texts.getAttach', [$id]) . earthRequestVars( request()->except(['text_id']) + ['text_id' => $item->id] ) }}"
                       class="btn btn-xs btn-default"
                       data-toggle="content"
                       data-refresh-container=".attach_container"
                       data-push-state="false">
                        <i class="fa fa-fw fa-plus"></i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </li>
        @endforeach
    </ul>
</div>

@include( earthView('boilerplate.paginationSimple', true), [
    'paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ),
    'refreshContainer' => '.attach_container',
    'pushState' => 'false'
] )