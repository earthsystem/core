@extends( $extendView )

@section('title', earthLang('elements.add_text'))
@section('app')
    <div class="attach_container">
        @include( earthView('texts.attach.list', true) )
    </div>
@stop
@section('footer')
    <a href="#" class="btn btn-link close-modal">{{earthLang('base.modal_close')}}</a>
@stop