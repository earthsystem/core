<div class="panel panel-default mt-20">

    <div class="panel-heading">
        <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
    </div>

    <!-- List group -->
    <ul class="list-group">
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('form.name')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->name}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('texts.type')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->type}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.image')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    @if ($item->hasImage())
                        <a href="{{$item->image->url('large')}}" class="popup">
                            <img src="{{$item->image->url('thumb')}}" alt="" class="img-responsive img-thumbnail">
                        </a>
                    @endif
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.created_at')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->created_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.created_by')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->createdByName}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.last_modified')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->updated_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.updated_by')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->updatedByName}}
                </div>
            </div>
        </li>
    </ul>

    <div class="panel-footer">
        @if (canWrite())
            <a href="{{earthRouteWithVars($route.'.getEdit', [$item->id]) . earthRequestVars()}}"
               data-toggle="content"
               class="btn btn-primary">
                <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
            </a>

            @if (canDelete())
                <a href="{{earthRouteWithVars($route.'.delete', [$item->id]) . earthRequestVars()}}"
                   data-csrf-token="{{csrf_token()}}"
                   data-refresh-uri="{{earthRoute($route.'.getIndex') . earthRequestVars()}}"
                   data-title="{{earthLangWithVars('texts.delete', ['name' => $item->name])}}"
                   data-deleted-title="{{earthLangWithVars('texts.deleted', ['name' => $item->name])}}"
                   data-not-deleted-title="{{earthLangWithVars('texts.not_deleted', ['name' => $item->name])}}"
                   class="delete btn btn-danger">
                    <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                </a>
            @endif
        @endif
    </div>

</div>