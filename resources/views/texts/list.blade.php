@include( earthView('boilerplate.createButton', true), ['createUri' => $createUri, 'createLabel' => earthLang('texts.create') ] )

<!-- search and filters -->
<div class="row">
    <div class="col-lg-{{$textTypes->count() > 1 ? 8 : 10}} col-md-{{$textTypes->count() > 1 ? 8 : 10}} col-sm-{{$textTypes->count() > 1 ? 8 : 10}} col-xs-12">
        @include( earthView('boilerplate.searchBar', true), ['baseRoute' => earthRoute($route.'.getIndex')])
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
        @include( earthView('boilerplate.sortButton', true), [
            'title' => earthLang('base.sort'),
            'baseRoute' => earthRoute($route.'.getIndex'),
            'sortFields' => [
                ['title' => earthLang('base.created_at'), 'value' => 'created_at-desc'],
                ['title' => earthLang('base.name'), 'value' => 'name'],
                ['title' => earthLang('base.last_modified'), 'value' => 'updated_at-desc'],
            ]
        ])
    </div>

    @if ( $textTypes->count() > 1 )
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            @include( earthView('boilerplate.filterButton', true), [
                'title' => earthLang('texts.type'),
                'baseRoute' => earthRoute($route.'.getIndex'),
                'filterFields' => $textTypes
            ])
        </div>
    @endif

</div>

@include( earthView('boilerplate.pagination', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )

<div class="list-group list-view-pf mt-20">
    @foreach($items as $item)
        <div class="list-group-item">
            <div class="list-view-pf-actions">
                <div class="dropdown pull-right dropdown-kebab-pf">
                    <button class="btn btn-link dropdown-toggle" type="button" id="dropdownKebabRight{{$item->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="fa fa-ellipsis-v"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownKebabRight{{$item->id}}">
                        @if (canWrite())
                            <li>
                                <a href="{{earthRouteWithVars($route.'.getEdit', [$item->id]) . earthRequestVars()}}" data-toggle="content">
                                    <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
                                </a>
                            </li>
                        @endif

                        @if (canDelete())
                            <li>
                                <a href="{{earthRouteWithVars($route.'.delete', [$item->id]) . earthRequestVars()}}"
                                   data-csrf-token="{{csrf_token()}}"
                                   data-refresh-uri="{{$listUri.earthRequestVars()}}"
                                   data-title="{{earthLangWithVars('texts.delete', ['name' => $item->name])}}"
                                   data-deleted-title="{{earthLangWithVars('texts.deleted', ['name' => $item->name])}}"
                                   data-not-deleted-title="{{earthLangWithVars('texts.not_deleted', ['name' => $item->name])}}"
                                   class="delete">
                                    <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="list-view-pf-main-info">
                <div class="list-view-pf-left">
                    <a href="{{earthRouteWithVars($route.'.getShow', [$item->id]) . earthRequestVars()}}" title="{{$item->name}}" data-toggle="content">
                        @if ($item->hasImage())
                            <img src="{{$item->image->url('thumb')}}" alt="" class="img-circle list-img">
                        @else
                            <div class="img-circle list-img"></div>
                        @endif
                    </a>
                </div>
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-heading">
                            <a href="{{earthRouteWithVars($route.'.getShow', [$item->id]) . earthRequestVars()}}" title="{{$item->name}}" data-toggle="content">
                                {{$item->name}}
                            </a>
                        </div>
                    </div>
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <i class="fa fa-calendar fa-fw"></i>
                            {{$item->created_at->format("d.m.Y")}}
                        </div>
                        <div class="list-view-pf-additional-info-item">
                            <i class="fa fa-user fa-fw"></i>
                            {{$item->createdByName or earthLang('users.no_name')}}
                        </div>
                        <div class="list-view-pf-additional-info-item hidden-xs">
                            {{earthLang('texts.type')}}:
                            {{$item->type}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@include( earthView('boilerplate.pagination', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )