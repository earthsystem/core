<div class="list-group mt-20">
    @foreach($items as $sidebarItem)
        <a href="{{earthRouteWithVars($route.'.getShow', [$sidebarItem->id]) . earthRequestVars()}}"
           class="list-group-item @if($sidebarItem->id == $item->id) active @endif"
           data-toggle="content">
            <p class="list-group-item-heading shortable" title="{{$sidebarItem->name}}">
                {{$sidebarItem->id}}. {{$sidebarItem->name}}
            </p>
            <p class="list-group-item-text shortable" title="{{$sidebarItem->name}}">
                <small><i class="fa fa-fw fa-calendar"></i> {{$sidebarItem->created_at->format("d.m.Y")}}</small>
            </p>
        </a>
    @endforeach
</div>

@include( earthView('boilerplate.paginationSimple', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )