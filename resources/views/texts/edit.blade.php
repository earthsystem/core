{!! Form::model($item, ['url' => earthRouteWithVars($route.'.putUpdate', [$item->id]) . earthRequestVars(), 'method' => 'put', 'class' => 'ajax-form', 'files' => true]) !!}

<div class="panel panel-default mt-20">
    <div class="panel-heading">
        <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
    </div>
    <div class="panel-body">

        {!! formRules(\Earth\Core\Entities\Text\Text::$singleFormRules) !!}

        @if ($dontShowTabs)
            <span class="meta-data" data-refresh-uri="{{earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] )}}"></span>
        @else
            <span class="meta-data" data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id]).earthRequestVars()}}"></span>
        @endif


        <div class="form-group">
            <label for="name">{{earthLang('form.name')}}</label>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => earthLang('form.name')]) !!}
        </div>

        <div class="form-group">
            <label for="type">{{earthLang('texts.type')}}</label>
            {!! Form::select('type', $types, null, ['data-placeholder' => earthLang('texts.type'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5><strong>{{earthLang('base.image')}}</strong></h5>
                @if ($item->hasImage())
                    <a href="{{$item->image->url('large')}}" class="popup">
                        <img src="{{$item->image->url('thumb')}}" alt="" class="img-responsive img-thumbnail">
                    </a>
                @endif

                <div class="form-group">
                    <label class="checkbox checkbox-inline m-r-20">
                        {!!Form::checkbox('delete_image', 1)!!}
                        <i class="input-helper"></i>
                        {{earthLang('base.delete_image')}}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="image">{{earthLang('base.image')}}</label>
            {!! Form::file('image', ['class' => 'fileInput']) !!}
        </div>

        <div class="form-group">
            <label for="created_at">{{earthLang('base.created_at')}}</label>
            <div class="input-group date">
                <input type="text"
                       class="form-control date-picker"
                       name="created_at"
                       placeholder="{{earthLang('base.created_at')}}"
                       value="{{$item->created_at->format("d.m.Y")}}">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>

    </div>
    <div class="panel-footer">

        @if ($dontShowTabs)
            {!! editButtons( earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ) ) !!}
        @else
            {!! editButtons(earthRouteWithVars($route.'.getShow', [$item->id]).earthRequestVars()) !!}
        @endif

    </div>
</div>

{!! Form::close() !!}