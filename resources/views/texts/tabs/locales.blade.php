<div id="locales">
    <div class="panel panel-default mt-20">
        <div class="panel-heading">

            <div class="pull-right">
                <a href="{{earthRouteWithVars($route.'.locales.getCreate', [$item->id])}}"
                   class="btn btn-success btn-xs"
                   data-toggle="content">
                    <i class="fa fa-fw fa-plus"></i>
                    {{earthLang('base.add_locale')}}
                </a>
            </div>

            <h3 class="panel-title">{{earthLang('base.locales')}} ({{$item->id}}. {{$item->name}})</h3>
        </div>

        @if ($localeItems = $item->locales()->get() and $localeItems->count())
            <table class="table">
                <thead>
                <tr>
                    @if (canDelete())
                        <th>
                            <input type="checkbox" class="toggleCheckedInNearestTable">
                        </th>
                    @endif
                    <th>{{earthLang('form.locale')}}</th>
                    <th>{{earthLang('form.title')}}</th>
                    <th class="text-right">
                        @if (canDelete())
                            <a href="{{earthRouteWithVars($route.'.locales.delete', [$item->id, ":id_placeholder"]) . earthRequestVars()}}"
                               data-csrf-token="{{csrf_token()}}"
                               data-deleted-title="{{earthLang('base.items_deleted')}}"
                               data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                               data-refresh-uri="{{earthRouteWithVars($route.".getShow", [$item->id, "locales"]) . earthRequestVars()}}"
                               class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                                <i class="fa fa-trash-o"></i>
                                {{earthLang('base.delete_all')}}
                            </a>
                        @endif
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($localeItems as $locale)
                    <tr>
                        @if (canDelete())
                            <td>
                                <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$locale->id}}">
                            </td>
                        @endif
                        <td>
                            <a href="#" {!! editable()
                                ->select()
                                ->name("locale")
                                ->primaryKey($locale->id)
                                ->title( earthLang('form.locale') )
                                ->source( json_encode($locales) )
                                ->value($locale->locale)
                                ->uri( earthRouteWithVars($route.".locales.putUpdate", [$item->id, $locale->id]) )
                                ->render() !!}>
                                {{get_locale_name($locales, $locale->locale)}}
                            </a>
                        </td>
                        <td>
                            <a href="#" {!! editable()
                                ->text("put")
                                ->name("title")
                                ->primaryKey($locale->id)
                                ->title( earthLang('form.title') )
                                ->uri( earthRouteWithVars($route.".locales.putUpdate", [$item->id, $locale->id]) )
                                ->render() !!}>
                                {{$locale->title}}
                            </a>
                        </td>
                        <td class="text-right">
                            @if (canWrite())
                                <a href="{{earthRouteWithVars($route.'.locales.getEdit', [$item->id, $locale->id]) . earthRequestVars()}}"
                                   class="btn btn-xs btn-default"
                                   data-toggle="content">
                                    <i class="fa fa-fw fa-edit"></i>
                                    {{earthLang('base.edit')}}
                                </a>
                                <a href="{{earthRouteWithVars('attachments.getShow', [$locale->id, get_class($locale), "gallery"]) . earthRequestVars()}}"
                                   class="btn btn-xs btn-default"
                                   data-open-in-modal="true"
                                   data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "locales"])}}"
                                   data-modal-size="modal-lg"
                                   data-refresh-type="soft">
                                    <i class="fa fa-fw fa-image"></i>
                                    {{earthLang('base.gallery')}}
                                </a>
                                <a href="{{earthRouteWithVars('attachments.getShow', [$locale->id, get_class($locale), "files"]) . earthRequestVars()}}"
                                   class="btn btn-xs btn-default"
                                   data-open-in-modal="true"
                                   data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "locales"])}}"
                                   data-modal-size="modal-lg"
                                   data-refresh-type="soft">
                                    <i class="fa fa-fw fa-file-o"></i>
                                    {{earthLang('base.files')}}
                                </a>
                                @if (canDelete())
                                    <a href="{{earthRouteWithVars($route.'.locales.delete', [$item->id, $locale->id]) . earthRequestVars()}}"
                                       data-csrf-token="{{csrf_token()}}"
                                       data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "locales"]) . earthRequestVars()}}"
                                       data-title="{{earthLangWithVars('base.delete_locale_title', ['title' => $locale->title])}}"
                                       data-deleted-title="{{earthLangWithVars('base.deleted_locale', ['title' => $locale->title])}}"
                                       data-not-deleted-title="{{earthLangWithVars('base.not_deleted_locale', ['title' => $locale->title])}}"
                                       class="delete btn btn-xs btn-danger">
                                        <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete_locale')}}
                                    </a>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>