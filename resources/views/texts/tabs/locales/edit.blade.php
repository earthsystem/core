 {!! Form::model($locale, ['method' => 'put', 'url' => earthRouteWithVars($route.'.locales.putUpdate', [$id, $localeId]), 'class' => 'ajax-form']) !!}

 <div class="panel panel-default mt-20">
     <div class="panel-heading">
         <h3 class="panel-title">
             <!-- title and back button -->
             @if ( request()->has("rel_route") )
                 {!! backButton( earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ) ) !!}
             @else
                 {!! backButton( earthRouteWithVars($route.".getShow", [$id, "locales"]).earthRequestVars() ) !!}
             @endif

             {{earthLang('base.edit_locale')}}
         </h3>
     </div>
     <div class="panel-body">

         {!! formRules(\Earth\Core\Entities\Text\Locale::$formRules) !!}

         @if ( request()->has("rel_route") )
             <span class="meta-data" data-refresh-uri="{{earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] )}}"></span>
         @else
             <span class="meta-data" data-refresh-uri="{{earthRouteWithVars($route.".getShow", [$id, "locales"]).earthRequestVars()}}"></span>
         @endif

         <div class="form-group">
             <label for="active" class="checkbox-inline">
                 {!!Form::checkbox('active', 1, null, ['class' => 'bsToggle'])!!}
                 {{earthLang('form.active')}}
             </label>
         </div>

         <div class="form-group">
             <label for="title">{{earthLang('form.title')}}</label>
             {!! Form::text('title', null, ['placeholder' => earthLang('form.title'), 'class' => 'form-control']) !!}
         </div>

         <div class="form-group">
             <label for="type">{{earthLang('base.locale')}}</label>
             {!! Form::select('locale', $locales, null, ['data-placeholder' => earthLang('base.locale'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
         </div>

         <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::textarea('content', $locale->data, ['class' => 'form-control wysiwyg mb-20', 'rows' => 15]) !!}
             </div>
         </div>

         <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                 <div class="panel-group" id="excerpts" role="tablist">
                     <div class="panel panel-default">
                         <div class="panel-heading" role="tab" id="excerptHeading">
                             <h4 class="panel-title">
                                 <a role="button" data-toggle="collapse" data-parent="#excerpts" href="#excerptsCollapse" aria-expanded="true" aria-controls="excerptCollapse">
                                     {{earthLang('texts.excerpts')}}
                                 </a>
                             </h4>
                         </div>

                         <div id="excerptsCollapse" class="panel-collapse collapse" role="tabpanel">
                             <div class="panel-body">

                                 <div class="form-group">
                                     <label for="subtitle">{{earthLang('texts.subtitle')}}</label>
                                     {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => earthLang('texts.subtitle')]) !!}
                                 </div>

                                 <div class="form-group">
                                     <label for="summary">{{earthLang('texts.summary')}}</label>
                                     {!! Form::textarea('summary', null, ['class' => 'form-control wysiwygSimple mb-20', 'rows' => 3]) !!}
                                 </div>

                             </div>
                         </div>

                     </div>
                 </div>

             </div>
         </div>

         <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                 <div class="panel-group" id="dates" role="tablist">
                     <div class="panel panel-default">
                         <div class="panel-heading" role="tab" id="datesHeading">
                             <h4 class="panel-title">
                                 <a role="button" data-toggle="collapse" data-parent="#dates" href="#datesCollapse" aria-expanded="true" aria-controls="excerptCollapse">
                                     {{earthLang('texts.dates')}}
                                 </a>
                             </h4>
                         </div>

                         <div id="datesCollapse" class="panel-collapse collapse" role="tabpanel">
                             <div class="panel-body">

                                 <div class="row">
                                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                             <label for="created_at">{{earthLang('base.publish_from')}}</label>
                                             <div class="input-group date">
                                                 <input type="text" class="form-control date-picker" name="publish_from" placeholder="{{earthLang('base.publish_from')}}" value="{{isset($item->publish_from) ? $item->publish_from->format("d.m.Y") : ""}}">
                                                 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                             <label for="created_at">{{earthLang('base.publish_to')}}</label>
                                             <div class="input-group date">
                                                 <input type="text" class="form-control date-picker" name="publish_to" placeholder="{{earthLang('base.publish_to')}}" value="{{isset($item->publish_to) ? $item->publish_to->format("d.m.Y") : ""}}">
                                                 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                             </div>
                         </div>

                     </div>
                 </div>

             </div>
         </div>

     </div>

     <div class="panel-footer">
         @if ( request()->has("rel_route") )
             {!! editButtons( earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ) ) !!}
         @else
             {!! editButtons( earthRouteWithVars($route.'.getShow', [$id, "locales"]).earthRequestVars() ) !!}
         @endif
     </div>

 </div>

{!! Form::close() !!}