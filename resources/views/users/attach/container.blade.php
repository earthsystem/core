@extends( $extendView )

@section('title', earthLang('roles.add_user'))
@section('app')
    <div class="attach_container">
        @include( earthView('users.attach.list', true) )
    </div>
@stop
@section('footer')
    <a href="#" class="btn btn-link close-modal">{{earthLang('base.modal_close')}}</a>
@stop