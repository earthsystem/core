@extends( earthView('ajax', true) )

@section('title', earthLang('users.switch_user'))

@section('app')
    {!! Form::open(['method' => 'post', 'url' => earthRoute('postSwitchUser')]) !!}
        <div class="form-group">
            <label for="user_id">{{earthLang('users.pick_a_user')}}</label>
            {!! Form::select('user_id', $users, $aliasUserId, [
                'class' => 'form-control',
                'data-selectize' => 'true',
                'data-placeholder' => earthLang('users.pick_a_user')
            ]) !!}
        </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <button type="button" class="btn btn-success" id="submit-modal-form">{{earthLang('form.submit')}}</button>
    <button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>
@stop