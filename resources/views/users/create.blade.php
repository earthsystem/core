{!! Form::open(['url' => earthRoute('users.postStore') . earthRequestVars(), 'method' => 'post', 'class' => 'ajax-form']) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <!-- title and back button -->
                {!! backButton($listUri.earthRequestVars()) !!}

                {{earthLang('users.create')}}
            </h3>
        </div>
        <div class="panel-body">

            <span class="meta-data" data-refresh-uri="{{$listUri.earthRequestVars()}}"></span>
            {!! formRules(\Earth\Core\Entities\User\User::$formRules + ['password' => 'required']) !!}

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="first_name">{{earthLang('users.first_name')}}</label>
                        {!! Form::text('first_name', null, ['placeholder' => earthLang('users.first_name'), 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="last_name">{{earthLang('users.last_name')}}</label>
                        {!! Form::text('last_name', null, ['placeholder' => earthLang('users.last_name'), 'class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="email">{{earthLang('users.email')}}</label>
                        {!! Form::email('email', null, ['placeholder' => earthLang('users.email'), 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="username">{{earthLang('users.username')}}</label>
                        {!! Form::text('username', null, ['placeholder' => earthLang('users.username'), 'class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="type">{{earthLang('users.type')}}</label>
                        {!! Form::select('type', $types, null, [
                                'placeholder' => earthLang('users.type'),
                                'class' => 'form-control',
                                'data-selectize' => 'true',
                                'data-placeholder' => earthLang('users.pick_a_type')
                            ]) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="password">{{earthLang('users.password')}}</label>
                        {!! Form::password('password', ['placeholder' => earthLang('users.password'), 'class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            {!! createButtons($listUri.earthRequestVars()) !!}
        </div>
    </div>
{!! Form::close() !!}