@extends( earthView('ajax', true) )

@section('title', earthLang('users.change_password'))

@section('app')
    {!! Form::open(['method' => 'post', 'url' => earthRoute('postChangePassword')]) !!}

        {!! formRules(['new_password' => 'required', 'confirm_new_password' => 'required']) !!}

        <div class="form-group">
            <label for="new_password">{{earthLang('users.new_password')}}</label>
            {!! Form::password('new_password', ['placeholder' => earthLang('users.new_password'), 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="confirm_new_password">{{earthLang('users.confirm_new_password')}}</label>
            {!! Form::password('confirm_new_password', ['placeholder' => earthLang('users.confirm_new_password'), 'class' => 'form-control']) !!}
        </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <button type="button" class="btn btn-success" id="submit-modal-form">{{earthLang('form.submit')}}</button>
    <button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>
@stop