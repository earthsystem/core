<div class="panel panel-default mt-20">
    <div class="panel-heading">

        <div class="pull-right">
            <a href="{{earthRouteWithVars('users.roles.getCreate', [$item->id])}}"
               class="btn btn-success btn-xs"
                data-open-in-modal="true"
                data-refresh-type="soft"
                data-refresh-uri="{{earthRouteWithVars('users.getShow', [$item->id, "roles"]).earthRequestVars()}}">
                <i class="fa fa-fw fa-plus"></i>
                {{earthLang('users.add_role')}}
            </a>
        </div>

        <h3 class="panel-title">{{earthLang('base.roles')}} ({{$item->id}}. {{$item->fullName}})</h3>
    </div>
    <table class="table">
        <thead>
            <tr>
                @if (canDelete() and $item->can_modify)
                    <th>
                        <input type="checkbox" class="toggleCheckedInNearestTable">
                    </th>
                @endif
                <th>{{earthLang('roles.name')}}</th>
                <th class="text-right">
                    @if (canDelete() and $item->can_modify)
                        <a href="{{earthRouteWithVars('users.roles.delete', [$item->id, ":id_placeholder"]) . earthRequestVars()}}"
                           data-csrf-token="{{csrf_token()}}"
                           data-deleted-title="{{earthLang('base.items_deleted')}}"
                           data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                           data-refresh-uri="{{earthRouteWithVars("users.getShow", [$item->id, "roles"]) . earthRequestVars()}}"
                           class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                            <i class="fa fa-trash-o"></i>
                            {{earthLang('base.delete_all')}}
                        </a>
                    @endif
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($item->roles()->get() as $role)
                <tr>
                    @if (canDelete() and $item->can_modify)
                        <td>
                            <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$role->id}}">
                        </td>
                    @endif
                    <td>{{$role->name}}</td>
                    <td class="text-right">
                        @if (canWrite() and $role->can_modify)
                            <a href="{{earthRouteWithVars('roles.getEdit', [$role->id]) . earthRequestVars( request()->all() + ["rel_id" => $item->id, "rel_route" => "users.getShow", "rel_key" => "roles"] )}}"
                               class="btn btn-xs btn-default"
                               data-toggle="content">
                                <i class="fa fa-fw fa-edit"></i>
                                {{earthLang('base.edit')}}
                            </a>
                        @endif
                        @if (canDelete() and !(!$role->can_modify and !$item->can_modify) )
                            <a href="{{earthRouteWithVars('users.roles.delete', [$item->id, $role->id]) . earthRequestVars()}}"
                               data-csrf-token="{{csrf_token()}}"
                               data-refresh-uri="{{earthRouteWithVars('users.getShow', [$item->id, "roles"]) . earthRequestVars()}}"
                               data-title="{{earthLangWithVars('users.delete_role_title', ['name' => $item->full_name, 'role' => $role->name])}}"
                               data-deleted-title="{{earthLangWithVars('users.deleted_role', ['name' => $item->full_name, 'role' => $role->name])}}"
                               data-not-deleted-title="{{earthLangWithVars('users.not_deleted_role', ['name' => $item->full_name, 'role' => $role->name])}}"
                               class="delete btn btn-xs btn-danger">
                                <i class="fa fa-fw fa-trash-o"></i> {{earthLang('users.delete_role')}}
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>