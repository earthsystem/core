<div class="list-group mt-20">
    @foreach($items as $sidebarItem)
        <a href="{{earthRouteWithVars('users.getShow', [$sidebarItem->id]) . earthRequestVars()}}"
           class="list-group-item @if($sidebarItem->id == $item->id) active @endif"
           data-toggle="content">
            <p class="shortable" title="{{$sidebarItem->full_name}}">{{$sidebarItem->id}}. {{$sidebarItem->full_name}}</p>
        </a>
    @endforeach
</div>

@include( earthView('boilerplate.paginationSimple', true), ['paginator' => $items->appends( request()->except(['page', 'ajax', 'ajax_process']) ) ] )