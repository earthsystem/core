<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="token" content="{{csrf_token()}}">
    <title>{{$pageTitle or ''}}</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.bootstrap3.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/1.5.2/css/ionicons.css">
    <link rel="stylesheet" href="/vendor/earth/core/vendor/mfb/mfb.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/css/jquery.treegrid.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.1/css/bootstrap-toggle.min.css">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.6/sandstone/bootstrap.min.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.1/css/fileinput.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{earthAsset('css/main.css')}}">

@yield('system_layout_css')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@include(earthView('partials.header', true))

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
            <div class="jumbotron">
                <h1><i class="fa fa-exclamation-circle"></i> {{earthLang('not_authorized.title')}}</h1>
                <p>{{earthLang('not_authorized.paragraph')}}</p>
                <p><small>{!! earthLang('not_authorized.paragraph_detail') !!}</small></p>
            </div>
        </div>
    </div>
</div>

<!-- modal -->
<div class="modal fade" id="earthModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{earthLang('base.modal_title')}}</h4></div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- // modal -->

<!-- popstate placeholder -->
<a href="" id="popstatePlaceholder" data-toggle="content"></a>
<!-- // popstate placeholder -->

<script>
    var modalTitle = '{{earthLang('base.modal_title')}}';
    var modalErrorTitle = '{{earthLang('base.modal_error_title')}}';
    var modalErrorContent = '{!! earthLang('base.modal_error_content') !!}';
    var modalCloseButton = '<button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang("base.modal_close")}}</button>';
    var thisFieldIsRequiredText = '{{earthLang('form.this_field_is_required')}}';
    var errorNotificationTitle = '{{earthLang('base.error_notification_title')}}';
    var successNotificationTitle = '{{earthLang('base.success_notification_title')}}';
    var deleteAlertTitle = '{{earthLang('base.delete_alert_title')}}';
    var deleteItemsAlertTitle = '{{earthLang('base.delete_items_alert_title')}}';
    var deleteAlertButtonText = '{{earthLang('base.delete_alert_button_text')}}';
    var deleteAlertCancelButtonText = '{{earthLang('form.cancel')}}';
    var deleteAlertItemDeletedTitle = '{{earthLang('base.delete_item_deleted_title')}}';
    var deleteAlertItemNotDeletedTitle = '{{earthLang('base.delete_item_not_deleted_title')}}';
    var itemNotSortedTitle = '{{earthLang('base.item_not_sorted_title')}}';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.min.js"></script>
<script src="/vendor/earth/core/vendor/skaterdav85/validatorjs/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/history.js/1.8/bundled-uncompressed/html4+html5/jquery.history.js"></script>
<script src="/vendor/earth/core/vendor/LoadingOverlay.js"></script>
<script src="/vendor/earth/core/vendor/BootstrapNotify.js"></script>
<script src="/vendor/earth/core/vendor/mfb/mfb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/0.4.5/sweet-alert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/js/jquery.treegrid.min.js"></script>
<script src="/vendor/earth/core/vendor/SelectableAndSortable.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.1/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.1/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script src='/vendor/earth/core/vendor/tinymce/tinymce.min.js'></script>

<script src="{{earthAsset('js/main.js')}}"></script>
<script src="{{earthAsset('js/modal.js')}}"></script>
<script src="{{earthAsset('js/content.js')}}"></script>
<script src="{{earthAsset('js/form.js')}}"></script>
<script src="{{earthAsset('js/checkable.js')}}"></script>

@yield('system_layout_js')
</body>

</html>