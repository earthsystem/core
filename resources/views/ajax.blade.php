<div class="modal-header">
    <h4 class="modal-title">@yield('title')</h4>
</div>
<div class="modal-body">@yield('app')</div>
<div class="modal-footer">@yield('footer')</div>

@yield('ajax_css')
@yield('ajax_js')