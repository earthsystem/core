<div class="panel panel-default mt-20">
    <div class="panel-heading">
        <h3 class="panel-title">{{earthLang('base.custom-data')}} ({{$item->id}}. {{$item->name}})</h3>
    </div>

    @if ($customData = $item->custom_data()->get() and $customData->count())
        <table class="table">
            <thead>
                <tr>
                    @if (canDelete())
                        <th>
                            <input type="checkbox" class="toggleCheckedInNearestTable">
                        </th>
                    @endif
                    <th>{{earthLang('base.key')}}</th>
                    <th>{{earthLang('base.value')}}</th>
                    <th class="text-right">
                        @if (canDelete())
                            <a href="{{earthRouteWithVars('data.delete', [":id_placeholder"]) . earthRequestVars()}}"
                               data-csrf-token="{{csrf_token()}}"
                               data-deleted-title="{{earthLang('base.items_deleted')}}"
                               data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                               data-refresh-uri="{{route($routePrefix.".".$route.".getShow", [$item->id, "custom-data"]) . earthRequestVars()}}"
                               class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                                <i class="fa fa-trash-o"></i>
                                {{earthLang('base.delete_all')}}
                            </a>
                        @endif
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($customData as $data)
                    <tr>
                        @if (canDelete())
                            <td>
                                <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$data->id}}">
                            </td>
                        @endif
                        <td>
                            <a href="#" {!! editable()
                                ->text("put")
                                ->name("customable_key")
                                ->primaryKey($data->id)
                                ->title( earthLang('base.key') )
                                ->uri( earthRouteWithVars("data.putUpdate", [$data->id]) )
                                ->render() !!}>
                                {{$data->key}}
                            </a>
                        </td>
                        <td>
                            <a href="#" {!! editable()
                                ->text("put")
                                ->name("customable_key")
                                ->primaryKey($data->id)
                                ->title( earthLang('base.value') )
                                ->uri( earthRouteWithVars("data.putUpdate", [$data->id]) )
                                ->render() !!}>
                                {{$data->value}}
                            </a>
                        </td>
                        <td class="text-right">
                            @if (canDelete())
                                <a href="{{earthRouteWithVars('data.delete', [$data->id]) . earthRequestVars()}}"
                                   data-csrf-token="{{csrf_token()}}"
                                   data-refresh-uri="{{route($routePrefix.".".$route.".getShow", [$item->id, "custom-data"]) . earthRequestVars()}}"
                                   data-title="{{earthLangWithVars('base.delete_data_title', ['title' => $data->key])}}"
                                   data-deleted-title="{{earthLangWithVars('base.deleted_data', ['title' => $data->key])}}"
                                   data-not-deleted-title="{{earthLangWithVars('base.not_deleted_data', ['title' => $data->key])}}"
                                   class="delete btn btn-xs btn-danger">
                                    <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete_data')}}
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    <div class="panel-footer">
        {!! Form::open(['url' => earthRoute("data.postStore"), 'method' => 'post', 'class' => 'ajax-form']) !!}

            <span class="meta-data" data-refresh-uri="{{route($routePrefix.".".$route.".getShow", [$item->id, "custom-data"]) . earthRequestVars()}}"></span>
            {!! Form::hidden('customable_id', $item->id) !!}
            {!! Form::hidden('customable_type', get_class($item)) !!}

            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    {!! Form::text('key', null, ['class' => 'form-control', 'placeholder' => earthLang('base.key') ]) !!}
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => earthLang('base.value') ]) !!}
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <button type="submit" class="btn btn-success btn-block">
                        {{earthLang('form.save')}}
                    </button>
                </div>
            </div>

        {!! Form::close() !!}
    </div>

</div>