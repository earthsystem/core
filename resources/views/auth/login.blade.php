<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>{{$pageTitle}}</title>

        <!-- Bootstrap core CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.3.4/css/patternfly.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.3.4/css/patternfly-additions.min.css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <!-- Login styles -->
        <link rel="stylesheet" href="{{earthAsset('css/login.css')}}">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <form class="form-signin" method="post" action="{{earthRoute('postLogin', 'earth')}}">

                {!! csrf_field() !!}

                <h2 class="form-signin-heading">{!! earthLang('login.title') !!}</h2>

                <div class="errorResponsePlaceholder alert alert-danger hidden"
                    data-missing-arguments-text="{{ earthLang('login.missing_arguments') }}"
                    data-error-processing-sign-in-text="{{ earthLang('login.error_processing_sign_in') }}">
                </div>
                
                <div class="successResponsePlaceholder alert alert-success hidden">
                    <i class="fa fa-spinner fa-spin"></i>
                    {{ earthLang('login.sign_in_successful') }}
                </div>

                <label for="inputEmail" class="sr-only">{{ earthLang('login.email_title') }}</label>
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="{{ earthLang('login.email_title') }}" autofocus>
                <label for="inputPassword" class="sr-only">{{ earthLang('login.password_title') }}</label>
                <input name="password" type="password" id="inputPassword" class="form-control" placeholder="{{ earthLang('login.password_title') }}">
                <button class="btn btn-lg btn-primary btn-block" data-loading-text="{{ earthLang('login.sign_in_loading') }}">
                    {{ earthLang('login.sign_in_normal') }}
                </button>
            </form>

        </div> <!-- /container -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.3.4/js/patternfly.min.js"></script>
        <script src="{{earthAsset('js/login.js')}}"></script>

    </body>
</html>
