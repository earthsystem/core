@extends( earthView('ajax', true) )

@section('title', earthLangWithVars('elements.create_new_template_title', ['position' => $position]))

@section('app')
    {!! Form::open(['method' => 'post', 'url' => earthRouteWithVars($route.'.templates.postStore', [$id, $position])]) !!}

        {!! formRules(\Earth\Core\Entities\Template\Template::$formRules) !!}

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="active" class="checkbox-inline">
                        {!!Form::checkbox('active', 1, true, ['class' => 'bsToggle'])!!}
                        {{earthLang('form.active')}}
                    </label>
                </div>

                <div class="form-group">
                    <label for="recursive" class="checkbox-inline">
                        {!!Form::checkbox('recursive', 1, true, ['class' => 'bsToggle'])!!}
                        {{earthLang('templates.recursive')}}
                    </label>
                </div>

                <div class="form-group">
                    <label for="name">{{earthLang('form.name')}}</label>
                    {!! Form::text('name', null, ['placeholder' => earthLang('form.name'), 'class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="path">{{earthLang('templates.path')}}</label>
                    {!! Form::text('path', null, ['placeholder' => earthLang('templates.path'), 'class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="fileTree"></div>
            </div>

        </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <button type="button" class="btn btn-success" id="submit-modal-form">{{earthLang('form.submit')}}</button>
    <button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>
@stop

@section('ajax_css')
    <link rel="stylesheet" href="/vendor/earth/core/vendor/filetree/filetree.min.css">
@stop

@section('ajax_js')
    <script src="/vendor/earth/core/vendor/filetree/filetree.min.js"></script>
    <script>
        $(document).ready(function(){

            $(".fileTree").fileTree({
                root: "/{{get_relative_path( base_path() , config('view.paths')[0] )}}",
                script: "{{earthRouteWithVars($route.".templates.postConnector", [$id])}}",
                multiFolder: false,
                expandSpeed: 1,
                collapseSpeed: 1,
                loadMessage: '...'
            }, function(file) {

                var replaceFront = "/{{get_relative_path( base_path(), config("view.paths")[0] )}}";
                var replaceBack = ".blade.php";
                var path = file;
                path = path.replace(replaceFront, "");
                path = path.replace(replaceBack, "");
                $("input[name=path]").val(path);
            });

        });
    </script>
@stop