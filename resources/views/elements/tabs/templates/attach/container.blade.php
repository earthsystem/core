@extends( $extendView )

@section('title', earthLang('templates.add_existing'))
@section('app')
    <div class="attach_container">
        @include( earthView('elements.tabs.templates.attach.list', true) )
    </div>
@stop
@section('footer')
    <a href="#" class="btn btn-link close-modal">{{earthLang('base.modal_close')}}</a>
@stop