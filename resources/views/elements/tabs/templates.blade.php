<div class="panel-group mt-20" id="templatesCollapse" role="tablist" aria-multiselectable="true">
    @foreach(get_codes('template-positions')->pluck('data') as $position)
        <div class="panel panel-default">

            <div class="panel-heading" role="tab" id="heading-{{$position}}">

                <div class="pull-right">
                    <a href="{{earthRouteWithVars($route.'.templates.getCreate', [$item->id, $position])}}"
                       class="btn btn-xs btn-default"
                       data-open-in-modal="true"
                       data-refresh-type="soft"
                       data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "templates"]).earthRequestVars( request()->except(["position"]) + ["position" => $position] )}}">
                        <i class="fa fa-plus"></i> {{earthLang('elements.create_new_template')}}
                    </a>

                    <a href="{{earthRouteWithVars($route.'.templates.getAttach', [$item->id, $position])}}"
                       class="btn btn-xs btn-default"
                       data-open-in-modal="true"
                       data-refresh-type="soft"
                       data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "templates"]).earthRequestVars( request()->except(["position"]) + ["position" => $position] )}}">
                        <i class="fa fa-plus"></i> {{earthLang('elements.add_existing_template')}}
                    </a>
                </div>

                <h3 class="panel-title">
                    <a href="#{{$position}}Collapse"
                       role="button"
                       data-toggle="collapse"
                       data-parent="#templatesCollapse"
                       aria-expanded="true"
                       aria-controls="{{$position}}Collapse">
                        {{ucfirst($position)}}
                    </a>
                </h3>
            </div>

            <div id="{{$position}}Collapse"
                 class="panel-collapse collapse
                 @if( request()->has('position') and request()->get("position") == $position) in @endif
                 @if( !request()->has("position") and $position == "main") in @endif"
                 role="tabpanel">

                @if ($templates = $item->templates()->whereType($position)->orderBy('position')->get() and $templates->count())
                    <table class="table">
                        <thead>
                        <tr>
                            @if (canDelete())
                                <th>
                                    <input type="checkbox" class="toggleCheckedInNearestTable">
                                </th>
                            @endif
                            <th>{{earthLang('form.name')}}</th>
                            <th>{{earthLang('form.active')}}</th>
                            <th>{{earthLang('templates.recursive')}}</th>
                            <th class="text-right">
                                @if (canDelete())
                                    <a href="{{earthRouteWithVars($route.'.templates.delete', [$item->id, ":id_placeholder"]) . earthRequestVars()}}"
                                       data-csrf-token="{{csrf_token()}}"
                                       data-deleted-title="{{earthLang('base.items_deleted')}}"
                                       data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                                       data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "templates"]).earthRequestVars( request()->except(["position"]) + ["position" => $position] )}}"
                                       class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                                        <i class="fa fa-trash-o"></i>
                                        {{earthLang('base.delete_all')}}
                                    </a>
                                @endif
                            </th>
                        </tr>
                        </thead>
                        <tbody class="@if (canWrite()) sortable @endif">
                        @foreach($templates as $template)
                            <tr @if (canWrite())
                                data-id="{{$template->pivot->id}}"
                                data-href="{{earthRouteWithVars($route.".templates.putSort", [$item->id, $template->pivot->id])}}"
                                data-csrf-token="{{csrf_token()}}"
                                    @endif>
                                @if (canDelete())
                                    <td>
                                        <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$template->pivot->id}}">
                                    </td>
                                @endif
                                <td>
                                    <a href="#" {!! editable()
                                                    ->text("put")
                                                    ->name("name")
                                                    ->primaryKey($template->id)
                                                    ->title( earthLang('form.name') )
                                                    ->uri( earthRouteWithVars($route.".templates.putUpdate", [$item->id, $template->id]) )
                                                    ->render() !!}>
                                        {{$template->name}}
                                    </a>
                                </td>
                                <td>
                                    @if (canWrite())
                                        {!! toggleable("active", $template->pivot->active, earthRouteWithVars("elements.templates.putUpdatePivot", [$template->id, $template->pivot->id]) ) !!}
                                    @else
                                        {!! bool_icon($template->pivot->active) !!}
                                    @endif
                                </td>
                                <td>
                                    @if (canWrite())
                                        {!! toggleable("recursive", $template->pivot->recursive, earthRouteWithVars("elements.templates.putUpdatePivot", [$template->id, $template->pivot->id]) ) !!}
                                    @else
                                        {!! bool_icon($template->pivot->recursive) !!}
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if (canWrite())
                                        <a href="{{earthRouteWithVars($route.'.templates.getEdit', [$item->id, $template->id]) . earthRequestVars()}}"
                                           class="btn btn-xs btn-default"
                                           data-open-in-modal="true"
                                           data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "templates"]).earthRequestVars( request()->except(["position"]) + ["position" => $position] )}}"
                                           data-refresh-type="soft">
                                            <i class="fa fa-fw fa-edit"></i>
                                            {{earthLang('base.edit')}}
                                        </a>

                                        @if (canDelete())
                                            <a href="{{earthRouteWithVars($route.'.templates.delete', [$item->id, $template->pivot->id]) . earthRequestVars()}}"
                                               data-csrf-token="{{csrf_token()}}"
                                               data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "templates"]).earthRequestVars( request()->except(["position"]) + ["position" => $position] )}}"
                                               data-title="{{earthLangWithVars('templates.delete', ['name' => $template->name])}}"
                                               data-deleted-title="{{earthLangWithVars('templates.deleted', ['name' => $template->name])}}"
                                               data-not-deleted-title="{{earthLangWithVars('templates.not_deleted', ['name' => $template->name])}}"
                                               class="delete btn btn-xs btn-danger">
                                                <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                                            </a>
                                        @endif

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                
            </div>

        </div>
    @endforeach
</div>