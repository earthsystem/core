<div class="panel panel-default mt-20">
    <div class="panel-heading">

        <div class="pull-right">
            <a href="{{earthRoute('texts.getCreate') . earthRequestVars( request()->except([
                "rel_id",
                "rel_key",
                "rel_route"
            ]) + ["rel_id" => $item->id, "rel_route" => $route.".getShow", "rel_key" => "texts"])}}"
               class="btn btn-success btn-xs"
               data-toggle="content">
                <i class="fa fa-fw fa-plus"></i>
                {{earthLang('texts.create_new')}}
            </a>
            <a href="{{earthRouteWithVars($route.'.texts.getAttach', [$item->id]) . earthRequestVars()}}"
               class="btn btn-default btn-xs"
                data-open-in-modal="true"
                data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "texts"]) . earthRequestVars() }}"
                data-refresh-type="soft">
                <i class="fa fa-fw fa-plus"></i>
                {{earthLang('texts.add')}}
            </a>
        </div>

        <h3 class="panel-title">{{earthLang('base.texts')}} ({{$item->id}}. {{$item->name}})</h3>
    </div>

    @if ($texts = $item->texts()->with('locales')->orderBy('position')->get() and $texts->count())
        <table class="table">
            <thead>
                <tr>
                    @if (canDelete())
                        <th>
                            <input type="checkbox" class="toggleCheckedInNearestTable">
                        </th>
                    @endif
                    <th>{{earthLang('form.name')}}</th>
                    <th>{{earthLang('texts.type')}}</th>
                    <th></th>
                    <th class="text-right">
                        @if (canDelete())
                            <a href="{{earthRouteWithVars($route.'.texts.delete', [$item->id, ":id_placeholder"]) . earthRequestVars()}}"
                               data-csrf-token="{{csrf_token()}}"
                               data-deleted-title="{{earthLang('base.items_deleted')}}"
                               data-not-deleted-title="{{earthLang('base.items_not_deleted')}}"
                               data-refresh-uri="{{earthRouteWithVars($route.".getShow", [$item->id, "texts"]) . earthRequestVars()}}"
                               class="btn btn-danger btn-xs triggerDeletesInNearestTable disabled">
                                <i class="fa fa-trash-o"></i>
                                {{earthLang('base.delete_all')}}
                            </a>
                        @endif
                    </th>
                </tr>
            </thead>
            <tbody @if(canWrite()) class="sortable" @endif>
                @foreach($texts as $text)
                    <tr @if (canWrite())
                        data-id="{{$text->pivot->id}}"
                        data-href="{{earthRouteWithVars($route.".texts.putSort", [$item->id, $text->pivot->id])}}"
                        data-csrf-token="{{csrf_token()}}"
                        @endif>
                        @if (canDelete())
                            <td>
                                <input type="checkbox" class="selectCheckbox" name="checkable[]" value="{{$text->pivot->id}}">
                            </td>
                        @endif
                        <td>
                            <a href="#" {!! editable()
                                ->text("put")
                                ->name("name")
                                ->primaryKey($text->id)
                                ->title( earthLang('form.name') )
                                ->uri( earthRouteWithVars("texts.putUpdate", [$text->id]) )
                                ->render() !!}>
                                <p class="shortable">{{\Illuminate\Support\Str::limit($text->name, 40)}}</p>
                            </a>
                        </td>
                        <td>
                            <a href="#" {!! editable()
                                ->select()
                                ->name("type")
                                ->primaryKey($text->id)
                                ->title( earthLang('texts.type') )
                                ->source( json_encode($textTypes) )
                                ->value($text->type)
                                ->uri( earthRouteWithVars("texts.putUpdate", [$text->id]) )
                                ->render() !!}>
                                {{$text->type}}
                            </a>
                        </td>
                        <td>
                            @if (canWrite())

                                @foreach($text->locales as $locale)
                                    <div class="btn-group">

                                        <a href="{{earthRouteWithVars('texts.locales.getEdit', [$text->id, $locale->id]) . earthRequestVars( request()->except([
                                                "rel_id",
                                                "rel_key",
                                                "rel_route"
                                            ]) + ["rel_id" => $item->id, "rel_route" => "elements.getShow", "rel_key" => "texts"])}}"
                                           class="btn btn-xs btn-default"
                                           data-toggle="content">
                                            {{ empty($locale->locale) ? earthLang("base.all") : $locale->locale }}
                                        </a>

                                        <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu" role="menu">
                                            
                                            <li>
                                                <a href="{{earthRouteWithVars('texts.locales.getEdit', [$text->id, $locale->id]) . earthRequestVars( request()->except([
                                                        "rel_id",
                                                        "rel_key",
                                                        "rel_route"
                                                    ]) + ["rel_id" => $item->id, "rel_route" => "elements.getShow", "rel_key" => "texts"])}}"
                                                    data-toggle="content">
                                                    <i class="fa fa-fw fa-edit"></i>
                                                    {{earthLang('base.edit')}}
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a href="{{earthRouteWithVars('attachments.getShow', [$locale->id, get_class($locale), "gallery"]) . earthRequestVars()}}"
                                                   data-open-in-modal="true"
                                                   data-refresh-uri="{{earthRouteWithVars('elements.getShow', [$item->id, "text"])}}"
                                                   data-modal-size="modal-lg"
                                                   data-refresh-type="soft">
                                                    <i class="fa fa-fw fa-image"></i>
                                                    {{earthLang('base.gallery')}}
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{earthRouteWithVars('attachments.getShow', [$locale->id, get_class($locale), "files"]) . earthRequestVars()}}"
                                                   data-open-in-modal="true"
                                                   data-refresh-uri="{{earthRouteWithVars('elements.getShow', [$item->id, "text"])}}"
                                                   data-modal-size="modal-lg"
                                                   data-refresh-type="soft">
                                                    <i class="fa fa-fw fa-file-o"></i>
                                                    {{earthLang('base.files')}}
                                                </a>
                                            </li>

                                            @if (canDelete())
                                                <li>
                                                    <a href="{{earthRouteWithVars('texts.locales.delete', [$text->id, $locale->id]) . earthRequestVars()}}"
                                                       data-csrf-token="{{csrf_token()}}"
                                                       data-refresh-uri="{{earthRouteWithVars('elements.getShow', [$item->id, "texts"]) . earthRequestVars()}}"
                                                       data-title="{{earthLangWithVars('base.delete_locale_title', ['title' => $locale->title])}}"
                                                       data-deleted-title="{{earthLangWithVars('base.deleted_locale', ['title' => $locale->title])}}"
                                                       data-not-deleted-title="{{earthLangWithVars('base.not_deleted_locale', ['title' => $locale->title])}}"
                                                       class="delete">
                                                        <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete_locale')}}
                                                    </a>
                                                </li>
                                            @endif
                                            
                                        </ul>

                                    </div>
                                @endforeach

                                    <a href="{{earthRouteWithVars('texts.locales.getCreate', [$text->id]) . earthRequestVars( request()->except([
                                            "rel_id",
                                            "rel_key",
                                            "rel_route"
                                        ]) + ["rel_id" => $item->id, "rel_route" => "elements.getShow", "rel_key" => "texts"])}}"
                                       data-toggle="content"
                                       class="btn btn-xs btn-default">
                                        <i class="fa fa-plus"></i>
                                    </a>

                            @endif
                        </td>
                        <td class="text-right">

                            <div class="btn-group">

                                <a href="{{earthRouteWithVars('texts.getEdit', [$text->id]) . earthRequestVars( request()->except([
                                        "rel_id",
                                        "rel_key",
                                        "rel_route"
                                    ]) + ["rel_id" => $item->id, "rel_route" => "elements.getShow", "rel_key" => "texts"])}}"
                                   class="btn btn-xs btn-default"
                                   data-toggle="content">
                                    <i class="fa fa-fw fa-edit"></i>
                                    {{earthLang('base.edit')}}
                                </a>

                                <a href="{{earthRouteWithVars('attachments.getShow', [$text->id, get_class($text), "gallery"]) . earthRequestVars()}}"
                                   class="btn btn-xs btn-default"
                                   data-open-in-modal="true"
                                   data-refresh-uri="{{earthRouteWithVars('elements.getShow', [$item->id, "texts"])}}"
                                   data-modal-size="modal-lg"
                                   data-refresh-type="soft">
                                    <i class="fa fa-fw fa-image"></i>
                                    {{earthLang('base.gallery')}}
                                </a>
                                <a href="{{earthRouteWithVars('attachments.getShow', [$text->id, get_class($text), "files"]) . earthRequestVars()}}"
                                   class="btn btn-xs btn-default"
                                   data-open-in-modal="true"
                                   data-refresh-uri="{{earthRouteWithVars('elements.getShow', [$item->id, "texts"])}}"
                                   data-modal-size="modal-lg"
                                   data-refresh-type="soft">
                                    <i class="fa fa-fw fa-file-o"></i>
                                    {{earthLang('base.files')}}
                                </a>

                                @if (canDelete())
                                    <a href="{{earthRouteWithVars($route.'.texts.delete', [$item->id, $text->pivot->id]) . earthRequestVars()}}"
                                       data-csrf-token="{{csrf_token()}}"
                                       data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id, "texts"]) . earthRequestVars()}}"
                                       data-title="{{earthLangWithVars('elements.delete_text_title', ['name' => $text->name])}}"
                                       data-deleted-title="{{earthLangWithVars('elements.deleted_text', ['name' => $text->name])}}"
                                       data-not-deleted-title="{{earthLangWithVars('elements.not_deleted_text', ['name' => $text->name])}}"
                                       class="delete btn btn-xs btn-default">
                                        <i class="fa fa-fw fa-trash-o"></i> {{earthLang('elements.delete_text')}}
                                    </a>
                                @endif

                            </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif


</div>