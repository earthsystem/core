@extends( earthView('ajax', true) )

@section('title', earthLang('base.edit_locale'))

@section('app')
    {!! Form::model($item, ['method' => 'put', 'url' => earthRouteWithVars($route.'.locales.putUpdate', [$id, $localeId])]) !!}

    {!! formRules(\Earth\Core\Entities\Element\Locale::$formRules) !!}
    
    <div class="form-group">
        <label for="title">{{earthLang('form.title')}}</label>
        {!! Form::text('title', null, ['placeholder' => earthLang('form.title'), 'class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="slug">{{earthLang('form.slug')}}</label>
        {!! Form::text('slug', null, ['placeholder' => earthLang('form.slug'), 'class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="locale">{{earthLang('elements.locale')}}</label>
        {!! Form::select('locale', $locales, null, ['data-placeholder' => earthLang('elements.locale'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
    </div>

    <div class="form-group">
        <label for="comment">{{earthLang('elements.comment')}}</label>
        {!! Form::textarea('comment', null, ['placeholder' => earthLang('elements.comment'), 'class' => 'form-control', 'rows' => 3]) !!}
    </div>

    {!! Form::close() !!}
@stop

@section('footer')
    <button type="button" class="btn btn-success" id="submit-modal-form">{{earthLang('form.submit')}}</button>
    <button type="button" class="btn btn-link" data-dismiss="modal">{{earthLang('base.modal_close')}}</button>
@stop