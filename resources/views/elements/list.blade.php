<table class="tree table table-bordered @if (!request()->has("q")) sort-this @endif" id="tree">
    @foreach($items as $item)
        <tr class="treegrid-{{$item->id}}
            @if ($item->depth > 1) treegrid-parent-{{$item->parent_id}} @endif
            @if (isset($opened) and in_array($item->id, $opened->all())) treegrid-opened @endif
            @if (isset($id) and $id == $item->id) treegrid-selected @endif"
            data-id="{{$item->id}}"
            data-href="{{earthRouteWithVars($route.'.putSort', [$item->id])}}"
            data-create-button-modifier="{{earthRoute($route.'.getCreate').earthRequestVars()}}&element_id={{$item->id}}"
            data-csrf-token="{{csrf_token()}}"
            data-should-reload-tree="true">
            <td>
                <a href="{{earthRouteWithVars($route.'.getShow', [$item->id]) . earthRequestVars( request()->except(['element_id']) ) }}"
                    data-toggle="content" title="{{$item->id}}. {{$item->name}}">
                    <span class="shortable">
                        {{$item->id}}. {{$item->name}}
                        @if (!$item->active) <i class="fa fa-eye-slash"></i> @endif
                    </span>
                </a>
            </td>

            @if ($actions and canWrite())
                <td class="text-right">
                    <div class="btn-group">

                        <button type="button" class="dropdown-toggle btn btn-xs btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v fa-fw"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right">
                            @if (canWrite() and $item->can_modify)
                                <li>
                                    <a href="{{earthRouteWithVars($route.'.getEdit', [$item->id]) . earthRequestVars()}}" data-toggle="content">
                                        <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
                                    </a>
                                </li>
                            @endif

                            @if (canDelete() and $item->can_modify)
                                <li>
                                    <a href="{{earthRouteWithVars($route.'.delete', [$item->id]) . earthRequestVars()}}"
                                       data-csrf-token="{{csrf_token()}}"
                                       data-refresh-uri="{{$listUri.earthRequestVars( request()->except(["element_id"]) + ["element_id" => $item->parent_id])}}"
                                       data-title="{{earthLangWithVars('elements.delete', ['name' => $item->name])}}"
                                       data-deleted-title="{{earthLangWithVars('elements.deleted', ['name' => $item->name])}}"
                                       data-not-deleted-title="{{earthLangWithVars('elements.not_deleted', ['name' => $item->name])}}"
                                       class="delete">
                                        <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                                    </a>
                                </li>
                            @endif

                            @if (canWrite())
                                <li>
                                    <a href="{{earthRoute($route.'.getCreate') . earthRequestVars( request()->except(["element_id"]) + ["element_id" => $item->id])}}"
                                       data-toggle="content">
                                        <i class="fa fa-fw fa-plus"></i>
                                        {{earthLang('elements.add_new_in_this_element')}}
                                    </a>
                                </li>
                            @endif

                        </ul>

                    </div>
                </td>
            @endif

        </tr>
    @endforeach
</table>

<a href="{{earthRoute($route.'.getTree').earthRequestVars( request()->all() + ["element_id" => isset($id) ? $id : null ] )}}"
   data-toggle="content"
   data-refresh-container=".listContainer"
   data-show-loading="false"
   data-push-state="false"
   class="refreshElementsTreeList hidden"></a>