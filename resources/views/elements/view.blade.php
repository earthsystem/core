<div class="panel panel-default mt-20">
    <div class="panel-heading">
        <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
    </div>

    <!-- List group -->
    <ul class="list-group">
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('form.active')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    @if ($item->active) <i class="fa fa-check"></i> @else <i class="fa fa-remove"></i> @endif
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('elements.name')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->name}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('elements.data')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->data}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('elements.type')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->type}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('elements.icon')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <i class="fa fa-{{$item->icon}}"></i>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('elements.parent_id')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->parentName}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.created_at')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->created_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.created_by')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->createdByName}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.last_modified')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->updated_at->format('d.m.Y H:i:s')}}
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{earthLang('base.updated_by')}}:
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    {{$item->updatedByName}}
                </div>
            </div>
        </li>
    </ul>

    <div class="panel-footer">
        @if (canWrite() and $item->can_modify)
            <a href="{{earthRouteWithVars($route.'.getEdit', [$item->id]) . earthRequestVars()}}"
               data-toggle="content"
               class="btn btn-primary">
                <i class="fa fa-fw fa-edit"></i> {{earthLang('base.edit')}}
            </a>

            @if (canDelete())
                <a href="{{earthRouteWithVars($route.'.delete', [$item->id]) . earthRequestVars()}}"
                   data-csrf-token="{{csrf_token()}}"
                   data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->parent_id]) . earthRequestVars()}}"
                   data-title="{{earthLangWithVars('elements.delete', ['name' => $item->name])}}"
                   data-deleted-title="{{earthLangWithVars('elements.deleted', ['name' => $item->name])}}"
                   data-not-deleted-title="{{earthLangWithVars('elements.not_deleted', ['name' => $item->name])}}"
                   class="delete btn btn-danger">
                    <i class="fa fa-fw fa-trash-o"></i> {{earthLang('base.delete')}}
                </a>
            @endif
        @endif
    </div>
</div>