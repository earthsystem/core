@include( earthView('boilerplate.createButton', true), ['createUri' => $createUri, 'createLabel' => earthLang('elements.create') ] )
@include( earthView('boilerplate.searchBar', true), ['baseRoute' => earthRoute('elements.getIndex')])

<div class="listContainer mt-20">
    @include( earthView('elements.list', true), ['actions' => true] )
</div>