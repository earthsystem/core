<div class="row">

    <div class="col-lg-{{$sidebarLgWidth or 3}} col-md-{{$sidebarMdWidth or 3}} col-sm-12 col-xs-12">
        <div class="sidebar toggleable hidden-sm hidden-xs">
            @include( earthView('boilerplate.searchBar', true), ['baseRoute' => earthRouteWithVars($route.'.getCreate', [$elementId])])
            @include( earthView('elements.sidebar', true))
        </div>
    </div>

    <div class="col-lg-{{$contentLgWidth or 9}} col-md-{{$contentMdWidth or 9}} col-sm-12 col-xs-12">

        {!! Form::open(['url' => earthRoute($route.'.postStore') . earthRequestVars(), 'method' => 'post', 'class' => 'ajax-form']) !!}

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <!-- title and back button -->
                        {!! backButton(isset($customListUri) ? $customListUri : $listUri.earthRequestVars()) !!}

                        {{earthLang('elements.create')}}
                    </h3>
                </div>
                <div class="panel-body">

                    {!! formRules(\Earth\Core\Entities\Element\Element::$formRules) !!}
                    <span class="meta-data" data-refresh-uri="{{isset($customListUri) ? $customListUri : $listUri.earthRequestVars()}}"></span>

                    <div class="form-group">
                        <label for="active" class="checkbox-inline">
                            {!!Form::checkbox('active', 1, true, ['class' => 'bsToggle'])!!}
                            {{earthLang('form.active')}}
                        </label>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="name">{{earthLang('elements.name')}}</label>
                                {!! Form::text('name', null, ['placeholder' => earthLang('elements.name'), 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="type">{{earthLang('elements.type')}}</label>
                                {!! Form::select('type', $types, null, ['data-placeholder' => earthLang('elements.type'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                            </div>
                            <div class="form-group">
                                <label for="icon">{{earthLang('elements.icon')}}</label>
                                {!! Form::text('icon', null, ['placeholder' => earthLang('elements.icon'), 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="data">{{earthLang('elements.data')}}</label>
                                {!! Form::text('data', null, ['placeholder' => earthLang('elements.data'), 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="locale">{{earthLang('elements.locale')}}</label>
                                {!! Form::select('locale', $locales, config('earth.system_locale'), ['data-placeholder' => earthLang('elements.locale'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                            </div>
                            <div class="form-group">
                                <label for="parent_id">{{earthLang('elements.parent_id')}}</label>
                                {!! Form::select('parent_id', $options, $elementId, ['data-placeholder' => earthLang('elements.parent_id'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="comment">{{earthLang('elements.comment')}}</label>
                                {!! Form::textarea('comment', null, ['placeholder' => earthLang('elements.comment'), 'class' => 'form-control', 'rows' => 3]) !!}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    {!! createButtons(isset($customListUri) ? $customListUri : $listUri.earthRequestVars()) !!}
                </div>
            </div>

        {!! Form::close() !!}

    </div>

</div>