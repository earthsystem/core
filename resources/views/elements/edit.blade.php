{!! Form::model($item, ['url' => earthRouteWithVars($route.'.putUpdate', [$item->id]) . earthRequestVars(), 'method' => 'put', 'class' => 'ajax-form']) !!}

<div class="panel panel-default mt-20">
    <div class="panel-heading">
        <h3 class="panel-title">{{$item->id}}. {{$item->name}}</h3>
    </div>
    <div class="panel-body">

        {!! formRules(\Earth\Core\Entities\Element\Element::$formRules) !!}
        <span class="meta-data" data-refresh-uri="{{earthRouteWithVars($route.'.getShow', [$item->id]).earthRequestVars()}}"></span>

        <div class="form-group">
            <label for="active" class="checkbox-inline">
                {!!Form::checkbox('active', 1, $item->active, ['class' => 'bsToggle'])!!}
                {{earthLang('form.active')}}
            </label>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="name">{{earthLang('elements.name')}}</label>
                    {!! Form::text('name', null, ['placeholder' => earthLang('elements.name'), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="type">{{earthLang('elements.type')}}</label>
                    {!! Form::select('type', $types, null, ['data-placeholder' => earthLang('elements.type'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                </div>
                <div class="form-group">
                    <label for="icon">{{earthLang('elements.icon')}}</label>
                    {!! Form::text('icon', null, ['placeholder' => earthLang('elements.icon'), 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="data">{{earthLang('elements.data')}}</label>
                    {!! Form::text('data', null, ['placeholder' => earthLang('elements.data'), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="parent_id">{{earthLang('elements.parent_id')}}</label>
                    {!! Form::select('parent_id', $options, null, ['data-placeholder' => earthLang('elements.parent_id'), 'class' => 'form-control', 'data-selectize' => 'true']) !!}
                </div>
            </div>
        </div>

    </div>
    <div class="panel-footer">
        {!! editButtons(earthRouteWithVars($route.'.getShow', [$item->id]).earthRequestVars()) !!}
    </div>
</div>

{!! Form::close() !!}