<?php

class UpdateRoleTableWithCanModifyParam extends \Illuminate\Database\Migrations\Migration {

    /**
     * Run the migration
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->integer('can_modify')->default(1);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migration
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->dropColumn('can_modify');
            $table->dropSoftDeletes();
        });
    }

}