<?php

class CreateElementsTable extends \Illuminate\Database\Migrations\Migration {

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('active')->default(1);
            $table->string('name', 32);
            $table->string('data', 32);
            $table->string('icon', 32)->nullable();
            $table->string('type', 16)->default('default');
            $table->integer('can_modify')->default(1);

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('elements');
    }

}