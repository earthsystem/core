<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTextsTable
 */
class CreateTextsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('type', 16)->default('default');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string("image_file_name")->nullable();
            $table->integer("image_file_size")->nullable();
            $table->string("image_content_type")->nullable();
            $table->timestamp("image_updated_at")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('texts');
    }

}