<?php

class UpdateUsersTableWithUserTypeAndUsername extends \Illuminate\Database\Migrations\Migration {

    /**
     * Run the migration
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->string('type');
            $table->string('username')->nullable()->unique();
            $table->integer('can_modify')->default(1);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migration
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('username');
            $table->dropSoftDeletes();
        });
    }

}