<?php

use Illuminate\Database\Migrations\Migration;

class CreateElementLocaleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_locale', function(\Illuminate\Database\Schema\Blueprint $table)
        {
            $table->increments('id');
            $table->integer('element_id');
            $table->string('locale', 3)->nullable();
            $table->string('title', 128);
            $table->string('slug', 128);
            $table->string('comment', 256)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('element_locale');
    }

}