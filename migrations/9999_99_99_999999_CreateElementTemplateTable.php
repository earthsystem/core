<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElementTemplateTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_template', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('active')->default(1);
            $table->integer('element_id');
            $table->integer('template_id');
            $table->integer('recursive')->default(0);
            $table->integer('position');
            $table->string('type', 16);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('element_template');
    }

}