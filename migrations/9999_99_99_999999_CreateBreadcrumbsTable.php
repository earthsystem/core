<?php

use Illuminate\Database\Schema\Blueprint;

class CreateBreadcrumbsTable extends \Illuminate\Database\Migrations\Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breadcrumbs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('element_id');
            $table->string('locale', 3);
            $table->string('uri', 512);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('breadcrumbs');
    }

}