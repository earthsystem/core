<?php

Route::get('switch-user', [
    'as' => 'getSwitchUser',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getSwitchUser'
]);

Route::post('switch-user', [
    'as' => 'postSwitchUser',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@postSwitchUser'
]);

Route::get('change-password', [
    'as' => 'getChangePassword',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getChangePassword'
]);

Route::post('change-password', [
    'as' => 'postChangePassword',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@postChangePassword'
]);