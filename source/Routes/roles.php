<?php

/**
 * Roles
 */
Route::get('roles', [
    'as' => 'roles.getIndex',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@getIndex'
]);

Route::get('roles/create', [
    'as' => 'roles.getCreate',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@getCreate'
]);

Route::post('roles', [
    'as' => 'roles.postStore',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@postStore'
]);

Route::get('roles/{id}/edit', [
    'as' => 'roles.getEdit',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@getEdit'
]);

Route::get('roles/{id}/{tab?}', [
    'as' => 'roles.getShow',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@getShow'
]);

Route::put('roles/{id}', [
    'as' => 'roles.putUpdate',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@putUpdate'
]);

Route::delete('roles/{id}', [
    'as' => 'roles.delete',
    'uses' => Earth\Core\Controllers\System\RolesController::class.'@delete'
]);

/**
 * Role users
 */
Route::get('roles/{id}/users/create', [
    'as' => 'roles.users.getCreate',
    'uses' => Earth\Core\Controllers\System\Role\UsersController::class.'@getCreate'
]);

Route::delete('roles/{id}/users/{roleId}', [
    'as' => 'roles.users.delete',
    'uses' => Earth\Core\Controllers\System\Role\UsersController::class.'@delete'
]);