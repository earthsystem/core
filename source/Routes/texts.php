<?php

/**
 * Text locales
 */
Route::get('texts/{id}/locales/create', [
    'as' => 'texts.locales.getCreate',
    'uses' => Earth\Core\Controllers\System\Text\LocalesController::class.'@getCreate'
])->where('id', '[0-9]+');

Route::post('texts/{id}/locales', [
    'as' => 'texts.locales.postStore',
    'uses' => Earth\Core\Controllers\System\Text\LocalesController::class.'@postStore'
])->where('id', '[0-9]+');

Route::get('texts/{id}/locales/{localeId}', [
    'as' => 'texts.locales.getEdit',
    'uses' => Earth\Core\Controllers\System\Text\LocalesController::class.'@getEdit'
])->where('id', '[0-9]+');

Route::put('texts/{id}/locales/{localeId}', [
    'as' => 'texts.locales.putUpdate',
    'uses' => Earth\Core\Controllers\System\Text\LocalesController::class.'@putUpdate'
])->where('id', '[0-9]+');

Route::delete('texts/{id}/locales/{localeId}', [
    'as' => 'texts.locales.delete',
    'uses' => Earth\Core\Controllers\System\Text\LocalesController::class.'@delete'
])->where('id', '[0-9]+');

/**
 * Texts
 */
Route::get('texts', [
    'as' => 'texts.getIndex',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@getIndex'
]);

// Create
Route::get('texts/create', [
    'as' => 'texts.getCreate',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@getCreate'
]);

// Store
Route::post('texts', [
    'as' => 'texts.postStore',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@postStore'
]);

Route::get('texts/{id}/edit', [
    'as' => 'texts.getEdit',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@getEdit'
]);

// Show tab
Route::get('texts/{id}/{tab?}', [
    'as' => 'texts.getShow',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@getShow'
]);

// Update
Route::put('texts/{id}', [
    'as' => 'texts.putUpdate',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@putUpdate'
]);

// Delete
Route::delete('texts/{id}', [
    'as' => 'texts.delete',
    'uses' => Earth\Core\Controllers\System\Text\TextsController::class.'@delete'
]);