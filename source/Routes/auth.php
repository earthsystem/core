<?php

Route::get('login', [
    'as' => 'earth.getLogin',
    'uses' => Earth\Core\Controllers\AuthController::class.'@getLogin'
]);

Route::post('login', [
    'as' => 'earth.postLogin',
    'uses' => Earth\Core\Controllers\AuthController::class.'@postLogin'
]);

Route::get('logout', [
    'as' => 'earth.getLogout',
    'uses' => Earth\Core\Controllers\AuthController::class.'@getLogout'
]);