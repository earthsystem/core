<?php

/**
 * Users
 */
Route::get('users', [
    'as' => 'users.getIndex',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getIndex'
]);

Route::get('users/create', [
    'as' => 'users.getCreate',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getCreate'
]);

Route::post('users', [
    'as' => 'users.postStore',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@postStore'
]);

Route::get('users/{id}/edit', [
    'as' => 'users.getEdit',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getEdit'
]);

Route::get('users/{id}/{tab?}', [
    'as' => 'users.getShow',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@getShow'
]);

Route::put('users/{id}', [
    'as' => 'users.putUpdate',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@putUpdate'
]);

Route::delete('users/{id}', [
    'as' => 'users.delete',
    'uses' => Earth\Core\Controllers\System\UsersController::class.'@delete'
]);

/**
 * User roles
 */
Route::get('users/{id}/roles/create', [
    'as' => 'users.roles.getCreate',
    'uses' => Earth\Core\Controllers\System\User\RolesController::class.'@getCreate'
]);

Route::delete('users/{id}/roles/{roleId}', [
    'as' => 'users.roles.delete',
    'uses' => Earth\Core\Controllers\System\User\RolesController::class.'@delete'
]);