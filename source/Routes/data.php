<?php

Route::post('data', [
    'as' => 'data.postStore',
    'uses' => Earth\Core\Controllers\System\Data\DataController::class.'@postStore'
]);

Route::put('data/{id}/{prefix?}', [
    'as' => 'data.putUpdate',
    'uses' => Earth\Core\Controllers\System\Data\DataController::class.'@putUpdate'
]);

Route::delete('data/{id}', [
    'as' => 'data.delete',
    'uses' => Earth\Core\Controllers\System\Data\DataController::class.'@delete'
]);