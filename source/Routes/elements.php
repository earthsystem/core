<?php

/**
 * Element texts
 */
Route::get('elements/{id}/texts/attach', [
    'as' => 'elements.texts.getAttach',
    'uses' => Earth\Core\Controllers\System\Element\TextsController::class.'@getAttach'
])->where('id', '[0-9]+');

Route::put('elements/{id}/texts/{textId}/sort', [
    'as' => 'elements.texts.putSort',
    'uses' => Earth\Core\Controllers\System\Element\TextsController::class.'@putSort'
])->where('id', '[0-9]+')->where('textId', '[0-9]+');

Route::delete('elements/{id}/texts/{textId}', [
    'as' => 'elements.texts.delete',
    'uses' => Earth\Core\Controllers\System\Element\TextsController::class.'@delete'
])->where('id', '[0-9]+');

/**
 * Element templates
 */
Route::get('elements/{id}/templates/{position}/create', [
    'as' => 'elements.templates.getCreate',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@getCreate'
])->where('id', '[0-9]+');

Route::post('elements/{id}/templates/connector', [
    'as' => 'elements.templates.postConnector',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@postConnector'
])->where('id', '[0-9]+');

Route::get('elements/{id}/templates/{position}/attach', [
    'as' => 'elements.templates.getAttach',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@getAttach'
])->where('id', '[0-9]+');

Route::post('elements/{id}/templates/{position}', [
    'as' => 'elements.templates.postStore',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@postStore'
])->where('id', '[0-9]+');

Route::get('elements/{id}/templates/{templateId}', [
    'as' => 'elements.templates.getEdit',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@getEdit'
])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

Route::put('elements/{id}/templates/{templateId}', [
    'as' => 'elements.templates.putUpdate',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@putUpdate'
])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

Route::put('elements/{id}/templates/{templatePivotId}/pivot', [
    'as' => 'elements.templates.putUpdatePivot',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@putUpdatePivot'
])->where('id', '[0-9]+')->where('templatePivotId', '[0-9]+');

Route::put('elements/{id}/templates/{templateId}/sort', [
    'as' => 'elements.templates.putSort',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@putSort'
])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

Route::delete('elements/{id}/templates/{templateId}', [
    'as' => 'elements.templates.delete',
    'uses' => Earth\Core\Controllers\System\Element\TemplatesController::class.'@delete'
])->where('id', '[0-9]+');

/**
 * Element locales
 */
Route::get('elements/{id}/locales/create', [
    'as' => 'elements.locales.getCreate',
    'uses' => Earth\Core\Controllers\System\Element\LocalesController::class.'@getCreate'
])->where('id', '[0-9]+');

Route::post('elements/{id}/locales', [
    'as' => 'elements.locales.postStore',
    'uses' => Earth\Core\Controllers\System\Element\LocalesController::class.'@postStore'
])->where('id', '[0-9]+');

Route::get('elements/{id}/locales/{localeId}', [
    'as' => 'elements.locales.getEdit',
    'uses' => Earth\Core\Controllers\System\Element\LocalesController::class.'@getEdit'
])->where('id', '[0-9]+');

Route::put('elements/{id}/locales/{localeId}', [
    'as' => 'elements.locales.putUpdate',
    'uses' => Earth\Core\Controllers\System\Element\LocalesController::class.'@putUpdate'
])->where('id', '[0-9]+');

Route::delete('elements/{id}/locales/{localeId}', [
    'as' => 'elements.locales.delete',
    'uses' => Earth\Core\Controllers\System\Element\LocalesController::class.'@delete'
])->where('id', '[0-9]+');

/**
 * Elements
 */
Route::get('elements', [
    'as' => 'elements.getIndex',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getIndex'
]);

// Tree
Route::get('elements/tree', [
    'as' => 'elements.getTree',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getTree'
]);

// Element link list
Route::get('elements/tiny-mce-link-list', [
    'as' => 'elements.getTinyMCELinkList',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getTinyMCELinkList'
]);

// Create
Route::get('elements/create/{elementId?}', [
    'as' => 'elements.getCreate',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getCreate'
]);

// Store
Route::post('elements', [
    'as' => 'elements.postStore',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@postStore'
]);

Route::get('elements/{id}/edit', [
    'as' => 'elements.getEdit',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getEdit'
]);

// Show tab
Route::get('elements/{id}/{tab?}', [
    'as' => 'elements.getShow',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@getShow'
]);

// Update
Route::put('elements/{id}', [
    'as' => 'elements.putUpdate',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@putUpdate'
]);

// Move
Route::put('elements/{id}/sort', [
    'as' => 'elements.putSort',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@putSort'
]);

// Delete
Route::delete('elements/{id}', [
    'as' => 'elements.delete',
    'uses' => Earth\Core\Controllers\System\ElementsController::class.'@delete'
]);