<?php

/*
 * TinyMCE file uploader
 */
Route::get('attachments/file-manager/dialog', [
    'as' => 'attachments.file-manager.getDialog',
    'uses' => Earth\Core\Controllers\System\Attachment\FileManagerController::class.'@getDialog'
]);

Route::post('attachments/file-manager/upload', [
    'as' => 'attachments.file-manager.postUpload',
    'uses' => Earth\Core\Controllers\System\Attachment\FileManagerController::class.'@postUpload'
]);

/*
 * Attachment locale
 */
Route::get('attachments/{attachableId}/{attachableClass}/{attachmentId}/locales', [
    'as' => 'attachments.locales.getIndex',
    'uses' => Earth\Core\Controllers\System\Attachment\LocalesController::class.'@getIndex'
])->where('attachableId', '[0-9]+')->where('attachmentId', '[0-9]+');

Route::post('attachments/{attachableId}/{attachableClass}/{attachmentId}/locales', [
    'as' => 'attachments.locales.postStore',
    'uses' => Earth\Core\Controllers\System\Attachment\LocalesController::class.'@postStore'
])->where('attachableId', '[0-9]+')->where('attachmentId', '[0-9]+');

Route::put('attachments/locales/{localeId}', [
    'as' => 'attachments.locales.putUpdate',
    'uses' => Earth\Core\Controllers\System\Attachment\LocalesController::class.'@putUpdate'
])->where('localeId', '[0-9]+');

/**
 * Attachments
 */
Route::get('attachments/{attachableId}/{attachableType}/{attachmentType}', [
    'as' => 'attachments.getShow',
    'uses' => Earth\Core\Controllers\System\AttachmentsController::class.'@getShow'
]);

Route::post('attachments/{attachableId}/{attachableType}/{attachmentType}/upload', [
    'as' => 'attachments.postUpload',
    'uses' => Earth\Core\Controllers\System\AttachmentsController::class.'@postUpload'
]);

Route::put('attachments/{id}/sort', [
    'as' => 'attachments.putSort',
    'uses' => Earth\Core\Controllers\System\AttachmentsController::class.'@putSort'
])->where('id', '[0-9]+');

Route::delete('attachments/{attachableId}', [
    'as' => 'attachments.delete',
    'uses' => Earth\Core\Controllers\System\AttachmentsController::class.'@delete'
]);