<?php

Route::group([
    'middleware' => ['web']
], function() {
    include_once 'Routes/auth.php';
});

/*
 * Routes that are protected with authentication
 */
Route::group([
    'middleware' => ['web', 'earth.auth'],
    'prefix' => config('earth.system_prefix'),
    'as' => config('earth.system_route_prefix').'.'
], function() {
    
    include_once 'Routes/loggedInUser.php';

    Route::group([
        'middleware' => ['earth.access']
    ], function() {

        Route::get('/', [
            'as' => 'home',
            'uses' => Earth\Core\Controllers\System\DashboardController::class.'@getHome'
        ]);

        /**
         * Permissions
         */
        Route::post('permissions', [
            'as' => 'permissions.postStore',
            'uses' => Earth\Core\Controllers\System\Permission\PermissionsController::class.'@postStore'
        ]);

        include_once 'Routes/users.php';
        include_once 'Routes/roles.php';
        include_once 'Routes/elements.php';
        include_once 'Routes/attachments.php';
        include_once 'Routes/texts.php';
        include_once 'Routes/data.php';

    });

});