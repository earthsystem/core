<?php

/**
 * @return \Earth\Core\Entities\Element\Element
 */
function element()
{
    return new \Earth\Core\Entities\Element\Element;
}

/**
 * @param $type
 * @return \Earth\Core\Entities\Element\Element|\Illuminate\Database\Query\Builder
 */
function get_codes($type)
{
    $codesId = element()->whereData('codes')->value('id');
    $typeId = element()->whereData($type)->whereType('code-group')->whereParentId($codesId)->value('id');
    $codes = element()->whereParentId($typeId)->whereType('code');
    return $codes;
}

/**
 * @param $items
 * @return array
 */
function options_of_elements($items)
{
    $newArray = [];
    foreach($items as $index => $item) {
        $newArray[$item->id] = str_repeat('....', $item->depth).' '.$item->name;
    }
    return $newArray;
}

/**
 * @return array|\Illuminate\Support\Collection
 */
function get_locales()
{
    $locales = get_codes('translations')->pluck('name', 'data');
    $locales[''] = earthLang('base.all_locales');
    return $locales;
}

/**
 * @param $locales
 * @param $locale
 * @return mixed
 */
function get_locale_name($locales, $locale)
{
    if (isset($locales[$locale])) return $locales[$locale];
    return $locale;
}

/**
 * @param null $index
 * @return mixed
 */
function get_category($index = null)
{
    if ( isset($index) and !is_numeric($index) ) {
        return element()->whereData($index)->whereActive(1)->withLocale()->first();
    }

    if ( isset($index) ) {
        return element()->hasSlug( slug($index) )->whereActive(1)->withLocale()->first();
    }

    return config()->get('category');
}

/**
 * @return mixed
 */
function has_category()
{
    return config()->has('category');
}

/**
 * @return bool
 */
function is_home()
{
    if ( !has_category() ) {
        return false;
    }

    $category = get_category();
    if ($category->data == "home") return true;

    return false;
}

/**
 * @param null $start
 * @return mixed
 */
function web_navigation($start = null)
{
    if ( !isset($start) ) $start = config('application')->id;

    $elements = element()->whereActive(1)
        ->whereParentId($start)
        ->withLocale()
        ->with('submenu')
        ->with('breadcrumb')
        ->orderBy('lft')
        ->get(['id', 'data', 'lft', 'rgt', 'depth', 'type']);

    return $elements;
}

/**
 * @return mixed
 */
function slugs()
{
    return config('slugs');
}

/**
 * @param $index
 * @return null
 */
function slug($index)
{
    $slugs = slugs();
    return isset($slugs[$index]) ? $slugs[$index] : null;
}

/**
 * Reditect to first child
 */
function redirect_to_first_child()
{
    $child = config('category')->submenu()->first();

    if ( count($child) ) {
        header('Location: '. url()->current() . '/' . $child->slug);
        exit;
    }
}