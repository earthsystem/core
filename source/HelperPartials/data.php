<?php

/**
 * @param $object
 * @param $key
 * @return null
 */
function get_data($object, $key)
{
    if ( method_exists($object, "custom_data") ) {
        return $object->custom_data()->whereCustomableKey($key)->value("customable_value");
    }

    return null;
}

/**
 * @param $object
 * @param $key
 * @return null
 */
function get_multiple_data($object, $key)
{
    if ( method_exists($object, "custom_data") ) {
        return $object->custom_data()->whereCustomableKey($key)->get(["customable_value"]);
    }

    return null;
}