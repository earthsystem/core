<?php

/**
 * @return bool
 */
function isAliasUser()
{
    return Sentinel::getUser()->getUserId() != session("aliasUser")->id;
}

/**
 * @return bool
 */
function isAdminUser()
{
    return !Sentinel::getUser()->can_modify;
}

/**
 * @return mixed
 */
function isUserInAdminRole()
{
    return Sentinel::inRole("Admin");
}

/**
 * @return bool
 */
function canWrite()
{
    return session("access.write");
}

/**
 * @return bool
 */
function canDelete()
{
    return session("access.destroy");
}