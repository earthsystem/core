<?php

/**
 * @return mixed
 */
function has_article()
{
    return config()->has("article");
}

/**
 * @param null $categoryId
 * @return mixed
 */
function content_in_category($categoryId = null)
{
    if ( !isset($categoryId) ) {
        $categoryId = config("category")->id;
    }

    return \Earth\Core\Entities\Text\Text::isInElement($categoryId)->orderByRelationshipAttribute('position')->withLocale();
}

/**
 * @param null $types
 * @return \Earth\Core\Entities\Text\Text|\Illuminate\Database\Query\Builder
 */
function content_by_type($types = null)
{
    if ( is_string($types) ) $types = [$types];

    return \Earth\Core\Entities\Text\Text::whereIn("type", $types)->withLocale();
}