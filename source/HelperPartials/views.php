<?php

/**
 * @param $view
 * @param string $prefix
 * @param bool $stringOnly
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
function earthView($view, $stringOnly = false, $prefix = 'earth')
{
    if ($stringOnly) return $prefix."::".$view;
    return view($prefix."::".$view);
}

/**
 * @param $items
 * @param $routePrefix
 * @param int $level
 * @return mixed
 */
function navigation($items, $routePrefix, $level = 0)
{
    return earthView('partials.nav')->with('items', $items)->with('routePrefix', $routePrefix)->with('level', $level);
}

/**
 * @param $rules
 * @return \Illuminate\Support\HtmlString
 */
function formRules($rules)
{
    return Form::hidden('form-rules', json_encode($rules), ['class' => 'form-rules']);
}

/**
 * @param $listUri
 * @param null $createText
 * @param null $cancelText
 * @return string
 */
function createButtons($listUri, $createText = null, $cancelText = null)
{
    if (!isset($createText)) $createText = earthLang('form.create');
    if (!isset($cancelText)) $cancelText = earthLang('form.cancel');

    return '<button type="submit" class="btn btn-success">'.$createText.'</button>
            <a href="'.$listUri.'" title="'.$cancelText.'" aria-expanded="false" data-toggle="content" class="btn btn-link">'.$cancelText.'</a>';
}

/**
 * @param $viewUri
 * @param null $saveText
 * @param null $cancelText
 * @return string
 */
function editButtons($viewUri, $saveText = null, $cancelText = null)
{
    if (!isset($saveText)) $saveText = earthLang('form.save');
    if (!isset($cancelText)) $cancelText = earthLang('form.cancel');

    return '<button type="submit" class="btn btn-success">'.$saveText.'</button>
            <a href="'.$viewUri.'" title="'.$cancelText.'" aria-expanded="false" data-toggle="content" class="btn btn-link">'.$cancelText.'</a>';
}

/**
 * @param $listUri
 * @param null $backText
 * @return string
 */
function backButton($listUri, $backText = null) {

    if (!isset($backText)) $backText = earthLang('base.back');

    return '<a href="'.$listUri.'" class="btn btn-default btn-xs pull-left mr-20 backButton" data-toggle="content">
                <i class="fa fa-arrow-circle-left fa-fw"></i> '.$backText.'
            </a>';
}

/**
 * @param $value
 * @param string $trueIcon
 * @param string $falseIcon
 * @return string
 */
function bool_icon($value, $trueIcon = "fa-check", $falseIcon = "fa-remove")
{
    return '<i class="fa '.$value ? $trueIcon : $falseIcon.'"></i>';
}

/**
 * @param $name
 * @param $value
 * @param $uri
 * @param string $size
 * @return \Illuminate\Support\HtmlString
 */
function toggleable($name, $value, $uri, $size = "mini")
{
    $html = Form::checkbox($name, 1, $value, ['class' => 'bsToggle', 'data-token' => csrf_token(), 'data-size' => $size, 'data-put-update' => $uri]);
    return $html;
}

/**
 * @return string
 */
function placeholderImg()
{
    return "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTQ3ZDI5MDIzYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NDdkMjkwMjNjIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ0LjA1NDY4NzUiIHk9Ijc0LjUiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=";
}

/**
 * @param $items
 * @param int $stopAtLevel
 * @param null $currentSlug
 * @param null $view
 * @param int $currentLevel
 * @return $this|null
 */
function submenu($items, $stopAtLevel = 1, $currentSlug = null, $view = null, $currentLevel = 0)
{
    $application = config('application');
    
    if ($currentLevel == $stopAtLevel) return null;
    
    if ( !$items instanceof \Illuminate\Support\Collection ) {
        $category = get_category($items);
        if (!$category) return null;
        $items = web_navigation($category->id);
    }

    if ( !isset($view) ) {
        $view = $application->data.".page.partials.level";
    }

    if ( !isset($currentSlug) ) {
        $currentSlug = app()->getLocale();
    }

    return view($view)->with([
        'items' => $items,
        'view' => $view,
        'stopAtLevel' => $stopAtLevel,
        'currentLevel' => $currentLevel,
        'currentSlug' => $currentSlug
    ]);
}

/**
 * @param $bytes
 * @param int $decimals
 * @return string
 */
function file_size($bytes, $decimals = 2)
{
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

/**
 * @param $mimeType
 * @return string
 */
function file_type_icon($mimeType)
{
    return '<i class="fa fa-fw fa-'.config('earth.mime_types.'.$mimeType).'"></i>';
}