<?php

/**
 * @param $from
 * @param $to
 * @return string
 */
function get_relative_path($from, $to)
{
    // some compatibility fixes for Windows paths
    $from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
    $to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
    $from = str_replace('\\', '/', $from);
    $to   = str_replace('\\', '/', $to);

    $from     = explode('/', $from);
    $to       = explode('/', $to);
    $relPath  = $to;

    foreach($from as $depth => $dir) {
        // find first non-matching dir
        if($dir === $to[$depth]) {
            // ignore this directory
            array_shift($relPath);
        } else {
            // get number of remaining dirs to $from
            $remaining = count($from) - $depth;
            if($remaining > 1) {
                // add traversals up to first matching dir
                $padLength = (count($relPath) + $remaining - 1) * -1;
                $relPath = array_pad($relPath, $padLength, '..');
                break;
            } else {
                $relPath[0] = './' . $relPath[0];
            }
        }
    }
    return implode('/', $relPath);
}

/**
 * @param $tab
 * @param $links
 * @return mixed
 */
function find_active_tab($tab, $links)
{
    foreach($links as $item)
    {
        if ($item->id == $tab) return $item;
    }
}

/**
 * @return \Earth\Core\Entities\Editable
 */
function editable()
{
    return new \Earth\Core\Entities\Editable();
}

/**
 * Return translation for earth keys
 * @param $path
 * @param string $prefix
 * @return string|\Symfony\Component\Translation\TranslatorInterface
 */
function earthLang($path, $prefix = 'earth')
{
    return trans($prefix."::".$path);
}

/**
 * @param $path
 * @param array $vars
 * @param string $prefix
 * @return string|\Symfony\Component\Translation\TranslatorInterface
 */
function earthLangWithVars($path, $vars = [], $prefix = 'earth')
{
    return trans($prefix."::".$path, $vars);
}

/**
 * @param $path
 * @param string $basePath
 * @return string
 */
function earthAsset($path, $basePath = '/vendor/earth/core')
{
    return asset($basePath."/".$path);
}

/**
 * @param $templates
 * @param int $small
 * @param int $medium
 * @param int $full
 * @return int
 */
function column_width($templates, $small = 4, $medium = 8, $full = 12)
{
    if ($templates->left->count() and $templates->right->count()) {
        return $small;
    }

    if ($templates->left->count() or $templates->right->count()) {
        return $medium;
    }

    return $full;
}