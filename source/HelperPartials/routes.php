<?php

/**
 * @param $route
 * @param null $prefix
 * @return bool
 */
function earthRouteExists($route, $prefix = null)
{
    if (!isset($prefix)) $prefix = config('earth.system_route_prefix');
    $routeName = $prefix.'.'.$route;
    return Route::has($routeName);
}

/**
 * @param $route
 * @param string $prefix
 * @return string
 */
function earthRoute($route, $prefix = null)
{
    if (!isset($prefix)) $prefix = config('earth.system_route_prefix');
    return route($prefix.'.'.$route);
}

/**
 * @param $route
 * @param $vars
 * @param null $prefix
 * @return string
 */
function earthRouteWithVars($route, $vars = [], $prefix = null)
{
    if (!isset($prefix)) $prefix = config('earth.system_route_prefix');
    return route($prefix.'.'.$route, $vars);
}

/**
 * @param null $requestVars
 * @return string
 */
function earthRequestVars($requestVars = null)
{
    if (!isset($requestVars)) $requestVars = request()->except(['ajax', 'ajax_process']);
    if (isset($requestVars['ajax'])) unset($requestVars['ajax']);
    if (isset($requestVars['ajax_process'])) unset($requestVars['ajax_process']);
    return "?".http_build_query($requestVars);
}

/**
 * @param $route
 * @param null $prefix
 * @return bool
 */
function isCurrentRoute($route, $prefix = null)
{
    if (!isset($prefix)) $prefix = config('earth.system_route_prefix');
    $routeName = $prefix.'.'.$route;
    return Route::currentRouteName() == $routeName;
}

/************
 *
 * WEBSITE SPECIFIC
 *
 ************/

/**
 * @return \Earth\Core\Services\RouteService
 */
function earthRouter()
{
    return app()["earthRouter"];
}

/**
 * @return \Earth\Core\Entities\EarthRoute
 */
function webRoute()
{
    return (new \Earth\Core\Entities\EarthRoute);
}