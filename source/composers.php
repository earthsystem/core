<?php

view()->composer( earthView('partials.header', true) , Earth\Core\Composers\HeaderComposer::class);

// Lists and sidebars
view()->composer( earthView('users.list', true) , Earth\Core\Composers\User\ListComposer::class);
view()->composer( earthView('users.sidebar', true) , Earth\Core\Composers\User\ListComposer::class);

view()->composer( earthView('roles.list', true) , Earth\Core\Composers\Role\ListComposer::class);
view()->composer( earthView('roles.sidebar', true) , Earth\Core\Composers\Role\ListComposer::class);

view()->composer( earthView('elements.list', true) , Earth\Core\Composers\Element\ListComposer::class);
view()->composer( earthView('elements.tabs.permissions', true) , Earth\Core\Composers\Element\PermissionComposer::class);

view()->composer( earthView('texts.list', true) , Earth\Core\Composers\Text\ListComposer::class);
view()->composer( earthView('texts.sidebar', true) , Earth\Core\Composers\Text\ListComposer::class);
