<?php namespace Earth\Core\Controllers\Web;

use Exception;

class HomepageController extends CoreController {

    /**
     * Get the content
     */
    public function getHomepage()
    {
        try {

            $application = config("application");
            $homepageElement = element()->whereParentId($application->id)
                ->whereData('home')
                ->whereActive(0)
                ->withLocale()
                ->first();

            if (!$homepageElement) throw new Exception("missing_homepage_element_in_application");

            $this->setCategory($homepageElement);
            return $this->render();

        } catch (Exception $e) {

            if ( getenv("APP_DEBUG") == true ) {
                abort(500, $e->getMessage());
            }

            return (new ErrorController)->getError($e);

        }
    }

}