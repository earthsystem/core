<?php namespace Earth\Core\Controllers\Web;

use Exception;

class PageController extends CoreController {

    /**
     * @param $root
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getRootPage($root)
    {
        $this->setCategory($root);
        return $this->render();
    }

    /**
     * @param $root
     * @param $slugs
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getPage($root, $slugs)
    {

        try {

            array_shift($slugs);
            $searchElements = [$root->id];

            foreach($slugs as $slug)
            {
                if ( $this->isLastSlug($slug, $slugs) ) {

                    $element = element()->whereIn("parent_id", $searchElements)->hasSlug($slug)->withLocale()->whereActive(1)->first();
                    if (!$element) throw new Exception("missing_element_for_slug [$slug]");

                    $this->setCategory($element);
                } else {
                    $searchElements = element()->whereIn('parent_id', $searchElements)->hasSlug($slug)->hasLocale()->whereActive(1)->pluck('id');
                    if ( !count($searchElements) ) throw new Exception("missing_element_for_slug [$slug]");
                }
            }

            return $this->render();

        } catch (Exception $e) {

            if ( getenv("APP_DEBUG") == true ) {
                abort(500, $e->getMessage());
            }

            return (new ErrorController)->getError($e);

        }

    }

    /**
     * @param $slug
     * @param $slugs
     * @return bool
     */
    private function isLastSlug($slug, $slugs)
    {
        return $slug == $slugs[count($slugs) - 1];
    }

}