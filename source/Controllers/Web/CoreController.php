<?php namespace Earth\Core\Controllers\Web;

use Earth\Core\Entities\Template\Set;
use Earth\Core\Entities\Template\Template;
use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

class CoreController extends Controller {

    /**
     * CoreController constructor.
     */
    public function __construct()
    {
        if ( config("earth.protectRoutes") ) {
            $this->middleware("auth.basic");
        }

        config()->set("earth.is_frontend", true);
    }

    /**
     * Get the content
     *
     * @param $locale
     * @param null $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function get($locale, $slug = null)
    {
        try {

            $slugs = isset($slug) ? explode("/", $slug) : null;
            view()->share('slugs', $slugs);
            config()->set('slugs', $slugs);

            // Is it homepage?
            if ( !isset($slug) ) {
                return (new HomepageController)->callAction("getHomepage", []);
            }

            $root = element()->withLocale()
                ->hasSlug($slugs[0])
                ->whereActive(1)
                ->whereIn('parent_id', $this->baseSearchElements())
                ->first();
            if (!$root) throw new Exception("missing_category_for_slug_0");

            if ($root->type == "news") {
                return (new NewsController)->callAction("getNews", [$root, $slugs]);
            }

            if ( count($slugs) == 1 ) {
                return (new PageController)->callAction("getRootPage", [$root]);
            }

            return (new PageController)->callAction("getPage", [$root, $slugs]);

        } catch (Exception $e) {

            if ( getenv("APP_DEBUG") == true ) {
                abort(500, $e->getMessage());
            }

            return (new ErrorController)->getError($e);

        }
    }

    /**
     * @return array
     */
    protected function baseSearchElements()
    {
        $application = config('application');
        return [
            $application->id,
            isset($application->hiddenId)
                ? $application->hiddenId
                : null
        ];
    }

    /**
     * @param $element
     */
    protected function setCategory($element)
    {
        config()->set('category', $element);
        view()->share('category', $element);
    }

    /**
     * Set the templates
     */
    protected function setTemplates()
    {
        $application = config('application');
        $category = config('category');

        // Templates in parent categories
        $parentElements = element()->where('lft', '<', $category->lft)
            ->where('rgt', '>', $category->rgt)
            ->where('depth', '<', $category->depth)
            ->where('lft', '>', $application->lft)
            ->with(['templates' => function($query) {
                return $query->whereActive(1)->whereRecursive(1)->orderBy('type')->orderBy('position');
            }])
            ->get();

        $templatesInCategory = $category->templates()->whereActive(1)->orderBy('type')->orderBy('position')->get();
        $templatesArray = [];

        foreach($parentElements as $parentElement) {

            foreach ($parentElement->templates as $template) {
                $exists = false;

                foreach($templatesInCategory as $base) {
                    if ( $base->path == $template->path ) {
                        $exists = true;
                        break;
                    }
                }

                if (!$exists) $templatesArray[] = $template;
            }

        }

        $templates = $templatesInCategory->merge($templatesArray);
        $templates = $this->sortTemplates($templates);

        config()->set('templates', $templates);
        view()->share('templates', $templates);
    }

    /**
     * @param $templates
     * @return Set
     */
    protected function sortTemplates($templates)
    {
        $sortedTemplates = new Set;

        foreach($templates as $template) {
            $templateType = $template->pivot->type;
            $sortedTemplates->{$templateType}->add($template);
        }
        
        return $sortedTemplates;
    }

    /**
     * @param bool $error
     * @param bool $viewOnly
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    protected function render($error = false, $viewOnly = false)
    {
        try {

            if (!$error) $this->setTemplates();

            $application = config('application');
            $this->registerComposers();

            $application->data = isset($application->data) ? $application->data : "earth";

            if ( request()->has("ajax") ) {
                if ( !view()->exists($application->data . '.ajax') ) {
                    throw new Exception("missing_ajax_layout [".$application->data.".ajax]");
                }

                return view($application->data . '.ajax');
            }

            if ( !view()->exists($application->data . '.layout') ) {
                throw new Exception("missing_layout [".$application->data.".layout]");
            }

            $view = view($application->data . '.layout');
            
            if ( view()->exists($application->data . '.partials.head') ) {
                $view->with('head', view($application->data . '.partials.head') );
            }
            if ( view()->exists($application->data . '.partials.javascript') ) {
                $view->with('javascript', view($application->data . '.partials.javascript') );
            }
            if ( view()->exists($application->data . '.partials.header') ) {
                $view->with('header', view($application->data . '.partials.header') );
            }
            if ( view()->exists($application->data . '.partials.footer') ) {
                $view->with('footer', view($application->data . '.partials.footer') );
            }

            if ($viewOnly) return $view;
            return $view->render();

        } catch (Exception $e) {
            abort(500, $e->getMessage());
        }
    }

    /**
     *
     */
    private function registerComposers()
    {
        $file = app_path("Http/composers.php");
        if ( file_exists($file) ) include_once $file;
    }

}