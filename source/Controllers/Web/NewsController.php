<?php namespace Earth\Core\Controllers\Web;

use Earth\Core\Entities\Text\Text;
use Exception;

class NewsController extends CoreController {

    /**
     * @param $root
     * @param $slugs
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getNews($root, $slugs)
    {

        try {

            $this->setCategory($root);

            if ( request()->has("year") ) {
                config()->set('earth.year', request()->get("year") );
                view()->share('requestedYear', request()->get("year") );
            }

            if ( request()->has("month") ) {
                config()->set('earth.year', request()->get("month") );
                view()->share('requestedMonth', request()->get("month") );
            }

            if ( count($slugs) > 1 ) {

                if ( is_numeric($slugs[1]) and strlen($slugs[1]) == 4 ) {
                    // means this is a year ( we presume the year is 4 digits, if our program still exists after this changes, then it's a really good program)
                    config()->set('earth.year', $slugs[1]);
                    view()->share('requestedYear', $slugs[1]);

                    if ( isset($slugs[2]) and is_numeric($slugs[2]) and strlen($slugs[2]) == 2 ) {
                        config()->set('earth.month', $slugs[2]);
                        view()->share('requestedMonth', $slugs[2]);
                    }

                } else {

                    if ( !isset($slugs[2]) ) throw new Exception("invalid_article_id");
                    if ( !is_numeric($slugs[2]) ) throw new Exception("invalid_article_id");
                    
                    $article = Text::whereIn('type', config('earth.website_news_text_types') )
                        ->withLocale()
                        ->isPublished()
                        ->hasSlug($slugs[1])
                        ->whereId($slugs[2])
                        ->first();
                    if (!$article) throw new Exception("missing_article");

                    config()->set('article', $article);
                    view()->share('article', $article);
                }

            }

            return $this->render();

        } catch (Exception $e) {

            if ( getenv("APP_DEBUG") == true ) {
                abort(500, $e->getMessage());
            }

            return (new ErrorController)->getError($e);

        }

    }

}