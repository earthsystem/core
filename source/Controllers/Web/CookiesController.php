<?php namespace Earth\Core\Controllers\Web;

use Illuminate\Support\Collection;

class CookiesController extends CoreController {

    /**
     * @param $value
     * @return \Illuminate\Http\RedirectResponse
     */
    public function block($locale, $value)
    {
        if ($value == 1) {
            foreach ($_COOKIE as $key => $value) {
                unset($_COOKIE[$key]);
                setcookie($key, null, -1, "/");
            }
            setcookie('cookiesAreBlocked', 1, time() + (24*3600*30*6), "/");
        } else {
            unset($_COOKIE['cookiesAreBlocked']);
            setcookie('cookiesAreBlocked', null, -1, "/");
        }

        return back();
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getSettings($locale)
    {
        $cookieSettings = new \stdClass();
        $cookieSettings->pivot = new \stdClass();
        $cookieSettings->pivot->type = "main";
        $cookieSettings->path = config('application')->data . ".partials.cookiesSettings";

        $templates = new Collection([$cookieSettings]);
        $templates = $this->sortTemplates($templates);

        config()->set('templates', $templates);
        view()->share('templates', $templates);

        return $this->render(true);
    }

}