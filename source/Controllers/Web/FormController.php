<?php namespace Earth\Core\Controllers\Web;

use Exception;
use Illuminate\Mail\Message;
use Mail;
use Validator;

class FormController extends CoreController {

    /**
     * @var
     */
    private $senderEmail;

    /**
     * @var
     */
    private $senderName;

    /**
     * @var
     */
    private $destinationEmail;

    /**
     * @var
     */
    private $emailSubject;

    /**
     * @param $locale
     * @return mixed
     */
    public function submit($locale)
    {
        app()->setLocale($locale);

        try {

            $this->senderEmail = config('earth.website_form.sender_email');
            $this->senderName = config('earth.website_form.sender_name');
            $this->destinationEmail = config('earth.website_form.destination_email');
            $this->emailSubject = config('earth.website_form.email_subject');

            $validationRules = [
                'name' => 'required',
                'email' => 'required|email',
                'message' => 'required',
            ];

            if ( !env('NOCAPTCHA_SITEKEY') or !env('NOCAPTCHA_SECRET') ) {
                throw new Exception("missing_captcha_keys");
            }

            $validationRules['g-recaptcha-response'] = 'required|captcha';
            $validation = Validator::make( request()->input(), $validationRules );

            if ( !$validation->passes() ) {
                throw new Exception("validation_not_passed");
            }

            $this->send( request()->except(['_token', 'g-recaptcha-response']) );
            return redirect()->back()->with('success', true);

        } catch (Exception $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * @param $input
     * @throws Exception
     */
    private function send($input)
    {
        $application = config('application');
        $view = $application->data.'.contact.email';

        if ( !env('MAIL_USERNAME') or !env('MAIL_PASSWORD') ) {
            throw new Exception("missing_mailgun_username_and_password");
        }

        Mail::send($view, ['data' => $input], function(Message $message) {
            $message->from($this->senderEmail, $this->senderName)->to($this->destinationEmail)->subject(
                $this->emailSubject
            );
        });
    }

}