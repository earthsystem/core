<?php namespace Earth\Core\Controllers\Web;

use Earth\Core\Entities\Template\Set;
use Earth\Core\Entities\Template\Template;
use Exception;
use Log;

class ErrorController extends CoreController {

    /**
     * @param Exception $e
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getError(Exception $e)
    {
        $application = config('application');

        if (!view()->exists($application->data.'.404')) {
            abort(500, "missing_error_page_template");
        }

        $templates = new Set();
        $errorTemplate = new Template();
        $errorTemplate->path = $application->data.'.404';
        $templates->main->add($errorTemplate);

        config()->set('templates', $templates);
        view()->share('templates', $templates);
        view()->share('theMessage', $this->parseErrorMessage( $e->getMessage() ));

        return $this->render(true);
    }

    /**
     * @param $message
     */
    private function parseErrorMessage($message)
    {
        Log::error($message);
        return $message;
    }

}