<?php namespace Earth\Core\Controllers;

use Earth\Core\Entities\User\User;
use Exception;
use Illuminate\Routing\Controller;
use Sentinel;

class AuthController extends Controller {

    /**
     * Where to redirect after login - if successful
     */
    protected $redirectTo;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        // If there is a referer, we will write it to session so we know where to redirect
        if ($referer = session('referer')) {
            session()->flash('referer', $referer);
        }

        // Set the redirect to string
        $this->redirectTo = config('earth.system_after_login_redirect');
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return earthView('auth.login')->with('pageTitle', 'Earth &raquo; Login');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin()
    {
        try {

            $authenticate = Sentinel::authenticate([
                config('earth.system_login_key') => request()->input( config('earth.system_login_key') ),
                'password' => request()->input('password')
            ]);

            if (!$authenticate) throw new Exception;

            $user = User::whereId( Sentinel::getUser()->getUserId() )->first();

            if (session()->has("referer")) {
                return response()->json( [ 'uri' => session('referer') ] , 200);
            }

            if ( isset($user) and $defaultApp = $user->getDefaultApplication() ) {
                session()->put("aliasUser", $user);
                session()->put("adminUserRole", Sentinel::findRoleBySlug("admin"));
                return response()->json( [ 'uri' => earthRoute('home', 'earth.'.$defaultApp->data) ], 200);
            }

            return response()->json( [ 'uri' => $this->redirectTo ], 200);

        } catch (Exception $e) {
            return response()->json( [ 'message' => earthLang('login.invalid_login') ], 401);
        }
    }

    /**
     * @return mixed
     */
    public function getLogout()
    {
        session()->flush('aliasUser');
        session()->flush('adminUserRole');
        Sentinel::logout();

        return redirect( earthRoute('getLogin', 'earth') );
    }

}