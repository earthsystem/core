<?php namespace Earth\Core\Controllers\Module\Element;

class LocalesController extends \Earth\Core\Controllers\System\Element\LocalesController {

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $rootElement;

    /**
     * LocalesController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        view()->share('customSidebar', earthView('texts.sidebar', true));
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        view()->share('pageTitle', $this->title.' &raquo; '.earthLang('base.system').' &raquo; earth');
        return $this;
    }

    /**
     * @param $rootElement
     * @return $this
     */
    public function setRootElement($rootElement)
    {
        $this->rootElement = $rootElement;
        config()->set('elementsRootElement', $rootElement);
        return $this;
    }

    /**
     * @param $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;

        if (isset($this->route)) {
            $this->listUri = earthRoute($this->route.'.getIndex', $this->routePrefix);
            $this->createUri = earthRoute($this->route.'.getCreate', $this->routePrefix);
            view()->share([
                'createUri' => $this->createUri,
                'listUri' => $this->listUri,
                'route' => $this->route
            ]);
        }

        return $this;
    }

}