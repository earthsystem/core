<?php namespace Earth\Core\Controllers\Module\Element;

class TemplatesController extends \Earth\Core\Controllers\System\Element\TemplatesController {

    /**
     * @param $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;
        view()->share('route', $this->route);
        return $this;
    }

}