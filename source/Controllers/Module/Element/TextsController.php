<?php namespace Earth\Core\Controllers\Module\Element;

class TextsController extends \Earth\Core\Controllers\System\Element\TextsController {

    /**
     * @var
     */
    public $route;

    /**
     * @param $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;
        view()->share('route', $this->route);
        return $this;
    }

}