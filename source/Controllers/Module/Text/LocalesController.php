<?php namespace Earth\Core\Controllers\Module\Text;

class LocalesController extends \Earth\Core\Controllers\System\Text\LocalesController {

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $type;

    /**
     * LocalesController constructor.
     */
    public function __construct()
    {
        $this->route = config('singleTextTypeModuleRoute');
        parent::__construct();
        view()->share('customSidebar', earthView('texts.sidebar', true));
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        config()->set('textType', $this->type);
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        view()->share('pageTitle', $this->title.' &raquo; '.earthLang('base.system').' &raquo; earth');
        return $this;
    }

    /**
     * @param $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;

        if (isset($this->route)) {
            $this->listUri = earthRoute($this->route.'.getIndex', $this->routePrefix);
            $this->createUri = earthRoute($this->route.'.getCreate', $this->routePrefix);
            view()->share([
                'createUri' => $this->createUri,
                'listUri' => $this->listUri,
                'route' => $this->route
            ]);
        }

        return $this;
    }

}