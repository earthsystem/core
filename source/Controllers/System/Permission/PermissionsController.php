<?php namespace Earth\Core\Controllers\System\Permission;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\User\User;
use Earth\Core\Services\Permission\PermissionService;
use Exception;

class PermissionsController extends CoreController {

    /**
     * @param PermissionService $permissionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(PermissionService $permissionService)
    {
        try {
            $permissions = $permissionService->store( request()->input() );
            return response()->json( ['message' => earthLang('permissions.permissions_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('permissions.error_updating_permissions')] , 500);
        }
    }

}