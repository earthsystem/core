<?php namespace Earth\Core\Controllers\System\Element;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Text\Text;
use Earth\Core\Services\Text\TextService;
use Exception;

class TextsController extends CoreController {

    /**
     * @var string
     */
    public $route = 'elements';

    /**
     * @param TextService $textService
     * @param $id
     * @return mixed
     */
    public function getAttach(TextService $textService, $id)
    {
        $this->shareViewToExtend();

        if ( request()->has("text_id") ) {
            try {
                $textService->attachToElement($id, request()->get("text_id"));
                view()->share([
                    'message' => earthLang('texts.text_added_to_element'),
                    'type' => 'success'
                ]);
            } catch (Exception $e) {
                view()->share([
                    'message' => earthLang('texts.text_not_added_to_element'),
                    'type' => 'danger'
                ]);
            }
        }

        $items = Text::notInElement($id)->latest()->search( request()->get('q'), ['name'] )->paginate(6);
        return earthView('texts.attach.container')->with('items', $items)->with('id', $id);
    }

    /**
     * @param TextService $textService
     * @param $id
     * @param $pivotId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putSort(TextService $textService, $id, $pivotId)
    {
        try {
            $textService->sort( $id, $pivotId, request()->input() );
            return response()->json( ['message' => earthLang('texts.text_sorted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('texts.error_sorting_text')] , 500);
        }
    }

    /**
     * @param TextService $textService
     * @param $id
     * @param $pivotId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TextService $textService, $id, $pivotId)
    {
        try {
            $text = $textService->detachFromElement($pivotId, false);
            return response()->json( ['message' => earthLang('texts.text_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('texts.error_deleting_text')] , 500);
        }
    }

}