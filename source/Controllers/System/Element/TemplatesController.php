<?php namespace Earth\Core\Controllers\System\Element;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Template\Template;
use Earth\Core\Services\Template\ConnectorService;
use Earth\Core\Services\Template\TemplateService;
use Exception;

class TemplatesController extends CoreController {

    /**
     * @var string
     */
    protected $route = 'elements';

    /**
     * @param $id
     * @param $position
     * @return mixed
     */
    public function getCreate($id, $position)
    {
        return earthView('elements.tabs.templates.create')->with('id', $id)->with('position', $position);
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(TemplateService $templateService, $id, $position)
    {
        try {
            $template = $templateService->store($id, $position, request()->input() );
            return response()->json( ['message' => earthLang('templates.template_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('templates.error_storing_template')] , 500);
        }
    }

    /**
     * @param ConnectorService $connectorService
     * @param $id
     */
    public function postConnector(ConnectorService $connectorService, $id)
    {
        return $connectorService->getFiles();
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $position
     * @return mixed
     */
    public function getAttach(TemplateService $templateService, $id, $position)
    {
        $this->shareViewToExtend();

        if ( request()->has("template_id") ) {
            try {
                $templateService->attachToElement($id, $position, request()->get("template_id"), [
                    'active' => 1,
                    'recursive' => 1
                ]);
                view()->share([
                    'message' => earthLang('templates.template_added_to_element'),
                    'type' => 'success'
                ]);
            } catch (Exception $e) {
                view()->share([
                    'message' => earthLang('templates.template_not_added_to_element'),
                    'type' => 'danger'
                ]);
            }
        }

        $items = Template::notInElementAndPosition($id, $position)->latest()->search( request()->get('q'), ['name'] )->paginate(6);
        return earthView('elements.tabs.templates.attach.container')->with('items', $items)->with('id', $id)->with('position', $position);
    }

    /**
     * @param $id
     * @param $templateId
     * @return mixed
     */
    public function getEdit($id, $templateId)
    {
        $template = Template::findOrFail($templateId);
        return earthView('elements.tabs.templates.edit')->with('id', $id)->with('templateId', $templateId)->with('template', $template);
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $templateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(TemplateService $templateService, $id, $templateId)
    {
        try {

            if ( request()->input("name") and request()->input("value") ) {
                $template = $templateService->updatePartially($id, $templateId, request()->input() );
            } else {
                $template = $templateService->update($id, $templateId, request()->input() );
            }

            return response()->json( ['message' => earthLang('templates.template_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('templates.error_updating_template')] , 500);
        }
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $templatePivotId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdatePivot(TemplateService $templateService, $id, $templatePivotId)
    {
        try {
            $template = $templateService->updatePivot($id, $templatePivotId, request()->input() );
            return response()->json( ['message' => earthLang('templates.template_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('templates.error_updating_template')] , 500);
        }
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $templateId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putSort(TemplateService $templateService, $id, $templateId)
    {
        try {
            $templateService->sort( $id, $templateId, request()->input() );
            return response()->json( ['message' => earthLang('templates.template_sorted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('templates.error_sorting_template')] , 500);
        }
    }

    /**
     * @param TemplateService $templateService
     * @param $id
     * @param $pivotId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TemplateService $templateService, $id, $pivotId)
    {
        try {
            $template = $templateService->detachFromElement($id, $pivotId, false);
            return response()->json( ['message' => earthLang('templates.template_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('templates.error_deleting_template')] , 500);
        }
    }

}