<?php namespace Earth\Core\Controllers\System\Element;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Element\Locale;
use Earth\Core\Services\Element\LocaleService;
use Exception;

class LocalesController extends CoreController {

    /**
     * @var string
     */
    public $route = 'elements';

    /**
     * @param $id
     * @return mixed
     */
    public function getCreate($id)
    {
        return earthView('elements.tabs.locales.create')->with('id', $id)->with('locales', get_locales());
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(LocaleService $localeService, $id)
    {
        try {
            $locale = $localeService->store($id, request()->input() );
            return response()->json( ['message' => earthLang('base.locale_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_storing_locale')] , 500);
        }
    }

    /**
     * @param $id
     * @param $localeId
     * @return mixed
     */
    public function getEdit($id, $localeId)
    {
        $item = Locale::findOrFail($localeId);

        return earthView('elements.tabs.locales.edit')
            ->with('item', $item)
            ->with('id', $id)
            ->with('localeId', $localeId)
            ->with('locales', get_locales());
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @param $localeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(LocaleService $localeService, $id, $localeId)
    {
        try {
            if ( request()->input("name") and request()->input("value") ) {
                $locale = $localeService->updatePartially($id, $localeId, request()->input() );
            } else {
                $locale = $localeService->update($id, $localeId, request()->input() );
            }

            return response()->json( ['message' => earthLang('base.locale_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_updating_locale')] , 500);
        }
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @param $localeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(LocaleService $localeService, $id, $localeId)
    {
        try {
            $locale = $localeService->delete($id, $localeId);
            return response()->json( ['message' => earthLang('base.locale_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_deleting_locale')] , 500);
        }
    }

}