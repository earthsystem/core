<?php namespace Earth\Core\Controllers\System\User;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Services\Role\RoleService;
use Earth\Core\Services\User\UserService;
use Exception;

class RolesController extends CoreController {

    /**
     * @param UserService $userService
     * @param $userId
     * @return mixed
     */
    public function getCreate(UserService $userService, $userId)
    {
        $this->shareViewToExtend();

        if ( request()->has("role_id") ) {
            try {
                $userService->attachRole($userId, request()->get("role_id"));
                view()->share([
                    'message' => earthLang('users.user_added_to_role'),
                    'type' => 'success'
                ]);
            } catch (Exception $e) {
                view()->share([
                    'message' => earthLang('users.user_not_added_to_role'),
                    'type' => 'danger'
                ]);
            }
        }

        $items = Role::doesNotHaveUser($userId)->orderBy('name')->search( request()->get('q'), ['name'] )->paginate(6);
        return earthView('roles.attach.container')->with('items', $items)->with('id', $userId);
    }

    /**
     * @param UserService $userService
     * @param $id
     * @param $roleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(UserService $userService, $id, $roleId)
    {
        try {
            $userService->detachRole($id, $roleId);
            return response()->json( ['message' => earthLang('users.deleted_role')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.not_deleted_role')] , 500);
        }
    }

}