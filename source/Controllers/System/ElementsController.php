<?php namespace Earth\Core\Controllers\System;

use Earth\Core\Entities\Element\Element;
use Earth\Core\Entities\Tab;
use Earth\Core\Presenters\System\CreateNewPresenter;
use Earth\Core\Presenters\System\DetailPresenter;
use Earth\Core\Presenters\System\ListPresenter;
use Earth\Core\Services\Element\ElementService;
use Exception;

class ElementsController extends CoreController {

    /**
     * @var string
     */
    public $route = 'elements';

    /**
     * ElementsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        view()->share('pageTitle', earthLang('elements.title').' &raquo; '.earthLang('base.system').' &raquo; earth');

        /**
         * Sidebar settings
         */
        view()->share([
            'sidebarLgWidth' => 3,
            'sidebarMdWidth' => 4,
            'contentLgWidth' => 9,
            'contentMdWidth' => 8
        ]);
    }

    /**
     * @return $this
     */
    public function getIndex()
    {
        return (new ListPresenter("elements.listContainer"))->render();
    }

    /**
     * @return mixed
     */
    public function getTree()
    {
        return earthView('elements.list')->with('actions', true);
    }

    /**
     * @param ElementService $elementService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTinyMCELinkList(ElementService $elementService)
    {
        $webElement = element()->whereData('web')->whereCanModify(0)->first();
        $descendants = $webElement->getDescendants()->toHierarchy();
        $links = $elementService->getDescendantsLinks($descendants);
        return response()->json($links);
    }

    /**
     * @param null $elementId
     * @return $this
     */
    public function getCreate($elementId = null)
    {
        $types = get_codes('element-types')->pluck('name', 'data');
        view()->share('types', $types);
        view()->share('options', $this->getOptions());
        view()->share('locales', get_locales());

        if (request()->has("element_id") and !isset($elementId)) {
            $elementId = request()->get("element_id");
        }

        // Custom list URI
        if (isset($this->listUri) and isset($elementId)) {
            view()->share('customListUri', earthRouteWithVars($this->route.".getShow", [$elementId]).earthRequestVars());
        }

        return (new CreateNewPresenter("elements.create"))->render([
            'elementId' => $elementId,
            'id' => $elementId
        ]);
    }

    /**
     * @param ElementService $elementService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(ElementService $elementService)
    {
        try {
            $element = $elementService->store( request()->input() );
            return response()->json( ['message' => earthLang('elements.element_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('elements.error_storing_element')] , 500);
        }
    }

    /**
     * @param $id
     * @param null $tab
     * @return $this
     */
    public function getShow($id, $tab = null)
    {
        $links = $this->getDetailLinks($id);

        view()->share([
            'createLabel' => earthLang('elements.create'),
            'customCreateUri' => earthRouteWithVars($this->route.'.getCreate', [$id], $this->routePrefix),
            'item' => Element::find($id),
            'links' => $links,
            'tab' => $tab,
            'localeCodes' => get_locales(),
            'textTypes' => get_codes('text-types')->pluck('data', 'data'),
            'dontShowTabs' => false
        ]);

        // Custom list URI
        if (isset($this->listUri)) {
            view()->share('customListUri', $this->listUri.earthRequestVars( request()->all() + ["element_id" => $id] ));
        }

        if (isset($tab)) {
            $tab = find_active_tab($tab, $links);
            $view = $tab->view;
        } else {
            $view = earthView("elements.view", true);
        }

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $links = $this->getDetailLinks($id);
        $types = get_codes('element-types')->pluck('name', 'data');

        view()->share([
            'createLabel' => earthLang('elements.create'),
            'customCreateUri' => earthRouteWithVars($this->route.'.getCreate', [$id], $this->routePrefix),
            'item' => Element::findOrFail($id),
            'types' => $types,
            'options' => $this->getOptions(),
            'links' => $links,
            'tab' => 'edit',
            'dontShowTabs' => (
                request()->has("rel_id") and
                request()->has("rel_route") and
                request()->has("rel_key")
            )
        ]);

        // Custom list URI
        if (isset($this->listUri)) {
            view()->share('customListUri', $this->listUri.earthRequestVars( request()->all() + ["element_id" => $id] ));
        }

        $view = earthView("elements.edit", true);

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param ElementService $elementService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(ElementService $elementService, $id)
    {
        try {
            $element = $elementService->update( $id, request()->input() );
            return response()->json( ['message' => earthLang('elements.element_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('elements.error_updating_element')] , 500);
        }
    }

    /**
     * @param ElementService $elementService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putSort(ElementService $elementService, $id)
    {
        try {
            $elementService->sort( $id, request()->input() );
            return response()->json( ['message' => earthLang('elements.element_sorted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('elements.error_sorting_element')] , 500);
        }
    }

    /**
     * @param ElementService $elementService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(ElementService $elementService, $id)
    {
        try {
            $element = $elementService->delete($id);
            return response()->json( ['message' => earthLang('elements.element_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('elements.error_deleting_element')] , 500);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailLinks($id)
    {
        return [
            new Tab("locales", $id, $this->route, "globe", earthView("elements.tabs.locales", true)),
            new Tab("texts", $id, $this->route, "file-text-o", earthView("elements.tabs.texts", true)),
            new Tab("templates", $id, $this->route, "th-large", earthView("elements.tabs.templates", true)),
            new Tab("custom-data", $id, $this->route, "bookmark", earthView("custom_data.list", true)),
            new Tab("permissions", $id, $this->route, "shield", earthView("elements.tabs.permissions", true)),
        ];
    }

    /**
     * @return array
     */
    private function getOptions()
    {
        $parents = element()->root()->descendantsAndSelf()->get(['id', 'name', 'depth']);
        return options_of_elements($parents);
    }

}