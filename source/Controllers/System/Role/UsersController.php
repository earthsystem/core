<?php namespace Earth\Core\Controllers\System\Role;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;
use Earth\Core\Services\Role\RoleService;
use Earth\Core\Services\User\UserService;
use Exception;

class UsersController extends CoreController {

    /**
     * @param RoleService $roleService
     * @param $roleId
     * @return mixed
     */
    public function getCreate(RoleService $roleService, $roleId)
    {
        $this->shareViewToExtend();

        if ( request()->has("user_id") ) {
            try {
                $roleService->attachUser($roleId, request()->get("user_id"));
                view()->share([
                    'message' => earthLang('roles.role_added_to_user'),
                    'type' => 'success'
                ]);
            } catch (Exception $e) {
                view()->share([
                    'message' => earthLang('roles.role_not_added_to_user'),
                    'type' => 'danger'
                ]);
            }
        }

        $items = User::doesNotHaveRole($roleId)->orderBy('last_name')->orderBy('first_name')->search( request()->get('q'), ['first_name', 'last_name', 'email', 'username'] )->paginate(6);
        return earthView('users.attach.container')->with('items', $items)->with('id', $roleId);
    }

    /**
     * @param RoleService $roleService
     * @param $id
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(RoleService $roleService, $id, $userId)
    {
        try {
            $roleService->detachUser($id, $userId);
            return response()->json( ['message' => earthLang('roles.deleted_user')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('roles.not_deleted_user')] , 500);
        }
    }

}