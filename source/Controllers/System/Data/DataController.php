<?php namespace Earth\Core\Controllers\System\Data;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Data\Data;
use Earth\Core\Services\Data\DataService;
use Exception;

class DataController extends CoreController {

    /**
     * @param DataService $dataService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(DataService $dataService)
    {
        try {
            $data = $dataService->store( request()->input() );
            return response()->json( ['message' => earthLang('base.data_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_storing_data')] , 500);
        }
    }

    /**
     * @param DataService $dataService
     * @param $id
     * @param null $prefix
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(DataService $dataService, $id, $prefix = null)
    {
        try {
            $data = $dataService->updatePartially($id, $prefix, request()->input() );
            return response()->json( ['message' => earthLang('base.data_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => $e->getMessage()] , 500);
            return response()->json( ['message' => earthLang('base.error_updating_data')] , 500);
        }
    }

    /**
     * @param DataService $dataService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(DataService $dataService, $id)
    {
        try {
            $data = $dataService->delete($id);
            return response()->json( ['message' => earthLang('base.data_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_deleting_data')] , 500);
        }
    }

}