<?php namespace Earth\Core\Controllers\System;

use Earth\Core\Entities\Activity\Activity;

class DashboardController extends CoreController {

    /**
     * @return mixed
     */
    public function getHome()
    {
        view()->share('pageTitle', 'Earth &raquo; System &raquo; Home');

        // Last 10 activities
        $latestActivities = Activity::with('user')->latest()->limit(10)->get();

        // Can create new texts?
        $system = element()->whereData('system')->whereType('app')->value('id');
        $texts = element()->whereData('texts')->whereType('link')->first();
        $types = get_codes("text-types")->pluck('data', 'data');

        $parents = element()->root()->descendantsAndSelf()->get(['id', 'name', 'depth']);
        $options = options_of_elements($parents);

        $locales = get_locales();
        $canCreateTexts = $texts->canWrite();

        return earthView('home.index')
            ->with('latestActivities', $latestActivities)
            ->with('canCreateTexts', $canCreateTexts)
            ->with('types', $types)
            ->with('locales', $locales)
            ->with('options', $options);
    }

}