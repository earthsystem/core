<?php namespace Earth\Core\Controllers\System;

use Illuminate\Routing\Controller;

class CoreController extends Controller {

    /**
     * @var string
     */
    protected $package = 'earth';

    /**
     * @var string
     */
    protected $routePrefix = 'earth.system';

    /**
     * @var
     */
    protected $listUri;

    /**
     * @var
     */
    protected $createUri;

    /**
     * CoreController constructor.
     */
    public function __construct()
    {
        view()->share([
            'header' => earthView('partials.header', true),
            'package' => $this->package,
            'routePrefix' => $this->routePrefix
        ]);

        if (isset($this->route)) {
            $this->listUri = earthRoute($this->route.'.getIndex', $this->routePrefix);
            $this->createUri = earthRoute($this->route.'.getCreate', $this->routePrefix);
            view()->share([
                'createUri' => $this->createUri,
                'listUri' => $this->listUri,
                'route' => $this->route
            ]);
        }
    }

    /**
     * Share the view to extend variable
     */
    protected function shareViewToExtend()
    {
        // Load view to extend
        $extendView = earthView('ajax', true);
        if ( request()->has("ajax_process") ) $extendView = earthView('ajax_app_only', true);
        view()->share('extendView', $extendView);
    }

}