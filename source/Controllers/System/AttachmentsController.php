<?php namespace Earth\Core\Controllers\System;

use Earth\Core\Services\Attachment\AttachmentService;
use Exception;
use Plupload;

class AttachmentsController extends CoreController {

    /**
     * @param $id
     * @param $class
     * @param $type
     * @return mixed
     */
    public function getShow($id, $class, $type)
    {
        $title = earthLang('base.'.$type);
        $item = $class::findOrFail($id);

        if ( request()->has("list") ) {
            return earthView('attachments.list')->with([
                'id' => $id,
                'class' => $class,
                'type' => $type,
                'title' => $title,
                'item' => $item
            ]);
        }

        if ( request()->has("list_and_upload") ) {
            return earthView('attachments.list_and_uploader')->with([
                'id' => $id,
                'class' => $class,
                'type' => $type,
                'title' => $title,
                'item' => $item
            ]);
        }

        return earthView('attachments.index')->with([
            'id' => $id,
            'class' => $class,
            'type' => $type,
            'title' => $title,
            'item' => $item
        ]);
    }

    /**
     * @param AttachmentService $attachmentService
     * @param $id
     * @param $class
     * @param $type
     * @return array
     */
    public function postUpload(AttachmentService $attachmentService, $id, $class, $type)
    {
        try {
            $attachment = Plupload::receive('file', function($file) use($id, $class, $type, $attachmentService){
                return $attachmentService->upload($id, $class, $type, $file);
            });
            return response()->json( ['message' => earthLang('attachments.attachment_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('attachments.error_storing_attachment')] , 500);
        }
    }

    /**
     * @param AttachmentService $attachmentService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putSort(AttachmentService $attachmentService, $id)
    {
        try {
            $attachmentService->sort( $id, request()->input() );
            return response()->json( ['message' => earthLang('attachments.attachment_sorted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('attachments.error_sorting_attachment')] , 500);
        }
    }

    /**
     * @param AttachmentService $attachmentService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(AttachmentService $attachmentService, $id)
    {
        try {
            $attachment = $attachmentService->delete($id);
            return response()->json( ['message' => earthLang('attachments.attachment_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('attachments.error_deleting_attachment')] , 500);
        }
    }

}