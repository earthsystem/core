<?php namespace Earth\Core\Controllers\System\Text;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\ModalTab;
use Earth\Core\Entities\Tab;
use Earth\Core\Entities\Text\Locale;
use Earth\Core\Entities\Text\Text;
use Earth\Core\Presenters\System\CreateNewPresenter;
use Earth\Core\Presenters\System\DetailPresenter;
use Earth\Core\Services\Text\LocaleService;
use Exception;

class LocalesController extends CoreController {

    /**
     * @var string
     */
    public $route = 'texts';

    /**
     * @param $id
     * @return mixed
     */
    public function getCreate($id)
    {
        $locales = get_locales();
        $item = Text::find($id);
        view()->share([
            'item' => $item,
            'locales' => $locales,
            'id' => $id,
            'dontShowTabs' => false,
            'tab' => "locales",
            'links' => $this->getDetailLinks($id)
        ]);

        $view = earthView("texts.tabs.locales.create", true);
        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(LocaleService $localeService, $id)
    {
        try {
            $locale = $localeService->store($id, request()->input() );
            return response()->json( ['message' => earthLang('base.locale_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_storing_locale')] , 500);
        }
    }

    /**
     * @param $id
     * @param $localeId
     * @return mixed
     */
    public function getEdit($id, $localeId)
    {
        $locale = Locale::findOrFail($localeId);

        $locales = get_locales();
        $item = Text::find($id);
        view()->share([
            'locale' => $locale,
            'item' => $item,
            'locales' => $locales,
            'localeId' => $localeId,
            'id' => $id,
            'dontShowTabs' => false,
            'tab' => "locales",
            'links' => $this->getDetailLinks($id)
        ]);

        $view = earthView("texts.tabs.locales.edit", true);
        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @param $localeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(LocaleService $localeService, $id, $localeId)
    {
        try {
            if ( request()->input("name") and request()->input("value") ) {
                $locale = $localeService->updatePartially($id, $localeId, request()->input() );
            } else {
                $locale = $localeService->update($id, $localeId, request()->input() );
            }

            return response()->json( ['message' => earthLang('base.locale_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_updating_locale')] , 500);
        }
    }

    /**
     * @param LocaleService $localeService
     * @param $id
     * @param $localeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(LocaleService $localeService, $id, $localeId)
    {
        try {
            $locale = $localeService->delete($id, $localeId);
            return response()->json( ['message' => earthLang('base.locale_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_deleting_locale')] , 500);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailLinks($id)
    {
        return [
            new Tab("locales", $id, $this->route, "globe", earthView("texts.tabs.locales", true)),
            new ModalTab("gallery", earthRouteWithVars('attachments.getShow', [$id, Text::class, "gallery"]), "image", earthRouteWithVars($this->route.".getShow", [$id]), null, null, "modal-lg"),
            new ModalTab("files", earthRouteWithVars('attachments.getShow', [$id, Text::class, "files"]), "file-o", earthRouteWithVars($this->route.".getShow", [$id]), null, null, "modal-lg"),
            new Tab("custom-data", $id, $this->route, "bookmark", earthView("custom_data.list", true)),
        ];
    }

}