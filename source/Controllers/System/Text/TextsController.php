<?php namespace Earth\Core\Controllers\System\Text;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\ModalTab;
use Earth\Core\Entities\Tab;
use Earth\Core\Entities\Text\Text;
use Earth\Core\Presenters\System\CreateNewPresenter;
use Earth\Core\Presenters\System\DetailPresenter;
use Earth\Core\Presenters\System\ListPresenter;
use Earth\Core\Services\Text\TextService;
use Exception;

class TextsController extends CoreController {

    /**
     * @var string
     */
    public $route = 'texts';

    /**
     * TextsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        view()->share('pageTitle', earthLang('texts.title').' &raquo; '.earthLang('base.system').' &raquo; earth');
        view()->share('locales', get_locales());
        view()->share('textTypes', get_codes('text-types')->pluck('name', 'data'));
    }

    /**
     * @return $this
     */
    public function getIndex()
    {
        return (new ListPresenter("texts.list"))->render();
    }

    /**
     * @return $this
     */
    public function getCreate()
    {
        if (request()->has("rel_id") and request()->has("rel_route") and request()->has("rel_key")) {
            view()->share('customListUri', earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ).earthRequestVars());
        }

        view()->share([
            'rel_id' => request()->get("rel_id"),
            'rel_route' => request()->get("rel_route"),
            'rel_key' => request()->get("rel_key"),
        ]);

        $types = get_codes('text-types')->pluck('name', 'data');
        view()->share('types', $types);

        return (new CreateNewPresenter("texts.create"))->render();
    }

    /**
     * @param TextService $textService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(TextService $textService)
    {
        try {
            $text = $textService->store( request()->input(), request()->files );
            return response()->json( ['message' => earthLang('texts.text_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('texts.error_storing_text')] , 500);
        }
    }

    /**
     * @param $id
     * @param null $tab
     * @return $this
     */
    public function getShow($id, $tab = null)
    {
        $links = $this->getDetailLinks($id);

        view()->share([
            'createLabel' => earthLang('texts.create'),
            'customCreateUri' => earthRouteWithVars($this->route.'.getCreate', [$id], $this->routePrefix),
            'item' => Text::findOrFail($id),
            'links' => $links,
            'tab' => $tab,
            'dontShowTabs' => false
        ]);

        // Custom list URI
        if (request()->has("rel_id") and request()->has("rel_route") and request()->has("rel_key")) {
            view()->share('customListUri', earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ).earthRequestVars());
        }

        if (isset($tab)) {
            $tab = find_active_tab($tab, $links);
            $view = $tab->view;
        } else {
            $view = earthView("texts.view", true);
        }

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $links = $this->getDetailLinks($id);
        $types = get_codes('text-types')->pluck('name', 'data');

        view()->share([
            'createLabel' => earthLang('texts.create'),
            'customCreateUri' => earthRouteWithVars($this->route.'.getCreate', [$id], $this->routePrefix),
            'item' => Text::findOrFail($id),
            'types' => $types,
            'links' => $links,
            'tab' => 'edit',
            'dontShowTabs' => (
                request()->has("rel_id") and
                request()->has("rel_route") and
                request()->has("rel_key")
            )
        ]);

        // Custom list URI
        if (request()->has("rel_id") and request()->has("rel_route") and request()->has("rel_key")) {
            view()->share('customListUri', earthRouteWithVars( request()->get("rel_route"), [request()->get("rel_id"), request()->get("rel_key")] ).earthRequestVars());
        }

        $view = earthView("texts.edit", true);

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param TextService $textService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(TextService $textService, $id)
    {
        try {
            if ( request()->input("name") and request()->input("value") ) {
                $text = $textService->updatePartially($id, request()->input() );
            } else {
                $text = $textService->update($id, request()->input(), request()->files );
            }

            return response()->json( ['message' => earthLang('texts.text_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('texts.error_updating_text')] , 500);
        }
    }

    /**
     * @param TextService $textService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TextService $textService, $id)
    {
        try {
            $text = $textService->delete($id);
            return response()->json( ['message' => earthLang('texts.text_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => $e->getMessage()] , 500);
            return response()->json( ['message' => earthLang('texts.error_deleting_text')] , 500);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailLinks($id)
    {
        return [
            new Tab("locales", $id, $this->route, "globe", earthView("texts.tabs.locales", true)),
            new ModalTab("gallery", earthRouteWithVars('attachments.getShow', [$id, Text::class, "gallery"]), "image", earthRouteWithVars($this->route.".getShow", [$id]), null, null, "modal-lg"),
            new ModalTab("files", earthRouteWithVars('attachments.getShow', [$id, Text::class, "files"]), "file-o", earthRouteWithVars($this->route.".getShow", [$id]), null, null, "modal-lg"),
            new Tab("custom-data", $id, $this->route, "bookmark", earthView("custom_data.list", true)),
        ];
    }

}