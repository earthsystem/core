<?php namespace Earth\Core\Controllers\System\Attachment;

use Earth\Core\Controllers\System\CoreController;
use Earth\Core\Entities\Attachment\Attachment;
use Earth\Core\Entities\Attachment\Locale;
use Earth\Core\Services\Attachment\LocaleService;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class LocalesController extends CoreController {

    /**
     * @param $attachableId
     * @param $attachableClass
     * @param $attachmentId
     */
    public function getIndex($attachableId, $attachableClass, $attachmentId)
    {
        $item = $attachableClass::findOrFail($attachableId);
        $attachment = Attachment::findOrFail($attachmentId);

        if ( method_exists($item, "locales") ) {
            $locales = $item->locales()->get(["locale"]);
        } else if ( isset($item->locale) ) {
            $locales = new Collection([$item]);
        }

        $locales->each( function($locale) use ($attachmentId) {
            $locale->attachment_locale = Locale::whereAttachmentId($attachmentId)->whereLocale($locale->locale)->first();
        });

        return earthView('attachments.locales.index')->with([
            'locales' => $locales,
            'attachableId' => $attachableId,
            'attachableClass' => $attachableClass,
            'attachmentId' => $attachmentId,
            'attachment' => $attachment,
            'item' => $item
        ]);
    }

    /**
     * @param LocaleService $localeService
     * @param $attachableId
     * @param $attachableClass
     * @param $attachmentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(LocaleService $localeService, $attachableId, $attachableClass, $attachmentId)
    {
        try {
            $localeService->store( $attachmentId, request()->input() );
            return response()->json( ['message' => earthLang('base.locale_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_storing_locale')] , 500);
        }
    }

    /**
     * @param LocaleService $localeService
     * @param $localeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(LocaleService $localeService, $localeId)
    {
        try {
            $localeService->update( $localeId, request()->input() );
            return response()->json( ['message' => earthLang('base.locale_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('base.error_updating_locale')] , 500);
        }
    }

}