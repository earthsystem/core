<?php namespace Earth\Core\Controllers\System\Attachment;

use Earth\Core\Entities\Attachment\Media;
use Earth\Core\Services\Attachment\FileManagerService;
use Exception;
use Illuminate\Routing\Controller;

class FileManagerController extends Controller {

    /**
     * @return mixed
     */
    public function getDialog()
    {
        $type = request()->get('type');
        $media = Media::latest();

        if ($type == "image") {
            $media = $media->whereIn('file_content_type', [
                'image/jpeg',
                'image/png',
                'image/tiff',
                'image/bmp',
                'image/gif'
            ]);
        } else if ($type == "media") {
            $media = $media->whereIn('file_content_type', [
                'video/mpeg',
                'video/quicktime',
                'video/x-msvideo',
                'video/mp4'
            ]);
        }

        if ( request()->has("list_only") ) {
            return earthView("attachments.manager.list")
                ->with('type', $type)
                ->with('field_name', request()->get("field_name") )
                ->with('media', $media->paginate(10));
        }

        return earthView('attachments.manager.dialog')
            ->with('type', $type)
            ->with('field_name', request()->get("field_name") )
            ->with('media', $media->paginate(10));
    }

    /**
     * @param FileManagerService $fileManagerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpload(FileManagerService $fileManagerService)
    {
        try {
            $media = $fileManagerService->upload( request()->file('file') );
            return response()->json( ['message' => earthLang('attachments.file_uploaded'), 'value' => $media], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('attachments.file_not_uploaded')] , 500);
        }
    }

}