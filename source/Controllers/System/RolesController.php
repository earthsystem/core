<?php namespace Earth\Core\Controllers\System;

use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\Tab;
use Earth\Core\Presenters\System\CreateNewPresenter;
use Earth\Core\Presenters\System\DetailPresenter;
use Earth\Core\Presenters\System\ListPresenter;
use Earth\Core\Services\Role\RoleService;
use Exception;

class RolesController extends CoreController {

    /**
     * @var string
     */
    public $route = 'roles';

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        view()->share('pageTitle', earthLang('roles.title').' &raquo; '.earthLang('base.system').' &raquo; earth');
    }

    /**
     * @return $this
     */
    public function getIndex()
    {
        return (new ListPresenter("roles.list"))->render();
    }

    /**
     * @return $this
     */
    public function getCreate()
    {
        return (new CreateNewPresenter("roles.create"))->render();
    }

    /**
     * @param RoleService $roleService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(RoleService $roleService)
    {
        try {
            $role = $roleService->store( request()->input() );
            return response()->json( ['message' => earthLang('roles.role_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('roles.error_storing_role')] , 500);
        }
    }

    /**
     * @param $id
     * @param null $tab
     * @return $this
     */
    public function getShow($id, $tab = null)
    {
        $links = $this->getDetailLinks($id);
        
        view()->share([
            'createLabel' => earthLang('roles.create'),
            'item' => Role::find($id),
            'links' => $links,
            'tab' => $tab,
            'dontShowTabs' => false
        ]);

        if (isset($tab)) {
            $tab = find_active_tab($tab, $links);
            $view = $tab->view;
        } else {
            $view = earthView($this->route . ".view", true);
        }
        
        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $links = $this->getDetailLinks($id);

        view()->share([
            'createLabel' => earthLang('roles.create'),
            'item' => Role::find($id),
            'links' => $links,
            'tab' => 'edit',
            'dontShowTabs' => (
                request()->has("rel_id") and
                request()->has("rel_route") and
                request()->has("rel_key")
            )
        ]);

        $view = earthView($this->route . ".edit", true);

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param RoleService $roleService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(RoleService $roleService, $id)
    {
        try {
            $role = $roleService->update( $id, request()->input() );
            return response()->json( ['message' => earthLang('roles.role_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('roles.error_updating_role')] , 500);
        }
    }

    /**
     * @param RoleService $roleService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(RoleService $roleService, $id)
    {
        try {
            $role = $roleService->delete($id);
            return response()->json( ['message' => earthLang('roles.role_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('roles.error_deleting_role')] , 500);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailLinks($id)
    {
        return [new Tab("users", $id, $this->route, "users")];
    }

}