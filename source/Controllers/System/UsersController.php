<?php namespace Earth\Core\Controllers\System;

use Earth\Core\Entities\Tab;
use Earth\Core\Entities\User\User;
use Earth\Core\Exceptions\PasswordNotMatchingException;
use Earth\Core\Presenters\System\CreateNewPresenter;
use Earth\Core\Presenters\System\DetailPresenter;
use Earth\Core\Presenters\System\ListPresenter;
use Earth\Core\Services\User\UserService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class UsersController extends CoreController {

    /**
     * @var string
     */
    public $route = 'users';

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        view()->share('pageTitle', earthLang('users.title').' &raquo; '.earthLang('base.system').' &raquo; earth');
    }

    /**
     * @return $this
     */
    public function getIndex()
    {
        return (new ListPresenter("users.list"))->render();
    }

    /**
     * @return $this
     */
    public function getCreate()
    {
        $types = get_codes('user-types')->pluck('name', 'data');
        view()->share('types', $types);
        
        return (new CreateNewPresenter("users.create"))->render();
    }

    /**
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStore(UserService $userService)
    {
        try {
            $user = $userService->store( request()->input() );
            return response()->json( ['message' => earthLang('users.user_stored')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.error_storing_user')] , 500);
        }
    }

    /**
     * @param $id
     * @param null $tab
     * @return $this
     */
    public function getShow($id, $tab = null)
    {
        $links = $this->getDetailLinks($id);
        
        view()->share([
            'createLabel' => earthLang('users.create'),
            'item' => User::find($id),
            'links' => $links,
            'tab' => $tab,
            'dontShowTabs' => false
        ]);

        if (isset($tab)) {
            $tab = find_active_tab($tab, $links);
            $view = $tab->view;
        } else {
            $view = earthView($this->route . ".view", true);
        }
        
        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $links = $this->getDetailLinks($id);
        $types = get_codes('user-types')->pluck('name', 'data');

        view()->share([
            'createLabel' => earthLang('users.create'),
            'item' => User::find($id),
            'links' => $links,
            'tab' => 'edit',
            'types' => $types,
            'dontShowTabs' => (
                request()->has("rel_id") and
                request()->has("rel_route") and
                request()->has("rel_key")
            )
        ]);

        $view = earthView($this->route . ".edit", true);

        return (new DetailPresenter)->render(['view' => $view]);
    }

    /**
     * @param UserService $userService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdate(UserService $userService, $id)
    {
        try {
            $user = $userService->update( $id, request()->input() );
            return response()->json( ['message' => earthLang('users.user_updated')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.error_updating_user')] , 500);
        }
    }

    /**
     * @param UserService $userService
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(UserService $userService, $id)
    {
        try {
            $user = $userService->delete($id);
            return response()->json( ['message' => earthLang('users.user_deleted')], 200);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.error_deleting_user')] , 500);
        }
    }

    /**
     * @return mixed
     */
    public function getSwitchUser()
    {
        $users = User::orderBy('last_name')->orderBy('first_name')->get()->pluck("full_name", "id");
        $aliasUserId = session("aliasUser")->id;
        return earthView("users.switchUser")->with('users', $users)->with('aliasUserId', $aliasUserId);
    }

    /**
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSwitchUser(UserService $userService)
    {
        try {
            $switchedUser = $userService->switchUser(request()->input('user_id'));
            return response()->json( ['message' => earthLang('users.user_switched')], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json( ['message' => earthLang('users.user_not_found')] , 500);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.error_switching_user')] , 500);
        }
    }

    /**
     * @return mixed
     */
    public function getChangePassword()
    {
        return earthView("users.changePassword");
    }

    /**
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangePassword(UserService $userService)
    {
        try {
            $changedPassword = $userService->changePassword(request()->input());
            return response()->json( ['message' => earthLang('users.password_changed')], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json( ['message' => earthLang('users.user_not_found')] , 500);
        } catch (PasswordNotMatchingException $e) {
            return response()->json( ['message' => earthLang('users.passwords_not_matching')] , 500);
        } catch (Exception $e) {
            return response()->json( ['message' => earthLang('users.error_changing_password')] , 500);
        }
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailLinks($id)
    {
        return [new Tab("roles", $id, $this->route, "tags")];
    }

}