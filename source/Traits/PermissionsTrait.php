<?php namespace Earth\Core\Traits;

use Earth\Core\Entities\Permission\Permission;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;
use Sentinel;

trait PermissionsTrait {

    /**
     * @param $type
     * @param $accessId
     * @param $accessType
     * @return Permission|\Illuminate\Database\Query\Builder
     */
    public function getPermission($type, $accessId, $accessType)
    {
        return Permission::whereType($type)
            ->whereObjectType( get_class() )
            ->whereObjectId($this->id)
            ->whereAccessType($accessType)
            ->whereAccessId($accessId);
    }

    /**
     * @param $type
     * @param $accessId
     * @return Permission|\Illuminate\Database\Query\Builder
     */
    public function getUserPermission($type, $accessId)
    {
        if (isset($this->userPermissionModel)) {
            return $this->getPermission($type, $accessId, $this->userPermissionModel);
        }

        return $this->getPermission($type, $accessId, User::class);
    }

    /**
     * @param $type
     * @param $accessId
     * @return Permission|\Illuminate\Database\Query\Builder
     */
    public function getRolePermission($type, $accessId)
    {
        if (isset($this->rolePermissionModel)) {
            return $this->getPermission($type, $accessId, $this->rolePermissionModel);
        }

        return $this->getPermission($type, $accessId, Role::class);
    }

    /**
     * @param $type
     * @param null $userId
     * @return bool
     */
    private function can($type, $userId = null)
    {
        if (!session()->has("adminUserRole")) {
            session()->set("adminUserRole", Sentinel::findRoleBySlug("admin"));
        }

        if (!session()->has("aliasUser")) {
            $user = Sentinel::getUser();
            session()->set("aliasUser", $user);
        }

        if (!isset($userId)) {
            $user = session("aliasUser");
        } else {
            $user = Sentinel::findByUserId($userId);
        }

        if ($user->inRole( session("adminUserRole") )) {
            return $type == "deny" ? false : true;
        }

        if ( $this->getUserPermission($type, $user->id)->count() ) return true;

        $user->roles = $user->roles()->get();
        session()->set("aliasUser", $user);

        foreach($user->roles as $role) {
            if ($role->slug == "admin") return true;
            if ( $this->getRolePermission($type, $role->id)->count() ) return true;
        }

        return false;
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function canRead($userId = null)
    {
        return $this->can("read", $userId);
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function canWrite($userId = null)
    {
        return $this->can("write", $userId);
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function canDestroy($userId = null)
    {
        return $this->can("destroy", $userId);
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function accessDenied($userId = null)
    {
        return $this->can("deny", $userId);
    }

}