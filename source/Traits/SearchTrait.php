<?php namespace Earth\Core\Traits;

trait SearchTrait {

    /**
     * @param $query
     * @param $q
     * @param array $fields
     * @return mixed
     */
    public function scopeSearch($query, $q = null, $fields = [])
    {
        $thisQuery = $query;

        if (isset($q)) {
            $thisQuery->where(function($query) use ($fields, $q){
                $newQuery = $query;
                foreach($fields as $index => $field) {
                    if ($index == 0) {
                        $newQuery = $newQuery->where($field, 'like', '%'.$q.'%');
                    } else {
                        $newQuery = $newQuery->orWhere($field, 'like', '%'.$q.'%');
                    }
                }
                return $newQuery;
            });
        }

        return $thisQuery;
    }

    /**
     * @param $query
     * @param $relation
     * @param $q
     * @param array $fields
     * @return mixed
     */
    public function scopeSearchByRelation($query, $relation, $q = null, $fields = [])
    {
        return $query->whereHas($relation, function($query) use ($q, $fields) {
            return $query->where(function($query) use($fields, $q) {
                $newQuery = $query;

                foreach($fields as $index => $field) {
                    if ($index == 0) {
                        $newQuery = $newQuery->where($field, 'like', '%'.$q.'%');
                    } else {
                        $newQuery = $newQuery->orWhere($field, 'like', '%'.$q.'%');
                    }
                }

                return $newQuery;
            });
        });
    }

}