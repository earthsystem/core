<?php namespace Earth\Core\Traits;

use Earth\Core\Entities\User\User;

trait UserTrait {

    /**
     * @return null
     */
    public function getCreatedByNameAttribute()
    {
        if (isset($this->created_by) and $user = User::find($this->created_by)) {
            return $user->full_name;
        }

        return null;
    }

    /**
     * @return null
     */
    public function getUpdatedByNameAttribute()
    {
        if (isset($this->updated_by) and $user = User::find($this->updated_by)) {
            return $user->full_name;
        }

        return null;
    }
    
}