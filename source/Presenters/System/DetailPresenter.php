<?php namespace Earth\Core\Presenters\System;

class DetailPresenter extends CorePresenter {

    /**
     * @var string
     */
    protected $view = 'boilerplate.detail';

}