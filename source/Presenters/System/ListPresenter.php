<?php namespace Earth\Core\Presenters\System;

class ListPresenter extends CorePresenter {

    /**
     * @var string
     */
    protected $view = 'boilerplate.list';

}