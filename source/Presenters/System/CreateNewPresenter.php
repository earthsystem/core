<?php namespace Earth\Core\Presenters\System;

class CreateNewPresenter extends CorePresenter {

    /**
     * @var string
     */
    protected $view = 'boilerplate.create';

}