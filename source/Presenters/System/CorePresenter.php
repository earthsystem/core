<?php namespace Earth\Core\Presenters\System;

class CorePresenter {

    /**
     * @var
     */
    protected $view;

    /**
     * @var
     */
    protected $customView;

    /**
     * CorePresenter constructor.
     * @param null $customView
     */
    public function __construct($customView = null)
    {
        if (isset($customView)) $this->customView = $customView;
    }

    /**
     * @param array $variables
     * @return $this
     */
    public function render($variables = [])
    {
        if ( request()->has("ajax") ) {

            if (isset($this->customView)) {
                return earthView($this->customView)->with($variables);
            }
            
            return earthView($this->view."_ajax")->with($variables);
        }

        if (isset($this->customView)) {
            $this->customView = earthView($this->customView, true);
        }

        return earthView($this->view)->with('customView', $this->customView)->with($variables);
    }

}