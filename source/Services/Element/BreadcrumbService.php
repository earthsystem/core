<?php namespace Earth\Core\Services\Element;

use Earth\Core\Entities\Element\Breadcrumb;
use Earth\Core\Entities\Element\Element;

class BreadcrumbService {

    /**
     * @param $locale
     */
    public function update($locale)
    {
        $breadcrumb = $this->checkAndCreateBreadcrumb($locale);

        // Update all descendants of the element
        $element = Element::whereId($locale->element_id)->first();
        $descendants = $element->getDescendants();

        foreach($descendants as $descendant) {
            $thisLocale = $descendant->locale()->whereLocale($locale->locale)->first();
            if ( isset($thisLocale) ) {
                $this->checkAndCreateBreadcrumb($thisLocale);
            }
        }
    }

    /**
     * @param $locale
     * @return mixed
     */
    private function checkAndCreateBreadcrumb($locale)
    {
        $breadcrumb = Breadcrumb::whereElementId($locale->element_id)->whereLocale($locale->locale)->first();

        if ($breadcrumb) {
            $breadcrumb->uri = $this->createBreadcrumb($locale);
            $breadcrumb->save();
        } else {
            $breadcrumb = Breadcrumb::create([
                'element_id' => $locale->element_id,
                'locale' => $locale->locale,
                'uri' => $this->createBreadcrumb($locale)
            ]);
        }

        return $breadcrumb;
    }

    /**
     * @param $locale
     * @return string
     */
    private function createBreadcrumb($locale)
    {
        $element = element()->whereId($locale->element_id)->first();

        // Get ancestors up until a certain point
        $ancestors = $element->getAncestorsWithoutRoot();
        if (isset($ancestors[0]) and $ancestors[0]->data == "web") $ancestors->shift();
        if (isset($ancestors[0]) and $ancestors[0]->type == "web-app") $ancestors->shift();
        if (isset($ancestors[0]) and $ancestors[0]->data == "hidden") $ancestors->shift();

        $slug = "";
        foreach($ancestors as $ancestor)  {
            $thisLocale = $ancestor->locale()->whereLocale($locale->locale)->first();
            if (isset($thisLocale)) {
                $slug .= "/".$thisLocale->slug;
            }
        }
        $slug .= "/".$element->locale()->whereLocale($locale->locale)->first()->slug;

        return $slug;
    }

}