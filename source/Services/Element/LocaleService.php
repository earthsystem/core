<?php namespace Earth\Core\Services\Element;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Element\Locale;
use Illuminate\Support\Str;

class LocaleService {

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function store($id, $input)
    {
        $element = element()->findOrFail($id);

        if ( empty($input['slug']) ) {
            $input['slug'] = Str::slug($input['title']);
        }

        $locale = $element->locales()->create([
            'title' => $input['title'],
            'slug' => $input['slug'],
            'locale' => $input['locale'],
            'comment' => isset($input['comment']) ? $input['comment'] : null
        ]);

        if ($locale) {
            (new BreadcrumbService)->update($locale);
            new ObjectInsertedAction($locale);
            Activity::log("Translation [".$input['locale']."] with title [".$locale->title."] was added to element with ID [".$id."].", session("aliasUser")->id);
        }

        return $locale;
    }

    /**
     * @param $id
     * @param $localeId
     * @param $input
     * @return bool
     */
    public function update($id, $localeId, $input)
    {
        $locale = Locale::findOrFail($localeId);
        $locale->title = $input['title'];
        $locale->locale = $input['locale'];
        $locale->slug = $input['slug'];
        $locale->comment = $input['comment'];

        if ($locale->save()) {
            (new BreadcrumbService)->update($locale);
            new ObjectUpdatedAction($locale);
            Activity::log("Translation with ID [".$locale->id."] and locale [".$locale->locale."] and title [".$locale->title."] was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $localeId
     * @param $input
     * @return bool
     */
    public function updatePartially($id, $localeId, $input)
    {
        $locale = Locale::findOrFail($localeId);
        $locale->{$input['name']} = $input['value'];

        if (!$locale->save()) {
            return false;
        }

        if ($input['name'] == "slug") {
            (new BreadcrumbService)->update($locale);
        }

        new ObjectUpdatedAction($locale);
        Activity::log("Translation with ID [".$locale->id."] and locale [".$locale->locale."] and title [".$locale->title."] was updated.", session("aliasUser")->id);
        return true;
    }

    /**
     * @param $id
     * @param $localeId
     * @return bool|null
     * @throws \Exception
     */
    public function delete($id, $localeId)
    {
        if (Str::contains($localeId, ",")) {
            $ids = explode(",", $localeId);
            $items = Locale::findMany($ids);

            foreach($items as $item) {
                if ($item->delete($item->id)) {
                    new ObjectDeletedAction($item);
                    Activity::log("Translation with ID [".$item->id."] and locale [".$item->locale."] and title [".$item->title."] was deleted.", session("aliasUser")->id);
                }
            }
        } else {
            $locale = Locale::findOrFail($localeId);
            if ($locale->delete($id, $localeId)) {
                new ObjectDeletedAction($locale);
                Activity::log("Translation with ID [".$locale->id."] and locale [".$locale->locale."] and title [".$locale->title."] was deleted.", session("aliasUser")->id);
                return true;
            }

            return false;
        }
    }

}