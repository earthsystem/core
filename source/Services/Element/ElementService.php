<?php namespace Earth\Core\Services\Element;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Exception;
use Illuminate\Support\Str;

class ElementService {

    /**
     * @param $input
     */
    public function store($input)
    {
        $parent = element()->findOrFail($input['parent_id']);

        if ( empty($input['data']) ) {
            $input['data'] = Str::slug($input['name']);
        }

        $item = $parent->children()->create([
            'name' => $input['name'],
            'data' => $input['data'],
            'type' => $input['type'],
            'icon' => $input['icon'],
            'active' => isset($input['active'])
        ]);

        if ($item) {
            new ObjectInsertedAction($item);
            Activity::log("Element with name [".$item->name."] was added. Parent object is [".$item->parent_name."] with ID [".$item->parent_id."].", session("aliasUser")->id);

            if ( !empty($input['locale']) ) {
                (new LocaleService)->store($item->id, [
                    'title' => $input['name'],
                    'locale' => $input['locale'],
                    'comment' => $input['comment']
                ]);
            }
        }

        return $item;
    }

    /**
     * @param $id
     * @param $input
     * @return bool
     */
    public function update($id, $input)
    {
        $item = element()->findOrFail($id);
        $item->active = isset($input['active']);
        $item->name = $input['name'];
        $item->data = $input['data'];
        $item->type = $input['type'];
        $item->icon = $input['icon'];

        if (isset($input['parent_id']) and $item->parent_id != $input['parent_id']) {
            $parent = element()->findOrFail($input['parent_id']);
            $item->makeChildOf($parent);
            Activity::log("Element with name [".$item->name."] and ID [".$item->id."] belongs to a different parent now. Parent object is [".$item->parent_name."] with ID [".$item->parent_id."].", session("aliasUser")->id);
        }

        if ($item->save()) {
            new ObjectUpdatedAction($item);
            Activity::log("Element with name [".$item->name."] and ID [".$item->id."] was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $input
     * @throws Exception
     */
    public function sort($id, $input)
    {
        if (!isset($input['relItemId'])) throw new Exception("Missing relItemId.");
        if (!isset($input['direction'])) throw new Exception("Missing direction.");
        if (!in_array($input['direction'], ['left', 'right'])) throw new Exception("Direction not valid.");

        $item = element()->findOrFail($id);
        $relItem = element()->findOrFail($input['relItemId']);

        if ($input['direction'] == "left") {
            if ( !$item->moveToLeftOf($relItem) ) throw new Exception("Error sorting to the left.");
        } else {
            if ( !$item->moveToRightOf($relItem) ) throw new Exception("Error sorting to the right.");
        }

        return;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function delete($id)
    {
        $item = element()->findOrFail($id);

        if ($item->delete($id)) {
            new ObjectDeletedAction($item);
            Activity::log("Element with name [".$item->name."] and ID [".$item->id."] was deleted.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $items
     * @return array
     */
    public function getDescendantsLinks($items)
    {
        $links = [];

        foreach($items as $item) {
            $thisItem = ['title' => $item->name, 'value' => '{category:'.$item->id.'}'];

            if ($item->children->count()) {
                if ($item->data != "home" and $item->type != "web-app") {
                    $thisMenu = [['title' => "(parent)", 'value' => '{category:'.$item->id.'}']];
                } else {
                    $thisMenu = [];
                }

                $thisMenu = array_merge($thisMenu, $this->getDescendantsLinks($item->children));
                $thisItem['menu'] = $thisMenu;
            }

            $links[] = $thisItem;
        }

        return $links;
    }

}