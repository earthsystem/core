<?php namespace Earth\Core\Services\Attachment;

use Activity;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Entities\Attachment\Attachment;
use Earth\Core\Entities\Attachment\File;
use Earth\Core\Entities\Attachment\Image;
use Earth\Core\Entities\Attachment\Media;
use Exception;
use Illuminate\Support\Str;

class AttachmentService {

    /**
     * @param $id
     * @param $class
     * @param $type
     * @param $file
     * @return bool
     * @throws Exception
     */
    public function upload($id, $class, $type, $file)
    {
        if ($type == "files") {
            $className = File::class;
        } else if ($type == "gallery") {
            $className = Image::class;
        } else {
            $className = Media::class;
        }

        $media = $className::create( ['file' => $file] );
        if (!$media) throw new Exception("Cannot create media.");

        // Find the item
        $item = $class::findOrFail($id);
        $newPosition = $item->attachments()->whereType($type)->orderBy('position', 'desc')->value('position');
        if (!isset($newPosition)) $newPosition = 0;

        $attachment = $item->attachments()->create([
            'media_id' => $media->id,
            'type' => $type,
            'position' => $newPosition + 1
        ]);

        if (!$attachment) throw new Exception("Error creating attachment.");

        if ( isset($item->title) ) {
            Activity::log("Attachment [".$media->file->originalFileName()."] was uploaded to content translation '".$item->title."' [".$item->id."].", session("aliasUser")->id);
        } else if ( isset($item->name) ) {
            Activity::log("Attachment [".$media->file->originalFileName()."] was uploaded to content '".$item->name."' [".$item->id."].", session("aliasUser")->id);
        } else {
            Activity::log("Attachment [".$media->file->originalFileName()."] was uploaded to object [".get_class($item)."].", session("aliasUser")->id);
        }

        new ObjectInsertedAction($attachment);
        return true;
    }

    /**
     * @param $id
     * @param $input
     * @return bool
     */
    public function sort($id, $input)
    {
        $item = Attachment::findOrFail($id);
        $currentPosition = $item->position;
        $item->position = -1;
        $item->save();

        try {

            if ($input['direction'] == "left") {
                Attachment::whereAttachableId($item->attachable_id)
                    ->whereAttachableType($item->attachable_type)
                    ->whereType($item->type)
                    ->where('position', '>=', $input['newPosition'])
                    ->where('position', '<', $currentPosition)
                    ->increment('position');
            } else {
                Attachment::whereAttachableId($item->attachable_id)
                    ->whereAttachableType($item->attachable_type)
                    ->whereType($item->type)
                    ->where('position', '<=', $input['newPosition'])
                    ->where('position', '>', $currentPosition)
                    ->decrement('position');
            }

            $item->position = $input['newPosition'];
            return $item->save();

        } catch (Exception $e) {
            // Update the current item back to original position
            $item->position = $currentPosition;
            $item->save();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        if (Str::contains($id, ",")) {
            $ids = explode(",", $id);
            $items = Attachment::findMany($ids);

            foreach($items as $item) {
                $this->deleteSingle($item);
            }
        } else {
            $item = Attachment::findOrFail($id);
            return $this->deleteSingle($item);
        }
    }

    /**
     * @param $item
     * @return mixed
     */
    private function deleteSingle($item)
    {
        Attachment::whereAttachableId($item->attachable_id)
            ->whereAttachableType($item->attachable_type)
            ->whereType($item->type)
            ->where('position', '>', $item->position)
            ->decrement('position');

        if ($item->delete($item->id)) {

            $media = $item->media;
            $objectClass = $item->attachable_type;
            $objectId = $item->attachable_id;
            $object = $objectClass::find($objectId);

            if ( isset($object) ) {
                if ( isset($object->title) ) {
                    Activity::log("Attachment [".$media->file->originalFileName()."] [".$item->id."] was deleted from content locale '".$object->title."' [".$object->id."].", session("aliasUser")->id);
                } else if ( isset($object->name) ) {
                    Activity::log("Attachment [".$media->file->originalFileName()."] [".$item->id."] was deleted from content '".$object->name."' [".$object->id."].", session("aliasUser")->id);
                } else {
                    Activity::log("Attachment [".$media->file->originalFileName()."] [".$item->id."] was deleted.", session("aliasUser")->id);
                }
            }

            return true;
        }

        return false;
    }
    
}