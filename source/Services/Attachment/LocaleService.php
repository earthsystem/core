<?php namespace Earth\Core\Services\Attachment;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Attachment\Locale;

class LocaleService {

    /**
     * @param $attachmentId
     * @param $input
     * @return bool
     */
    public function store($attachmentId, $input)
    {
        $locale = Locale::create([
            'locale' => $input['locale'],
            'attachment_id' => $attachmentId,
            'title' => $input['title']
        ]);

        if ($locale) {
            new ObjectInsertedAction($locale);
            Activity::log("Attachment locale [".$input['locale']."] with title '".$input['title']."' for attachment with ID [".$attachmentId."] was inserted.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $localeId
     * @param $input
     * @return bool|null
     */
    public function update($localeId, $input)
    {
        if ( isset($localeId) and empty($input['title']) ) {
            return $this->delete($localeId);
        }

        $locale = Locale::findOrFail($localeId);
        $locale->title = $input['title'];

        if ($locale->save()) {
            new ObjectUpdatedAction($locale);
            Activity::log("Attachment locale [".$input['locale']."] with title '".$input['title']."' for attachment with ID [".$locale->attachment_id."] was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $localeId
     * @return bool|null
     * @throws \Exception
     */
    private function delete($localeId)
    {
        $locale = Locale::findOrFail($localeId);

        if ($locale->delete($localeId)) {
            new ObjectDeletedAction($locale);
            Activity::log("Attachment locale [".$locale->locale."] with title '".$locale->locale."' for attachment with ID [".$locale->attachment_id."] was deleted.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

}