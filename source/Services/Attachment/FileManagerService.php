<?php namespace Earth\Core\Services\Attachment;

use Earth\Core\Entities\Attachment\File;
use Earth\Core\Entities\Attachment\Image;
use Earth\Core\Entities\Attachment\Media;
use Exception;

class FileManagerService {

    /**
     * @param $file
     * @return static
     * @throws Exception
     */
    public function upload($file)
    {
        $type = request()->get("type");

        if ($type == "image") {
            $className = Image::class;
        } else {
            $className = File::class;
        }

        $media = $className::create( [ 'file' => $file ] );
        if (!$media) throw new Exception("Cannot create media.");

        return $media->file->url();
    }

}