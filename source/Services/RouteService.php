<?php namespace Earth\Core\Services;

class RouteService {

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @param $route
     */
    public function add($route)
    {
        $this->routes[$route->domain] = $route;
    }

    /**
     * Run the routes
     */
    public function run()
    {
        if ( app()->runningInConsole() ) return;

        $domain = config('earth.system_domain');
        $visitedDomain = request()->server('HTTP_HOST');
        $slug = request('slug');

        // Check if the route is admin
        $isAdmin = $domain and $visitedDomain == $domain and $slug === NULL;

        if (!$isAdmin) {

            if ( !$earthRoute = $this->getRoute($visitedDomain) ) {
                abort(404, "missing_route_for_this_domain");
            }

            if ( method_exists($earthRoute, "middleware") ) {
                $earthRoute->middleware();
            }

            if ( method_exists($earthRoute, "composers") ) {
                $earthRoute->composers();
            }

            $earthRoute->register();

        }
    }

    /**
     * @param $visitedDomain
     * @return bool|mixed
     */
    public function getRouteInstance($visitedDomain)
    {
        if ( !$this->hasRoute($visitedDomain) ) {
            return false;
        }

        return $this->routes[ $this->getParsedDomain($visitedDomain) ];
    }

    /**
     * @param $visitedDomain
     * @return bool
     */
    private function getRoute($visitedDomain)
    {
        if ( !$this->hasRoute($visitedDomain) ) {
            return false;
        }

        $thisRoute = $this->routes[ $this->getParsedDomain($visitedDomain) ];
        config()->set('earth.protectRoutes', $thisRoute->protected);
        return $thisRoute->adapter;
    }

    /**
     * @param $visitedDomain
     * @return bool
     */
    private function hasRoute($visitedDomain)
    {
        $visitedDomain = $this->getParsedDomain($visitedDomain);

        if ( !isset($this->routes[$visitedDomain]) ) {
            return false;
        }

        return true;
    }

    /**
     * @param $visitedDomain
     * @return mixed
     */
    private function getParsedDomain($visitedDomain)
    {
        return str_replace("www.", "", $visitedDomain);
    }

}