<?php namespace Earth\Core\Services\Text;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Text\Locale;
use Earth\Core\Entities\Text\Text;
use Illuminate\Support\Str;

class LocaleService {

    /**
     * @param $textId
     * @param $input
     * @return bool
     */
    public function store($textId, $input)
    {
        $text = Text::findOrFail($textId);

        $attributes = [
            'active' => isset($input['active']),
            'title' => $input['title'],
            'subtitle' => isset($input['subtitle']) ? $input['subtitle'] : null,
            'summary' => isset($input['summary']) ? $input['summary'] : null,
            'data' => $input['content'],
            'locale' => $input['locale'],
        ];

        if ( !empty($input['publish_from']) ) {
            $attributes['publish_from'] = date('Y-m-d', strtotime($input['publish_from'])).' 00:00:00';
        }
        if ( !empty($input['publish_to']) ) {
            $attributes['publish_to'] = date('Y-m-d', strtotime($input['publish_to'])).' 00:00:00';
        }

        $locale = $text->locales()->create($attributes);

        if ($locale) {
            Activity::log("Translation [".$locale->locale."] with title '".$locale->title."' was added to the content '".$text->name."' with ID [".$text->id."]", session("aliasUser")->id);
            new ObjectInsertedAction($locale);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $localeId
     * @param $input
     * @return bool
     */
    public function update($id, $localeId, $input)
    {
        $item = Locale::findOrFail($localeId);
        $item->active = isset($input['active']);
        $item->title = $input['title'];
        $item->subtitle = $input['subtitle'];
        $item->summary = $input['summary'];
        $item->data = $input['content'];
        $item->locale = $input['locale'];

        if ( !empty($input['publish_from']) ) {
            $item->publish_from = date('Y-m-d', strtotime($input['publish_from'])).' 00:00:00';
        } else {
            $item->publish_from = null;
        }
        if ( !empty($input['publish_to']) ) {
            $item->publish_to = date('Y-m-d', strtotime($input['publish_to'])).' 00:00:00';
        } else {
            $item->publish_to = null;
        }

        if ($item->save()) {
            new ObjectUpdatedAction($item);
            Activity::log("Translation [".$item->locale."] with title '".$item->title."' was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $localeId
     * @param $input
     * @return bool
     */
    public function updatePartially($id, $localeId, $input)
    {
        if ($input["value"] == "true") $input["value"] = 1;

        $locale = Locale::findOrFail($localeId);
        $locale->{$input['name']} = $input['value'];

        if ($locale->save()) {
            new ObjectUpdatedAction($locale);
            Activity::log("Translation [".$locale->locale."] with title '".$locale->title."' was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $localeId
     * @return mixed
     */
    public function delete($id, $localeId)
    {
        $text = Text::find($id);

        if (Str::contains($localeId, ",")) {
            $ids = explode(",", $localeId);
            $items = Locale::findMany($ids);

            foreach($items as $item) {
                $item->delete($item->id);
                Activity::log("Translation [".$item->locale."] with title '".$item->title."' was deleted from the content '".$text->name."' with ID [".$text->id."]", session("aliasUser")->id);
                new ObjectDeletedAction($item);
            }
        } else {
            $locale = Locale::findOrFail($localeId);
            $locale->delete($id, $localeId);
            Activity::log("Translation [".$locale->locale."] with title '".$locale->title."' was deleted from the content '".$text->name."' with ID [".$text->id."]", session("aliasUser")->id);
            new ObjectDeletedAction($locale);
            return true;
        }
    }

}