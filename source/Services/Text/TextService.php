<?php namespace Earth\Core\Services\Text;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Element\Element;
use Earth\Core\Entities\Element\Text as ElementText;
use Earth\Core\Entities\Text\Text;
use Earth\Core\Services\Text\LocaleService;
use Exception;
use Illuminate\Support\Str;

class TextService {

    /**
     * @param $input
     * @param null $files
     * @return bool
     */
    public function store($input, $files = null)
    {
        if (!isset($input['name'])) {
            $input['name'] = substr($input['title'], 0, 255);
        }

        $attributes = [
            'name' => $input['name'],
            'type' => $input['type'],
        ];

        if ( !empty($input['created_at']) ) {
            $attributes['created_at'] = date("Y-m-d", strtotime($input['created_at'])) . " 00:00:00";
        }

        if ( $files->has("image") ) {
            $attributes['image'] = $files->get("image");
        }

        $item = Text::create($attributes);

        if ($item) {
            new ObjectInsertedAction($item);
            Activity::log("Content with name '".$item->name."' was created.", session("aliasUser")->id);
            (new LocaleService)->store($item->id, $input);

            if ( request()->has("rel_id") and request()->has("rel_route") and request()->get("rel_route") == "elements.getShow" ) {
                $this->attachToElement(request()->get("rel_id"), $item->id);
            }

            return true;
        }

        return false;
    }

    /**
     * @param $elementId
     * @param $textId
     */
    public function attachToElement($elementId, $textId)
    {
        $text = Text::findOrFail($textId);
        $element = Element::findOrFail($elementId);
        
        $newPosition = $element->texts()->orderBy('position', 'desc')->value('position');
        if (!isset($newPosition)) $newPosition = 0;

        $text->elements()->attach($elementId, [
            'position' => $newPosition + 1
        ]);
        Activity::log("Content with name '".$text->name."' was attached to the element with name '".$element->name."' [".$element->id."].", session("aliasUser")->id);
    }

    /**
     * @param $id
     * @param $input
     * @param null $files
     * @return bool
     */
    public function update($id, $input, $files = null)
    {
        $text = Text::findOrFail($id);
        $text->name = $input['name'];
        $text->type = $input['type'];

        if ( isset($input['created_at'])
            and $input['created_at'] != date('d.m.Y')
            and !empty($input['created_at']) ) {
            $text->created_at = date("Y-m-d", strtotime($input['created_at'])) . " 00:00:00";
        }

        if ( !$files->has("image") and isset($input['delete_image']) ) {
            $text->image = STAPLER_NULL;
        }

        if ( $files->has("image") ) {
            $text->image = $files->get("image");
        }

        if ($text->save()) {
            Activity::log("Content with name '".$text->name."' [".$text->id."] was updated.", session("aliasUser")->id);
            new ObjectUpdatedAction($text);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $input
     * @return bool
     */
    public function updatePartially($id, $input)
    {
        if ($input["value"] == "true") $input["value"] = 1;

        $text = Text::findOrFail($id);
        $text->{$input['name']} = $input['value'];

        if (!$text->save()) {
            Activity::log("Content with name '".$text->name."' [".$text->id."] was updated.", session("aliasUser")->id);
            new ObjectUpdatedAction($text);
        }

        return false;
    }

    /**
     * @param $id
     * @param $pivotId
     * @param $input
     * @return bool
     */
    public function sort($id, $pivotId, $input)
    {
        $item = ElementText::findOrFail($pivotId);
        $currentPosition = $item->position;
        $item->position = -1;
        $item->save();

        try {

            if ($input['direction'] == "left") {
                ElementText::whereElementId($item->element_id)
                    ->where('position', '>=', $input['newPosition'])
                    ->where('position', '<', $currentPosition)
                    ->increment('position');
            } else {
                ElementText::whereElementId($item->element_id)
                    ->where('position', '<=', $input['newPosition'])
                    ->where('position', '>', $currentPosition)
                    ->decrement('position');
            }

            $item->position = $input['newPosition'];
            return $item->save();

        } catch (Exception $e) {
            // Update the current item back to original position
            $item->position = $currentPosition;
            $item->save();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $item = Text::findOrFail($id);

        foreach($item->elements as $element) {
            $this->detachFromElement($element->pivot->id);
        }

        $item->delete($id);
        Activity::log("Content with name '".$item->name."' [".$item->id."] was deleted.", session("aliasUser")->id);
        new ObjectDeletedAction($item);
        return true;
    }

    /**
     * @param $pivotId
     * @return mixed
     */
    public function detachFromElement($pivotId)
    {
        if (Str::contains($pivotId, ",")) {
            $ids = explode(",", $pivotId);
            $items = ElementText::findMany($ids);

            foreach($items as $item) {
                ElementText::whereElementId($item->element_id)->where('position', '>', $item->position)->decrement('position');
                $element = element()->find($item->element_id);
                $text = Text::find($item->text_id);
                $item->delete($pivotId);
                Activity::log("Content with name '".$text->name."' [".$text->id."] was detached from the element with name '".$element->name."' [".$element->id."].", session("aliasUser")->id);
            }
        } else {
            $item = ElementText::findOrFail($pivotId);
            ElementText::whereElementId($item->element_id)->where('position', '>', $item->position)->decrement('position');
            $element = element()->find($item->element_id);
            $text = Text::find($item->text_id);

            $item->delete($pivotId);
            Activity::log("Content with name '".$text->name."' [".$text->id."] was detached from the element with name '".$element->name."' [".$element->id."].", session("aliasUser")->id);
            return true;
        }
    }

}