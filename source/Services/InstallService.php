<?php namespace Earth\Core\Services;

use DB;
use Earth\Core\Entities\Element\Element;
use Earth\Core\Entities\Text\Text;
use Faker\Factory;
use File;
use Illuminate\Support\Str;
use RuntimeException;
use Sentinel;

class InstallService {

    /**
     * Move migration files
     *
     * @return bool
     */
    public function moveMigrationFiles()
    {
        $source = __DIR__ . '/../../migrations';
        $destination = base_path('database/migrations');

        if (File::copyDirectory($source, $destination)) {
            return true;
        }

        throw new RuntimeException("Unable to publish migrations");
    }

    /**
     * Seed users and roles
     *
     * @return void
     */
    public function seedUsersAndRoles()
    {
        $tables = [ 'activations', 'persistences', 'reminders', 'roles', 'role_users', 'throttle', 'users' ];

        foreach($tables as $table) {
            DB::table($table)->delete();
        }

        $identicalData = [
            'password' => env('DEFAULT_PASSWORD', 'waterandfire'),
            'activated' => 1,
            'type' => 'default',
            'can_modify' => 0
        ];

        $users = [
            'lovro.papez@gmail.com' => [ 'first_name' => 'Lovro', 'last_name' => 'Papež' ],
            'leon@sdlev.si' => [ 'first_name' => 'Leon', 'last_name' => 'Papež' ],
        ];

        $role = Sentinel::getRoleRepository()->createModel()->create([ 'name' => 'Admin', 'slug' => 'admin' ]);
        $role->can_modify = 0;
        $role->save();

        foreach($users as $email => $data) {
            $data['email'] = $email;
            $user = Sentinel::registerAndActivate( array_merge($identicalData, $data) );
            $user->can_modify = 0;
            $user->type = 'default';
            $user->save();
            $role->users()->attach($user);
        }
    }

    /**
     * Seed application structure
     *
     * @return void
     */
    public function seedApplicationStructure()
    {
        DB::table('elements')->delete();

        $user = Sentinel::getUserRepository()->findByCredentials([ 'email' => 'lovro.papez@gmail.com' ]);

        $identicalData = [
            'created_by' => isset($user) ? $user->getUserId() : 0,
            'updated_by' => isset($user) ? $user->getUserId() : 0,
            'can_modify' => 0,
        ];
        
        $elements = [
            $identicalData + [
                'name' => 'Root',
                'data' => 'root',
                'children' => [

                    // System
                    $identicalData + [
                        'name' => 'System',
                        'data' => config('earth.system_prefix'),
                        'type' => 'app',
                        'icon' => 'cogs',
                        'children' => [

                            // Admin
                            $identicalData + [
                                'name' => 'Admin',
                                'data' => 'admin',
                                'type' => 'group',
                                'icon' => 'gear',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'Elements',
                                        'data' => 'elements',
                                        'type' => 'link',
                                        'icon' => 'cube'
                                    ],

                                    $identicalData + [
                                        'name' => 'Texts',
                                        'data' => 'texts',
                                        'type' => 'link',
                                        'icon' => 'pencil-square-o'
                                    ],

                                    $identicalData + [
                                        'name' => 'Attachments',
                                        'data' => 'attachments',
                                        'type' => 'link-hidden',
                                    ],

                                    $identicalData + [
                                        'name' => 'Permissions',
                                        'data' => 'permissions',
                                        'type' => 'link-hidden',
                                    ],

                                    $identicalData + [
                                        'name' => 'Data',
                                        'data' => 'data',
                                        'type' => 'link-hidden',
                                    ],

                                    $identicalData + [
                                        'name' => 'User management',
                                        'data' => 'user-management',
                                        'type' => 'group',
                                        'icon' => 'users',
                                        'children' => [

                                            $identicalData + [
                                                'name' => 'Roles',
                                                'data' => 'roles',
                                                'type' => 'link',
                                                'icon' => 'users'
                                            ],

                                            $identicalData + [
                                                'name' => 'Users',
                                                'data' => 'users',
                                                'type' => 'link',
                                                'icon' => 'user'
                                            ]

                                        ]
                                    ]

                                ]

                            ]
                        ]
                    ],

                    // Web
                    $identicalData + [
                        'name' => 'Web',
                        'data' => 'web'
                    ],

                    // Codes
                    $identicalData + [
                        'name' => 'Codes',
                        'data' => 'codes',
                        'children' => [

                            // Template positions
                            $identicalData + [
                                'name' => 'Template positions',
                                'data' => 'template-positions',
                                'type' => 'code-group',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'Top',
                                        'data' => 'top',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Main',
                                        'data' => 'main',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Left',
                                        'data' => 'left',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Center',
                                        'data' => 'center',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Right',
                                        'data' => 'right',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Bottom',
                                        'data' => 'bottom',
                                        'type' => 'code'
                                    ],

                                ]
                            ],

                            // Text types
                            $identicalData + [
                                'name' => 'Text types',
                                'data' => 'text-types',
                                'type' => 'code-group',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'Page',
                                        'data' => 'page',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'News',
                                        'data' => 'news',
                                        'type' => 'code'
                                    ]

                                ]
                            ],

                            // Translations
                            $identicalData + [
                                'name' => 'Translations',
                                'data' => 'translations',
                                'type' => 'code-group',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'English',
                                        'data' => 'en',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Slovensko',
                                        'data' => 'sl',
                                        'type' => 'code'
                                    ]

                                ]
                            ],

                            // Element types
                            $identicalData + [
                                'name' => 'Element types',
                                'data' => 'element-types',
                                'type' => 'code-group',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'Default',
                                        'data' => 'default',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Group',
                                        'data' => 'group',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Link',
                                        'data' => 'link',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Hidden link',
                                        'data' => 'hidden-link',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Code group',
                                        'data' => 'code-group',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Code',
                                        'data' => 'code',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Application',
                                        'data' => 'app',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'Web application',
                                        'data' => 'web-app',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'URL',
                                        'data' => 'url',
                                        'type' => 'code'
                                    ],

                                    $identicalData + [
                                        'name' => 'News',
                                        'data' => 'news',
                                        'type' => 'code'
                                    ]

                                ]
                            ],

                            // User types
                            $identicalData + [
                                'name' => 'User types',
                                'data' => 'user-types',
                                'type' => 'code-group',
                                'children' => [

                                    $identicalData + [
                                        'name' => 'Default',
                                        'data' => 'default',
                                        'type' => 'code'
                                    ]

                                ]
                            ]

                        ]
                    ]
                ]
            ]
        ];
        
        Element::buildTree($elements);
    }

}