<?php namespace Earth\Core\Services\Data;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Data\Data;
use Illuminate\Support\Str;

class DataService {

    /**
     * @param $input
     * @return bool
     */
    public function store($input)
    {
        if ( isset($input['key_prefix']) ) {
            $input['key'] = $input['key_prefix'] . $input['key'];
        }

        $item = Data::create([
            'customable_id' => $input['customable_id'],
            'customable_type' => $input['customable_type'],
            'customable_key' => $input['key'],
            'customable_value' => $input['value'],
        ]);

        if ($item) {
            new ObjectInsertedAction($item);
            Activity::log("Custom data with key [".$input['key']."] was inserted to the object [".$input['customable_type']."] with ID [".$input['customable_id']."].", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $prefix
     * @param $input
     * @return bool
     */
    public function updatePartially($id, $prefix, $input)
    {
        $data = Data::findOrFail($id);

        if (isset($prefix) and $input['name'] == "customable_key") {
            $input["value"] = $prefix.$input["value"];
        }
        
        $data->{$input['name']} = $input['value'];

        if ($data->save()) {
            new ObjectUpdatedAction($data);
            Activity::log("Custom data with key [".$data->customable_key."] was updated on the object [".$data->customable_type."] with ID [".$data->customable_id."].", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete($id)
    {
        if (Str::contains($id, ",")) {
            $ids = explode(",", $id);
            $items = Data::findMany($ids);

            foreach($items as $item) {
                if ($item->delete($item->id)) {
                    Activity::log("Custom data with key [".$item->customable_key."] was deleted on the object [".$item->customable_type."] with ID [".$item->customable_id."].", session("aliasUser")->id);
                    new ObjectDeletedAction($item);
                }
            }
        } else {
            $data = Data::findOrFail($id);
            if ($data->delete($id)) {
                Activity::log("Custom data with key [".$data->customable_key."] was deleted on the object [".$data->customable_type."] with ID [".$data->customable_id."].", session("aliasUser")->id);
                new ObjectDeletedAction($data);
                return true;
            }

            return false;
        }
    }

}