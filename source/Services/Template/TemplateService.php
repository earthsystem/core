<?php namespace Earth\Core\Services\Template;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Actions\ObjectUpdatedAction;
use Earth\Core\Entities\Element\Template as PivotTemplate;
use Earth\Core\Entities\Template\Template;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class TemplateService {

    /**
     * @param $id
     * @param $position
     * @param $input
     * @return static
     */
    public function store($id, $position, $input)
    {
        $input['path'] = str_replace("/", ".", $input['path']);

        // Create the base template
        $item = Template::create([
            'name' => $input['name'],
            'path' => $input['path']
        ]);

        if ($item) {
            new ObjectInsertedAction($item);
            Activity::log("Template with name '".$item->name."' and path '".$item->path."' was created.", session("aliasUser")->id);
            $this->attachToElement($id, $position, $item->id, $input);
        }

        return $item;
    }

    /**
     * @param $id
     * @param $position
     * @param $templateId
     * @param $input
     */
    public function attachToElement($id, $position, $templateId, $input)
    {
        $element = element()->findOrFail($id);
        $template = Template::findOrFail($templateId);
        $newPosition = $element->templates()->whereType($position)->orderBy('position', 'desc')->value('position');
        if (!isset($newPosition)) $newPosition = 0;

        $template->elements()->attach($id, [
            'position' => $newPosition + 1,
            'type' => $position,
            'active' => isset($input['active']),
            'recursive' => isset($input['recursive'])
        ]);
        Activity::log("Template with ID [".$templateId."] was attached to the element with name '".$element->name." [".$element->id."]'.", session("aliasUser")->id);

    }

    /**
     * @param $id
     * @param $templateId
     * @param $input
     * @return bool
     */
    public function update($id, $templateId, $input)
    {
        $input['path'] = str_replace("/", ".", $input['path']);

        $template = Template::findOrFail($templateId);
        $template->name = $input['name'];
        $template->path = $input['path'];

        if ($template->save()) {
            Activity::log("Template with ID [".$template->id."] was updated.", session("aliasUser")->id);
        }
    }

    /**
     * @param $id
     * @param $templateId
     * @param $input
     * @return bool
     */
    public function updatePartially($id, $templateId, $input)
    {
        $template = Template::findOrFail($templateId);
        $template->{$input['name']} = $input['value'];

        if ($template->save()) {
            new ObjectUpdatedAction($template);
            Activity::log("Template with ID [".$template->id."] was updated.", session("aliasUser")->id);
        }

        return false;
    }

    /**
     * @param $id
     * @param $pivotId
     * @param $input
     * @return mixed
     */
    public function updatePivot($id, $pivotId, $input)
    {
        if ($input['value'] == "true") $input['value'] = 1;
        
        $templatePivot = PivotTemplate::findOrFail($pivotId);
        $templatePivot->{$input['name']} = $input['value'];
        return $templatePivot->save();
    }

    /**
     * @param $id
     * @param $pivotId
     * @param $input
     * @return bool
     */
    public function sort($id, $pivotId, $input)
    {
        $item = PivotTemplate::findOrFail($pivotId);
        $currentPosition = $item->position;
        $item->position = -1;
        $item->save();

        try {

            if ($input['direction'] == "left") {
                PivotTemplate::whereElementId($item->element_id)
                    ->whereType($item->type)
                    ->where('position', '>=', $input['newPosition'])
                    ->where('position', '<', $currentPosition)
                    ->increment('position');
            } else {
                PivotTemplate::whereElementId($item->element_id)
                    ->whereType($item->type)
                    ->where('position', '<=', $input['newPosition'])
                    ->where('position', '>', $currentPosition)
                    ->decrement('position');
            }

            $item->position = $input['newPosition'];
            return $item->save();

        } catch (Exception $e) {
            // Update the current item back to original position
            $item->position = $currentPosition;
            $item->save();
        }
    }

    /**
     * @param $id
     * @param $templateId
     * @return bool|null
     * @throws Exception
     */
    public function delete($id, $templateId)
    {
        $item = Template::findOrFail($templateId);

        foreach($item->elements as $element) {
            $this->detachFromElement($element->id, $element->pivot->id, false);
        }

        return $item->delete($templateId);
    }

    /**
     * @param $id
     * @param $pivotId
     * @param $deleteBaseTemplate
     * @return bool
     */
    public function detachFromElement($id, $pivotId, $deleteBaseTemplate)
    {
        if (Str::contains($pivotId, ",")) {
            $ids = explode(",", $pivotId);
            $items = PivotTemplate::findMany($ids);

            foreach($items as $item) {
                PivotTemplate::whereElementId($item->element_id)->whereType($item->type)->where('position', '>', $item->position)->decrement('position');

                if ($deleteBaseTemplate) {
                    $this->delete($id, $item->template_id);
                }

                $item->delete($item->id);
                new ObjectDeletedAction($item);
                $element = element()->find($item->element_id);
                Activity::log("Template with ID [".$item->id."] was detached from the element with name '".$element->name." [".$element->id."]'.", session("aliasUser")->id);
            }
        } else {
            $item = PivotTemplate::findOrFail($pivotId);

            PivotTemplate::whereElementId($item->element_id)->whereType($item->type)->where('position', '>', $item->position)->decrement('position');

            if ($deleteBaseTemplate) {
                $this->delete($id, $item->template_id);
            }

            $item->delete($item->id);
            new ObjectDeletedAction($item);
            $element = element()->find($item->element_id);
            Activity::log("Template with ID [".$item->id."] was detached from the element with name '".$element->name." [".$element->id."]'.", session("aliasUser")->id);
            return true;
        }
    }

}