<?php namespace Earth\Core\Services\Template;

class ConnectorService {

    public function getFiles()
    {
        $root = base_path();
        $postDir = rawurldecode($root.(isset($_POST['dir']) ? $_POST['dir'] : null ));
        $checkbox = null;
        $onlyFolders = false;
        $onlyFiles = false;

        if ( file_exists($postDir) ) {

            $files = scandir($postDir);
            $returnDir = substr($postDir, strlen($root));

            natcasesort($files);

            if ( count($files) > 2 ) {

                echo "<ul class='jqueryFileTree'>";

                foreach( $files as $file ) {
                    $htmlRel	= htmlentities($returnDir . $file);
                    $htmlName	= htmlentities($file);
                    $ext		= preg_replace('/^.*\./', '', $file);

                    if( file_exists($postDir . $file) && $file != '.' && $file != '..' ) {
                        if( is_dir($postDir . $file) && (!$onlyFiles || $onlyFolders) )
                            echo "<li class='directory collapsed'>{$checkbox}<a href='#' rel='" .$htmlRel. "/'>" . $htmlName . "</a></li>";
                        else if (!$onlyFolders || $onlyFiles)
                            echo "<li class='file ext_{$ext}'>{$checkbox}<a href='#' rel='" . $htmlRel . "'>" . $htmlName . "</a></li>";
                    }
                }

                echo "</ul>";

            }
        }
    }

}