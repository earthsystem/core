<?php namespace Earth\Core\Services\User;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;
use Earth\Core\Exceptions\PasswordNotMatchingException;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Sentinel;

class UserService {

    /**
     * @param $input
     * @return bool|\Cartalyst\Sentinel\Users\UserInteface
     */
    public function store($input)
    {
        $credentials = [
            'email' => $input['email'],
            'type' => $input['type'],
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'password' => $input['password'],
        ];

        if ( !empty($input['username']) ) {
            $credentials['username'] = $input['username'];
        }

        $user = User::whereEmail($credentials['email'])->withTrashed()->first();

        if (!$user) {
            $user = Sentinel::registerAndActivate($credentials);
            $user->type = $input['type'];
            if ($user->save()) {
                Activity::log("User with name '". $input['first_name']. " " . $input['last_name'] ."' was created.", session("aliasUser")->id);
            }
        } else {
            Sentinel::update($user, $credentials);
            if ($user->restore()) {
                Activity::log("User [".$user->id."] with name '". $input['first_name']. " " . $input['last_name'] ."' was restored.", session("aliasUser")->id);
            }
        }

        return $user;
    }

    /**
     * @param $id
     * @param $input
     * @return mixed
     * @throws Exception
     */
    public function update($id, $input)
    {
        $user = User::findOrFail($id);

        if (!$user->can_modify) {
            if (config('earth.system_login_key') == "email") {
                unset($input['email']);
            } else if (config('earth.system_login_key') == "username") {
                unset($input['username']);
            }
        }

        if ( empty($input['password']) ) {
            unset($input['password']);
        }

        $user->type = $input['type'];
        $user->save();

        if (Sentinel::update($user, $input)) {
            Activity::log("User [".$user->id."] with name '". $user->full_name ."' was updated.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws Exception
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);
        if (!$user->can_modify) throw new Exception("User is admin.");

        $user->delete();
        new ObjectDeletedAction($user);
        Activity::log("User [".$user->id."] with name '". $user->full_name ."' was deleted.", session("aliasUser")->id);
        return true;
    }

    /**
     * @param $userId
     */
    public function switchUser($userId)
    {
        $user = User::findOrFail($userId);
        session()->put('aliasUser', $user);
    }

    /**
     * @param $input
     * @return mixed
     * @throws PasswordNotMatchingException
     */
    public function changePassword($input)
    {
        $user = Sentinel::getUser();

        if ($input['new_password'] != $input['confirm_new_password']) {
            throw new PasswordNotMatchingException;
        }

        if (Sentinel::update($user, ['password' => $input['new_password']])) {
            $user = User::find($user->getUserId());
            Activity::log("Password changed for user [".$user->id."] with name '". $user->full_name ."'.", session("aliasUser")->id);
        }
    }

    /**
     * @param $roleId
     * @param $userId
     */
    public function attachRole($userId, $roleId)
    {
        $user = User::find($userId);
        if (!$user) throw new ModelNotFoundException;
        $role = Sentinel::findRoleById($roleId);
        if (!$role) throw new ModelNotFoundException;

        $user->roles()->attach($role);
        Activity::log("Role '".$role->name."' with ID [".$roleId."] was attached to the user with name '".$user->full_name."' [".$user->id."].", session("aliasUser")->id);
    }

    /**
     * @param $roleId
     * @param $userId
     * @return mixed
     */
    public function detachRole($userId, $roleId)
    {
        $user = User::find($userId);
        if (!$user) throw new ModelNotFoundException;

        if (Str::contains($roleId, ",")) {
            $ids = explode(",", $roleId);
            $user->roles()->detach($ids);
            Activity::log("Roles with IDs [".$roleId."] were detached from the user with name '".$user->full_name."' [".$user->id."].", session("aliasUser")->id);
        } else {
            $role = Role::find($roleId);
            if (!$role) throw new ModelNotFoundException;
            $user->roles()->detach($role);
            Activity::log("Role with ID [".$roleId."] was detached from the user with name '".$user->full_name."' [".$user->id."].", session("aliasUser")->id);

        }
    }

}