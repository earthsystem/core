<?php namespace Earth\Core\Services\Role;

use Activity;
use Earth\Core\Actions\ObjectDeletedAction;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;
use Illuminate\Contracts\Validation\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Sentinel;

class RoleService {

    /**
     * @param $input
     * @return mixed
     */
    public function store($input)
    {
        $credentials = [
            'name' => $input['name'],
            'slug' => Str::slug($input['name'])
        ];

        $role = Role::whereSlug($credentials['slug'])->withTrashed()->first();

        if (!$role) {
            $role = Sentinel::getRoleRepository()->createModel()->create($credentials);
            if ($role->save()) {
                Activity::log("Role with name '".$input['name']."' was created.", session("aliasUser")->id);
            }
        } else {
            $role->update($credentials);
            if ($role->restore()) {
                Activity::log("Role with name '".$input['name']."' [".$role->id."] was restored.", session("aliasUser")->id);
            }
        }

        return $role;
    }

    /**
     * @param $id
     * @param $input
     * @return bool
     */
    public function update($id, $input)
    {
        $role = Role::findOrFail($id);
        if (!$role->can_modify) throw new UnauthorizedException;
        $role->name = $input['name'];
        $role->slug = Str::slug($input['name']);

        if ($role->save()) {
            Activity::log("Role with name '".$role->name."' [".$role->id."] was deleted.", session("aliasUser")->id);
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws Exception
     */
    public function delete($id)
    {
        $role = Role::findOrFail($id);
        if (!$role->can_modify) throw new Exception("Role is admin.");

        $role->delete();
        Activity::log("Role '".$role->name."' [".$role->id."] was deleted.", session("aliasUser")->id);
        new ObjectDeletedAction($role);
        return true;
    }

    /**
     * @param $roleId
     * @param $userId
     */
    public function attachUser($roleId, $userId)
    {
        $user = User::find($userId);
        if (!$user) throw new ModelNotFoundException;
        $role = Sentinel::findRoleById($roleId);
        if (!$role) throw new ModelNotFoundException;

        $role->users()->attach($user);
        Activity::log("User '".$user->full_name."' [".$user->id."] was attached to the role with name '".$role->name."' [".$role->id."].", session("aliasUser")->id);
    }

    /**
     * @param $roleId
     * @param $userId
     * @return mixed
     */
    public function detachUser($roleId, $userId)
    {
        $role = Role::find($roleId);
        if (!$role) throw new ModelNotFoundException;

        if (Str::contains($userId, ",")) {
            $ids = explode(",", $userId);
            $role->users()->detach($ids);
            Activity::log("Users with IDs [".$userId."] were detached from the role with name '".$role->name."' [".$role->id."].", session("aliasUser")->id);
        } else {
            $user = User::find($userId);
            if (!$user) throw new ModelNotFoundException;
            $role->users()->detach($user);
            Activity::log("User with ID [".$userId."] was detached from the role with name '".$role->name."' [".$role->id."].", session("aliasUser")->id);
        }
    }

}