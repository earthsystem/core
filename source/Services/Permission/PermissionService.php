<?php namespace Earth\Core\Services\Permission;

use Activity;
use Earth\Core\Actions\ObjectInsertedAction;
use Earth\Core\Entities\Permission\Permission;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;

class PermissionService {

    /**
     * @param $input
     * @return bool
     */
    public function store($input)
    {
        Permission::whereObjectId($input['object_id'])
            ->whereObjectType($input['object_type'])
            ->whereType($input['field'])->delete();

        if (isset($input['permissionsRoles'])) {
            foreach($input['permissionsRoles'] as $item) {
                $access = explode(";", $item);
                $permission = Permission::create([
                    'object_id' => $input['object_id'],
                    'object_type' => $input['object_type'],
                    'type' => $input['field'],
                    'access_id' => $access[1],
                    'access_type' => $access[0]
                ]);

                if ($permission) {
                    new ObjectInsertedAction($permission);
                    $role = Role::find($access[1]);
                    Activity::log("Permission [".$input['field']."] was added for role '".$role->name."' [".$role->id."] to object [".$input['object_id']."] with ID [".$input['object_type']."]", session("aliasUser")->id);
                }
            }
        }

        if (isset($input['permissionsUsers'])) {
            foreach($input['permissionsUsers'] as $item) {
                $access = explode(";", $item);
                $permission = Permission::create([
                    'object_id' => $input['object_id'],
                    'object_type' => $input['object_type'],
                    'type' => $input['field'],
                    'access_id' => $access[1],
                    'access_type' => $access[0]
                ]);

                if ($permission) {
                    $user = User::find($access[1]);
                    Activity::log("Permission [".$input['field']."] was added for user '".$user->full_name."' [".$user->id."] to object [".$input['object_type']."] with ID [".$input['object_id']."]", session("aliasUser")->id);
                    new ObjectInsertedAction($permission);
                }
            }
        }

        return true;
    }

}