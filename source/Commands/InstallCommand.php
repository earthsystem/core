<?php namespace Earth\Core\Commands;

use Earth\Core\Exceptions\ApplicationInstalledException;
use Earth\Core\Services\InstallService;
use Exception;
use Illuminate\Console\Command;
use PDOException;
use RuntimeException;

class InstallCommand extends Command {

    /**
     * @var string
     */
    protected $name = 'earth:install';

    /**
     * @var string
     */
    protected $description = 'Install the Earth App.';

    /**
     * Fire the command
     * @param InstallService $installService
     */
    public function fire(InstallService $installService)
    {
        try {

            $this->line("--------------------");
            $this->line("Installing Earth.");
            $this->line("--------------------");

            $bar = $this->output->createProgressBar(5);

            $bar->advance();
            $this->line(" Publishing vendors.");
            $this->callSilent('vendor:publish');
            $this->line("--------------------");

            $bar->advance();
            $this->line(" Moving migration files.");
            $installService->moveMigrationFiles();
            $this->line("--------------------");

            $bar->advance();
            $this->line(" Migrating to database.");
            $this->callSilent('migrate');
            $this->line("--------------------");

            $bar->advance();
            $this->line(" Seeding users and roles.");
            $installService->seedUsersAndRoles();
            $this->line("--------------------");

            $bar->advance();
            $this->line(" Seeding application structure.");
            $installService->seedApplicationStructure();
            $this->line("--------------------");

        } catch (ApplicationInstalledException $e) {
            $this->error("Application is already installed.");
        } catch (PDOException $e) {
            $this->error("Could not connect to the database.");
        } catch (RuntimeException $e) {
            $this->error("Could not publish the migrations.");
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}