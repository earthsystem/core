<?php namespace Earth\Core\Commands;

use Earth\Core\ServiceProviders\CoreServiceProvider;
use Illuminate\Console\Command;

class PublishAssetsCommand extends Command {

    /**
     * @var string
     */
    protected $name = 'earth:assets';

    /**
     * @var string
     */
    protected $description = 'Publish the Earth App assets.';

    /**
     * Fire the command
     */
    public function fire()
    {
        $this->callSilent('vendor:publish', ['--force', '--provider' => CoreServiceProvider::class, '--tag' => ['earth-assets']]);
        $this->info("Published assets for Earth.");
    }

    /**
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
    
}