<?php namespace Earth\Core\Entities;

class ModalTab {

    /**
     * @var
     */
    public $url;

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $icon;

    /**
     * @var
     */
    public $refreshUri;

    /**
     * @var null
     */
    public $refreshType;

    /**
     * @var null
     */
    public $modalSize;

    /**
     * ModalTab constructor.
     * @param $id
     * @param $url
     * @param $icon
     * @param $refreshUri
     * @param null $refreshType
     * @param null $name
     */
    public function __construct($id, $url, $icon, $refreshUri, $refreshType = null, $name = null, $modalSize = null)
    {
        $this->id = $id;
        $this->icon = $icon;
        $this->url = $url;
        $this->refreshUri = $refreshUri;

        if (isset($refreshType)) {
            $this->refreshType = $refreshType;
        }

        if (isset($name)) {
            $this->name = $name;
        } else {
            $this->name = earthLang('base.' . $id);
        }

        if (isset($modalSize)) {
            $this->modalSize = $modalSize;
        }
    }

    /**
     * @param null $tab
     * @return bool
     */
    public function isActive($tab = null)
    {
        if (isset($tab) and $tab == $this->id) return true;

        return false;
    }

}