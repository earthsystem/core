<?php namespace Earth\Core\Entities\Role;

use Cartalyst\Sentinel\Roles\EloquentRole;
use Earth\Core\Entities\User\User;
use Earth\Core\Traits\SearchTrait;
use Earth\Core\Traits\UserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends EloquentRole {

    use SearchTrait, UserTrait, SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'name' => 'required'
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var
     */
    protected static $usersModel = User::class;

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeDoesNotHaveUser($query, $id)
    {
        return $query->whereNotIn( 'id', function($query) use($id) {
            return $query->select('role_id')->from('role_users')->whereUserId($id);
        });
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeSort($query, $value = null)
    {
        if (isset($value)) {
            if ($value == "name") {
                return $query->orderBy('name');
            } else {
                $sort = explode('-', request()->get('sort'));
                return $query->orderBy($sort[0], isset($sort[1]) ? $sort[1] : 'asc');
            }
        } else {
            return $query->latest();
        }
    }

    /**
     * @param $query
     * @param $id
     * @param $field
     * @param $objectType
     * @return mixed
     */
    public function scopeDoesNotHavePermission($query, $id, $field, $objectType)
    {
        return $query->whereNotIn('id', function($query) use($id, $field, $objectType) {
            return $query->select('access_id')
                ->from('permissions')
                ->whereObjectId($id)
                ->whereObjectType($objectType)
                ->whereType($field)
                ->whereAccessType(self::class);
        });
    }

    /**
     * @param $query
     * @param $id
     * @param $field
     * @param $objectType
     * @return mixed
     */
    public function scopeHasPermission($query, $id, $field, $objectType)
    {
        return $query->whereIn('id', function($query) use($id, $field, $objectType) {
            return $query->select('access_id')
                ->from('permissions')
                ->whereObjectId($id)
                ->whereObjectType($objectType)
                ->whereType($field)
                ->whereAccessType(self::class);
        });
    }

}