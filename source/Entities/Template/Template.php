<?php namespace Earth\Core\Entities\Template;

use Earth\Core\Entities\Element\Element;
use Earth\Core\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model {

    use SearchTrait, SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'name' => 'required',
        'path' => 'required',
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return $this
     */
    public function elements()
    {
        return $this->belongsToMany(Element::class)->withPivot('id', 'position', 'type', 'active', 'recursive');
    }

    /**
     * @param $query
     * @param $id
     * @param $position
     * @return mixed
     */
    public function scopeNotInElementAndPosition($query, $id, $position)
    {
        return $query->whereNotIn('id', function($query) use($id, $position){
            return $query->select('template_id')->from('element_template')->whereElementId($id)->whereType($position);
        });
    }

}