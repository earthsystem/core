<?php namespace Earth\Core\Entities\Template;

use Illuminate\Database\Eloquent\Collection;

class Set {

    /**
     * Set constructor.
     */
    public function __construct()
    {
        $templateTypes = get_codes('template-positions')->pluck('data');

        foreach($templateTypes as $type) {
            $this->{$type} = new Collection([]);
        }
    }

}