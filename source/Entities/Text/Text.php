<?php namespace Earth\Core\Entities\Text;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use DB;
use Earth\Core\Entities\Attachment\Attachment;
use Earth\Core\Entities\Data\Data;
use Earth\Core\Entities\Element\Element;
use Earth\Core\Traits\SearchTrait;
use Earth\Core\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class Text extends Model implements StaplerableInterface {

    use EloquentTrait, SoftDeletes, SearchTrait, UserTrait;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'type' => 'required',
        'title' => 'required',
    ];

    /**
     * @var array
     */
    public static $singleFormRules = [
        'type' => 'required',
        'name' => 'required',
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Text constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        foreach ( config('earth.texts_image_sizes') as $field => $staple ) {
            $this->hasAttachedFile($field, $staple);
        }

        parent::__construct($attributes);
    }

    /**
     * Boot the model
     */
    public static function boot()
    {
        parent::boot();
        static::bootStapler();

        // Now, before the record is saved, set the filename attribute on the model:
        static::saving(function($model)
        {
            $pathInfo = pathinfo($model->image->originalFileName());
            if (isset($pathInfo['filename']) and isset($pathInfo['extension'])) {
                $newFilename = Str::slug($pathInfo['filename']) . '.' . $pathInfo['extension'];
                $model->image->instanceWrite('file_name', $newFilename);
            }
        });
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locales()
    {
        return $this->hasMany(Locale::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function locale()
    {
        return $this->hasOne(Locale::class);
    }

    /**
     * @return $this
     */
    public function elements()
    {
        return $this->belongsToMany(Element::class)->withPivot('id', 'position');
    }

    /**
     * @return mixed
     */
    public function custom_data()
    {
        return $this->hasMany(Data::class, 'customable_id')->whereCustomableType(self::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    /**
     * @return mixed
     */
    public function gallery()
    {
        return $this->attachments()->with('media')->whereType('gallery');
    }

    /**
     * @return mixed
     */
    public function files()
    {
        return $this->attachments()->with('media')->whereType('files');
    }

    /**
     * @return bool
     */
    public function hasImage()
    {
        if (!isset($this->image_file_name)) {
            return false;
        }

        return true;
    }

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeNotInElement($query, $id)
    {
        return $query->whereNotIn('id', function($query) use($id){
            return $query->select('text_id')->from('element_text')->whereElementId($id);
        });
    }

    /**
     * @param $query
     * @param null $id
     * @return mixed
     */
    public function scopeIsInElement($query, $id)
    {
        return $query->whereHas('elements', function($query) use ($id){
            return $query->where('element_id', $id);
        });
    }

    /**
     * @param $query
     * @param $orderByField
     * @param string $orderByDirection
     * @param null $id
     * @return mixed
     */
    public function scopeOrderByRelationshipAttribute($query, $orderByField, $orderByDirection = 'asc', $id = null)
    {
        if ( !isset($id) ) {
            $id = config('category')->id;
        }

        $prefix = config()->get('database.connections.mysql.prefix');
        if ( !empty($prefix) ) $prefix .= "_";

        return $query->orderByRaw('(select '.$orderByField.' from '.$prefix.'element_text where element_id = '.$id.' and text_id = '.$prefix.'texts.id) '.$orderByDirection);
    }

    /**
     * @param $query
     * @param null $value
     * @return mixed
     */
    public function scopeSort($query, $value = null)
    {
        if (isset($value)) {
            if ($value == "name") {
                return $query->orderBy('name');
            } else {
                $sort = explode('-', request()->get('sort'));
                return $query->orderBy($sort[0], isset($sort[1]) ? $sort[1] : 'asc');
            }
        } else {
            return $query->latest();
        }
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeHasLocale($query, $locale = null)
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        if (is_array($locale)) {
            return $query->whereHas('locales', function($query) use ($locale){
                return $query->whereIn('locale', $locale);
            });
        }

        return $query->whereHas('locales', function($query) use ($locale){
            return $query->where(function($query) use($locale) {
                $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        });
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeWithLocale($query, $locale = null)
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        if (is_array($locale)) {
            return $query->hasLocale($locale)->with(['locale' => function($query) use ($locale){
                return $query->whereIn('locale', $locale);
            }]);
        }

        return $query->hasLocale($locale)->with(['locale' => function($query) use ($locale){
            return $query->where(function($query) use($locale) {
                $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        }]);
    }

    /**
     * @param $query
     * @param $slug
     * @param null $locale
     * @return mixed
     */
    public function scopeHasSlug($query, $slug, $locale = null)
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        return $query->whereIn('id', function($query) use ($locale, $slug){
            return $query->select('text_id')->from('text_locale')->whereSlug($slug)->where(function($query) use ($locale){
                return $query->where(function($query) use($locale) {
                    $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
                });
            });
        });
    }

    /**
     * @param $query
     * @param null $date
     * @param null $locale
     * @return mixed
     */
    public function scopeIsPublished($query, $date = null, $locale = null)
    {
        if (!isset($date)) {
            $date = date('Y-m-d');
        }

        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        // (publish_from is null and publish_to is null) or
        // (publish_from is not null and date(publish_from) >= 'datum') or
        // (publish_to is not null and date(publish_to)  <= 'datum')

        return $query->whereIn('id', function($query) use($locale, $date) {
            return $query->select('text_id')->from('text_locale')->where('locale', $locale)->where(function($query) use($date){
                return $query->where(function($query) use ($date){
                    return $query->whereNull('publish_from')->whereNull('publish_to');
                })->orWhere(function($query) use($date){
                    return $query->whereNotNull('publish_from')->whereNull('publish_to')->where(DB::raw('DATE(publish_from)'), '<=', $date);
                })->orWhere(function($query) use($date){
                    return $query->whereNotNull('publish_to')->whereNull('publish_from')->where(DB::raw('DATE(publish_to)'), '>=', $date);
                })->orWhere(function($query) use($date){
                    return $query->whereNotNull('publish_from')->whereNotNull('publish_to')->where(DB::raw('DATE(publish_from)'), '<=', $date)->where(DB::raw('DATE(publish_to)'), '>=', $date);
                });
            });
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDistinctYears($query)
    {
        return $query->groupBy(DB::raw('YEAR(created_at)'));
    }

    /**
     * @param $query
     * @param $year
     * @return mixed
     */
    public function scopeDistinctMonths($query, $year)
    {
        return $query->where(DB::raw('YEAR(created_at)'), $year->year)->groupBy(DB::raw('MONTH(created_at)'));
    }

    /**
     * @param $query
     * @param $year
     * @return mixed
     */
    public function scopeIsInYear($query, $year)
    {
        return $query->where(DB::raw('YEAR(created_at)'), $year);
    }

    /**
     * @param $query
     * @param $month
     * @return mixed
     */
    public function scopeIsInMonth($query, $month)
    {
        return $query->where(DB::raw('MONTH(created_at)'), $month);
    }

    /**
     * @param $index
     * @return string
     */
    public function getUri($index)
    {
        $slugAtIndex = slug($index);
        return "/".app()->getLocale()."/".$slugAtIndex."/".$this->slug."/".$this->id;
    }

    /**
     * @return mixed
     */
    public function getYearAttribute()
    {
        return $this->created_at->format('Y');
    }

    /**
     * @return mixed
     */
    public function getMonthAttribute()
    {
        return $this->created_at->format('m');
    }

    /**
     * @return mixed|string
     */
    public function getTitleAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->title;
    }

    /**
     * @return mixed|string
     */
    public function getSubtitleAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->subtitle;
    }

    /**
     * @return mixed|string
     */
    public function getSummaryAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->summary;
    }

    /**
     * @return mixed|string
     */
    public function getSlugAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->slug;
    }

    /**
     * @return mixed|string
     */
    public function getContentAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->data;
    }

    /**
     * @return mixed|string
     */
    public function getParsedContentAttribute()
    {
        // Find all matches for {category:something}
        $re1='(\\{)';	# Any Single Character 1
        $re2='(category)';	# Word 1
        $re3='(:)';	# Any Single Character 2
        $re4='(\\d+)';	# Integer Number 1
        $re5='(\\})';	# Any Single Character 3

        $parsedData = $this->locale->data;

        if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5."/is", $parsedData, $matches))
        {
            $fullTextMatches = $matches[0];
            $idMatches = $matches[4];
            $matches = [];
            foreach($fullTextMatches as $index => $ftMatch) {
                $matches[$idMatches[$index]] = $ftMatch;
            }

            $elements = element()->whereIn('id', $idMatches)->withLocale()->with('breadcrumb')->get();

            foreach($matches as $id => $match) {
                $thisElement = $elements->whereIn('id', [$id])->first();
                if (isset($thisElement->breadcrumb)) {
                    $parsedData = str_replace(
                        $match,
                        "/" . app()->getLocale() . $thisElement->breadcrumb->uri,
                        $parsedData
                    );
                } else {
                    $parsedData = str_replace($match, "", $parsedData);
                }
            }
            
            return $parsedData;
        }
        
        if ( !isset($this->locale) ) return "";
        return $this->locale->data;
    }

    /**
     * @param int $length
     * @return mixed|string
     */
    public function getParsedSummary($length = 256)
    {
        if ( !isset($this->locale) ) return "";

        if ( isset($this->locale->summary) and !empty($this->locale->summary) ) {
            return Str::limit($this->locale->summary, $length);
        }

        if ( isset($this->locale->data) and !empty($this->locale->data) ) {
            return strip_tags(Str::limit($this->locale->summary, $length));
        }

        return $this->locale->title;
    }


}