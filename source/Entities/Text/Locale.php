<?php namespace Earth\Core\Entities\Text;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Earth\Core\Entities\Attachment\Attachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class Locale extends Model implements SluggableInterface {

    use SluggableTrait, SoftDeletes;

    /**
     * @var array
     */
    public static $formRules = [
        'title' => 'required'
    ];

    /**
     * @var array
     */
    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug',
        'unique' => true
    ];

    /**
     * @var array
     */
    public $dates = [
        'publish_from',
        'publish_to',
        'deleted_at'
    ];

    /**
     * @var string
     */
    public $table = 'text_locale';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    /**
     * @return mixed
     */
    public function gallery()
    {
        return $this->attachments()->with('media')->whereType('gallery');
    }

    /**
     * @return mixed
     */
    public function files()
    {
        return $this->attachments()->with('media')->whereType('files');
    }

}