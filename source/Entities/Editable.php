<?php namespace Earth\Core\Entities;

class Editable {

    /**
     * @var
     */
    private $method;

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $class;

    /**
     * @var
     */
    private $name;

    /**
     * @var
     */
    private $primaryKey;

    /**
     * @var
     */
    private $uri;

    /**
     * @var
     */
    private $title;

    /**
     * @var
     */
    private $source;

    /**
     * @var
     */
    private $value;

    /**
     * @return string
     */
    public function render()
    {
        if (!canWrite()) {
            return "";
        }

        $html = 'class="'.$this->class.'" ';
        $html .= 'data-name="'.$this->name.'" ';
        $html .= 'data-pk="'.$this->primaryKey.'" ';
        $html .= 'data-url="'.$this->uri.'" ';
        $html .= 'data-title="'.$this->title.'" ';

        if ($this->type == "select") {
            $html .= "data-source='".$this->source."'";
            $html .= 'data-value="'.$this->value.'"';
        }

        return $html;
    }

    /**
     * @param $method
     * @return $this
     */
    public function text($method = "post")
    {
        $this->type = "text";
        $this->method = $method;
        $this->class = "editable-".$this->method;
        return $this;
    }

    /**
     * @return $this
     */
    public function select()
    {
        $this->type = "select";
        $this->method = "put";
        $this->class = "editable-select";
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function name($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function primaryKey($key)
    {
        $this->primaryKey = $key;
        return $this;
    }

    /**
     * @param $uri
     * @return $this
     */
    public function uri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function value($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param $source
     * @return $this
     */
    public function source($source)
    {
        $this->source = $source;
        return $this;
    }

}