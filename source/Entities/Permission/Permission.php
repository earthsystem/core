<?php namespace Earth\Core\Entities\Permission;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

    /**
     * @var array
     */
    protected $guarded = ['id'];

}