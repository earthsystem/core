<?php namespace Earth\Core\Entities\Activity;

use Earth\Core\Entities\User\User;

class Activity extends \Spatie\Activitylog\Models\Activity {

    /**
     * Get the user that the activity belongs to.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}