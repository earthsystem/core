<?php namespace Earth\Core\Entities\User;

use Cartalyst\Sentinel\Users\EloquentUser;
use Earth\Core\Entities\Element\Collection;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Traits\SearchTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends EloquentUser {

    use SearchTrait, SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'type' => 'required',
    ];

    /**
     * @var array
     */
    protected $loginNames = [ 'email', 'username' ];

    /**
     * @var
     */
    protected static $rolesModel = Role::class;

    /**
     * @return Collection
     */
    public function getApplications()
    {
        $applications = new Collection( element()->whereType('app')->get() );
        return $applications->showOnlyThoseUserCanAccess();
    }

    /**
     * @return mixed
     */
    public function getDefaultApplication()
    {
        return $this->getApplications()->first();
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeDoesNotHaveRole($query, $id)
    {
        return $query->whereNotIn('id', function($query) use($id) {
            return $query->select('user_id')->from('role_users')->whereRoleId($id);
        });
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeSort($query, $value = null)
    {
        if (isset($value)) {
            if ($value == "name") {
                return $query->orderBy('last_name')->orderBy('first_name');
            } else {
                $sort = explode('-', request()->get('sort'));
                return $query->orderBy($sort[0], isset($sort[1]) ? $sort[1] : 'asc');
            }
        } else {
            return $query->latest();
        }
    }

    /**
     * @param $query
     * @param $id
     * @param $field
     * @param $objectType
     * @return mixed
     */
    public function scopeDoesNotHavePermission($query, $id, $field, $objectType)
    {
        return $query->whereNotIn('id', function($query) use($id, $field, $objectType) {
           return $query->select('access_id')
               ->from('permissions')
               ->whereObjectId($id)
               ->whereObjectType($objectType)
               ->whereType($field)
               ->whereAccessType(self::class);
        });
    }

    /**
     * @param $query
     * @param $id
     * @param $field
     * @param $objectType
     * @return mixed
     */
    public function scopeHasPermission($query, $id, $field, $objectType)
    {
        return $query->whereIn('id', function($query) use($id, $field, $objectType) {
            return $query->select('access_id')
                ->from('permissions')
                ->whereObjectId($id)
                ->whereObjectType($objectType)
                ->whereType($field)
                ->whereAccessType(self::class);
        });
    }

}