<?php namespace Earth\Core\Entities\Data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Data extends Model {

    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var string
     */
    public $table = 'custom_data';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function customable()
    {
        return $this->morphTo();
    }

    /**
     * @return mixed
     */
    public function getKeyAttribute()
    {
        return $this->customable_key;
    }

    /**
     * @return mixed
     */
    public function getValueAttribute()
    {
        return $this->customable_value;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->customable_value;
    }
}