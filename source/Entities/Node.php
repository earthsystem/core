<?php namespace Earth\Core\Entities;

use Earth\Core\Traits\UserTrait;

class Node extends \Baum\Node {

    use UserTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery()
    {
        $query = parent::newQuery();

        if (session()->has('t')) {
            if (method_exists($this, "withTrashed") and !config()->has("is_website")) {
                $query = $query->withTrashed();
            }
        }

        return $query;
    }

    /**
     * @param bool $excludeDeleted
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newNestedSetQuery($excludeDeleted = true)
    {
        if (session()->has('t')) {
            $excludeDeleted = false;
        }
        
        return parent::newNestedSetQuery($excludeDeleted);
    }

}