<?php namespace Earth\Core\Entities;

class Tab {

    /**
     * @var
     */
    public $url;

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $view;

    /**
     * @var
     */
    public $icon;

    /**
     * @var
     */
    public $route;

    /**
     * Tab constructor.
     * @param $id
     * @param $itemId
     * @param $route
     * @param $icon
     * @param null $view
     * @param null $name
     * @param null $url
     */
    public function __construct($id, $itemId, $route, $icon, $view = null, $name = null, $url = null)
    {
        $this->id = $id;
        $this->icon = $icon;
        $this->route = $route;

        if (isset($url)) {
            $this->url = $url;
        } else {
            $this->url = earthRouteWithVars($route.".getShow", [$itemId, $id]);
        }

        if (isset($view)) {
            $this->view = $view;
        } else {
            $this->view = earthView($route . ".tabs." . $id, true);
        }

        if (isset($name)) {
            $this->name = $name;
        } else {
            $this->name = earthLang('base.' . $id);
        }
    }

    /**
     * @param null $tab
     * @return bool
     */
    public function isActive($tab = null)
    {
        if (isset($tab) and $tab == $this->id) return true;

        return false;
    }

}