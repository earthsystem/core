<?php namespace Earth\Core\Entities\Attachment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model {

    use SoftDeletes;

    /**
     * @var string
     */
    public $table = 'attachments';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function attachable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locales()
    {
        return $this->hasMany(Locale::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function locale()
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        return $this->hasOne(Locale::class)->whereLocale($locale);
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeHasLocale($query, $locale = null)
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        return $query->whereHas('locales', function($query) use ($locale){
            return $query->where(function($query) use($locale){
                return $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            })->where('title', '!=', '');
        });
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeWithLocale($query, $locale = null)
    {
        if (!isset($locale)) {
            $locale = app()->getLocale();
        }

        return $query->hasLocale($locale)->with(['locale' => function($query) use ($locale){
            return $query->where(function($query) use($locale){
                return $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        }]);
    }

}