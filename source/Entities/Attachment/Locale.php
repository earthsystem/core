<?php namespace Earth\Core\Entities\Attachment;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model {

    /**
     * @var string
     */
    public $table = 'attachment_locale';

    /**
     * @var array
     */
    public $guarded = ['id'];

}