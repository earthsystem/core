<?php namespace Earth\Core\Entities\Attachment;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Image extends Model implements StaplerableInterface {

    use EloquentTrait;

    /**
     * @var string
     */
    public $table = 'media';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        foreach(config('earth.gallery_image_sizes') as $field => $staple) {
            $this->hasAttachedFile($field, $staple);
        }
        parent::__construct($attributes);
    }

    /**
     * Boot the model
     */
    public static function boot()
    {
        parent::boot();
        static::bootStapler();

        // Now, before the record is saved, set the filename attribute on the model:
        static::saving(function($model)
        {
            $pathInfo = pathinfo($model->file->originalFileName());
            if (isset($pathInfo['filename']) and isset($pathInfo['extension'])) {
                $newFilename = Str::slug($pathInfo['filename']) . '.' . $pathInfo['extension'];
                $model->file->instanceWrite('file_name', $newFilename);
            }
        });
    }

}