<?php namespace Earth\Core\Entities\EarthRoute;

abstract class CoreRoute {

    /**
     * @return mixed
     */
    abstract function register();

}