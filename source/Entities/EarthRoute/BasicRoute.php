<?php namespace Earth\Core\Entities\EarthRoute;

use Earth\Core\Controllers\Web\CookiesController;
use Earth\Core\Controllers\Web\CoreController;
use Earth\Core\Controllers\Web\FormController;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Router;

class BasicRoute extends CoreRoute {

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Router
     */
    protected $router;

    /**
     * BasicRoute constructor.
     */
    public function __construct()
    {
        $this->app = app();
        $this->router = app()['router'];
    }

    /**
     * Register the routes
     */
    public function register()
    {
        $this->router->get('/', ['middleware' => ['earth.web', 'web'], 'uses' => function() {
            return;
        }]);

        $this->router->get('block-cookies/{value}', ['as' => 'blockCookies', 'uses' => CookiesController::class."@block"]);

        $this->router->get('cookie-settings', [
            'middleware' => ['earth.web', 'web'],
            'as' => 'earth.cookieSettings',
            'uses' => CookiesController::class.'@getSettings'
        ]);

        $this->router->get('{locale}/{slug?}', [
            'middleware' => ['earth.web', 'web'],
            'uses' => CoreController::class.'@get'
        ])->where('slug', '.*');

        $this->router->post('sendForm', [
            'middleware' => ['earth.web', 'web'],
            'as' => 'earth.sendForm',
            'uses' => FormController::class.'@submit'
        ]);

    }

}