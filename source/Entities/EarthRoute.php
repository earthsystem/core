<?php namespace Earth\Core\Entities;

use Earth\Core\Entities\EarthRoute\BasicRoute;

class EarthRoute {

    /**
     * @var
     */
    public $domain;

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $adapter;

    /**
     * @var bool
     */
    public $protected = false;

    /**
     * @param $domain
     * @return $this
     */
    public function onDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param $adapter
     * @return $this
     */
    public function withAdapter($adapter)
    {
        $this->adapter = new $adapter;
        return $this;
    }

    /**
     * @return $this
     */
    public function protect()
    {
        $this->protected = true;
        return $this;
    }

    /**
     * Add the route to the route service
     */
    public function add()
    {
        if ( !isset($this->adapter) ) {
            $this->adapter = new BasicRoute;
        }
        
        earthRouter()->add($this);
    }

}