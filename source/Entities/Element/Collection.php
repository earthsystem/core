<?php namespace Earth\Core\Entities\Element;

class Collection extends \Baum\Extensions\Eloquent\Collection {

    /**
     * @return Collection
     */
    public function showOnlyThoseUserCanAccess()
    {
        $this->items = $this->filter( function($item) {

            $canBeAccessed = false;
            if ($item->canRead()) $canBeAccessed = true;
            if ($item->accessDenied()) $canBeAccessed = false;

            if ( $canBeAccessed and isset($item->children) ) {
                $thisCollection = new Collection($item->children);
                $item->children = $thisCollection->showOnlyThoseUserCanAccess();
            }

            return $canBeAccessed;
        });

        return new Collection($this->items);
    }

}