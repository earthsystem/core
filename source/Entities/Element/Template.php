<?php namespace Earth\Core\Entities\Element;

use Illuminate\Database\Eloquent\Model;

class Template extends Model {
    
    /**
     * @var string
     */
    public $table = 'element_template';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    public $guarded = ['id'];

}