<?php namespace Earth\Core\Entities\Element;

use Earth\Core\Entities\Data\Data;
use Earth\Core\Entities\Node;
use Earth\Core\Entities\Template\Template;
use Earth\Core\Entities\Text\Text;
use Earth\Core\Traits\PermissionsTrait;
use Earth\Core\Traits\SearchTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Element extends Node {

    use SoftDeletes, PermissionsTrait, SearchTrait;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'name' => 'required',
        'parent_id' => 'required',
        'type' => 'required'
    ];

    /**
     * @return mixed|string
     */
    public function getParentNameAttribute()
    {
        $parent = element()->find($this->parent_id);

        if (isset($parent)) {
            return $parent->name;
        }

        return "";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locales()
    {
        return $this->hasMany(Locale::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function locale()
    {
        return $this->hasOne(Locale::class);
    }

    /**
     * @return mixed
     */
    public function custom_data()
    {
        return $this->hasMany(Data::class, 'customable_id')->whereCustomableType(self::class);
    }

    /**
     * @return $this
     */
    public function templates()
    {
        return $this->belongsToMany(Template::class)->withPivot('id', 'position', 'type', 'active', 'recursive');
    }

    /**
     * @return mixed
     */
    public function breadcrumb()
    {
        return $this->hasOne(Breadcrumb::class, "element_id")->whereLocale( app()->getLocale() );
    }

    /**
     * @return $this
     */
    public function texts()
    {
        return $this->belongsToMany(Text::class)->withPivot('id', 'position');
    }

    /**
     * @return mixed
     */
    public function submenu()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->whereActive(1)->withLocale()->with('breadcrumb')->with('submenu')->orderBy('lft');
    }

    /********
     * Website specific
     *******/

    /**
     * @param $query
     * @param $slug
     * @param null $locale
     * @return mixed
     */
    public function scopeHasSlug($query, $slug, $locale = null)
    {
        if ( !isset($locale) ) $locale = app()->getLocale();

        return $query->whereIn('id', function($query) use ($locale, $slug) {
            return $query->select('element_id')->from('element_locale')->whereSlug($slug)->where( function($query) use($locale) {
                return $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        });
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeHasLocale($query, $locale = null)
    {
        if ( !isset($locale) ) $locale = app()->getLocale();

        return $query->whereHas('locales', function($query) use($locale) {
            return $query->where(function($query) use ($locale) {
                return $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        });
    }

    /**
     * @param $query
     * @param null $locale
     * @return mixed
     */
    public function scopeWithLocale($query, $locale = null)
    {
        if ( !isset($locale) ) $locale = app()->getLocale();

        return $query->hasLocale($locale)->with(['locale' => function($query) use ($locale){
            return $query->where(function($query) use ($locale){
                return $query->whereNull('locale')->orWhere('locale', '')->orWhere('locale', $locale);
            });
        }]);
    }

    /**
     * @return mixed|string
     */
    public function getTitleAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->title;
    }

    /**
     * @return mixed|string
     */
    public function getSlugAttribute()
    {
        if ( !isset($this->locale) ) return "";
        return $this->locale->slug;
    }

    /**
     * @param $index
     * @param string $comparator
     * @param string $class
     * @return string
     */
    public function isActive($index, $comparator = "slug", $class = "active")
    {
        $slug = slug($index);
        $comparatorValue = $this->{$comparator};

        if ( isset($slug) and $comparatorValue == $slug ) {
            return $class;
        }

        return "";
    }

    /**
     * @return bool
     */
    public function isUrl()
    {
        return $this->type == "url";
    }

    /**
     * @param array $slugs
     * @param null $locale
     * @return string
     */
    public function getInternalUrl($slugs = [], $locale = null)
    {
        if ( !isset($locale) ) $locale = app()->getLocale();

        if ( isset($this->breadcrumb->uri) ) {
            return "/" . $locale . $this->breadcrumb->uri;
        }

        $url = "/".$locale;
        if ( count($slugs) ) $url .= "/" . implode("/", $slugs);
        return $url."/".$this->slug;
    }

    /**
     * @param string $uriContainer
     * @return mixed
     */
    public function getExternalUri($uriContainer = "slug")
    {
        return $this->{$uriContainer};
    }

}