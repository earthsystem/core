<?php namespace Earth\Core\Entities\Element;

use Illuminate\Database\Eloquent\Model;

class Breadcrumb extends Model {

    /**
     * @var string
     */
    protected $table = 'breadcrumbs';

    /**
     * @var array
     */
    protected $guarded = ['id'];

}