<?php namespace Earth\Core\Entities\Element;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locale extends Model {

    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    public static $formRules = [
        'title' => 'required',
    ];

    /**
     * @var string
     */
    protected $table = 'element_locale';

    /**
     * @var array
     */
    protected $guarded = ['id'];

}