<?php namespace Earth\Core\Entities\Element;

use Illuminate\Database\Eloquent\Model;

class Text extends Model {

    /**
     * @var string
     */
    public $table = 'element_text';

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    public $guarded = ['id'];

}