<?php

include_once __DIR__.'/HelperPartials/base.php';
include_once __DIR__.'/HelperPartials/routes.php';
include_once __DIR__.'/HelperPartials/views.php';
include_once __DIR__.'/HelperPartials/users.php';
include_once __DIR__.'/HelperPartials/elements.php';
include_once __DIR__.'/HelperPartials/data.php';
include_once __DIR__.'/HelperPartials/texts.php';