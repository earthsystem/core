<?php namespace Earth\Core\Actions;

use Activity;

class ObjectUpdatedAction {

    /**
     * ObjectUpdatedAction constructor.
     * @param $object
     */
    public function __construct($object)
    {
        if (session()->has("aliasUser")) {
            $userId = session()->get("aliasUser")->id;
        } else {
            $userId = null;
        }

        $object->updated_by = $userId;
        $object->save();
    }

}