<?php namespace Earth\Core\Modules;

class CoreModule {

    /**
     * @var
     */
    protected $app;

    /**
     * @var
     */
    protected $router;

    /**
     * @var
     */
    protected $route;

    /**
     * @var
     */
    public $title;

    /**
     * CoreModule constructor.
     */
    public function __construct()
    {
        $this->app = app();
        $this->router = $this->app['router'];
    }

    /**
     * @return mixed
     */
    public function register() {}

    /**
     * @param $route
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

}