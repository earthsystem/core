<?php namespace Earth\Core\Modules;

use Earth\Core\Controllers\Module\Element\ElementsController;
use Earth\Core\Controllers\Module\Element\LocalesController;
use Earth\Core\Controllers\Module\Element\TemplatesController;
use Earth\Core\Controllers\Module\Element\TextsController;
use Earth\Core\Services\Element\ElementService;
use Earth\Core\Services\Element\LocaleService;
use Earth\Core\Services\Template\TemplateService;
use Earth\Core\Services\Text\TextService;

class SpecificElementStructureModule extends CoreModule {

    /**
     * @var
     */
    protected $rootElement;

    /**
     * @param $rootElement
     * @return $this
     */
    public function setRootElement($rootElement)
    {
        $this->rootElement = $rootElement;
        return $this;
    }

    /**
     * Register routes
     */
    public function register()
    {
        $this->router->group([
            'middleware' => ['web', 'earth.auth', 'earth.access'],
            'prefix' => config('earth.system_prefix'),
            'as' => config('earth.system_route_prefix').'.'
        ], function() {

            $this->router->get($this->route.'/{id}/texts/attach', ['as' => $this->route.'.texts.getAttach', 'uses' => function($id) {
                return (new TextsController)
                    ->setRoute($this->route)
                    ->callAction("getAttach", [new TextService, $id]);
            }])->where('id', '[0-9]+');

            $this->router->put($this->route.'/{id}/texts/{textId}/sort', ['as' => $this->route.'.texts.putSort', 'uses' => function($id, $textId) {
                return (new TextsController)
                    ->setRoute($this->route)
                    ->callAction("putSort", [new TextService, $id, $textId]);
            }])->where('id', '[0-9]+');

            $this->router->delete($this->route.'/{id}/texts/{textId}', ['as' => $this->route.'.texts.delete', 'uses' => function($id, $textId) {
                return (new TextsController)
                    ->setRoute($this->route)
                    ->callAction("delete", [new TextService, $id, $textId]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/templates/{position}/create', ['as' => $this->route.'.templates.getCreate', 'uses' => function($id, $position) {
                return (new TemplatesController)
                    ->callAction("getCreate", [$id, $position]);
            }])->where('id', '[0-9]+');

            $this->router->post($this->route.'/{id}/templates/connector', ['as' => $this->route.'.templates.postConnector', 'uses' => function($id) {
                return (new TemplatesController)
                    ->callAction("postConnector", [$id]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/templates/{position}/attach', ['as' => $this->route.'.templates.getAttach', 'uses' => function($id, $position) {
                return (new TemplatesController)
                    ->setRoute($this->route)
                    ->callAction("getAttach", [new TemplateService, $id, $position]);
            }])->where('id', '[0-9]+');

            $this->router->post($this->route.'/{id}/templates/{position}', ['as' => $this->route.'.templates.postStore', 'uses' => function($id, $position) {
                return (new TemplatesController)
                    ->callAction("postStore", [$id, $position]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/templates/{templateId}', ['as' => $this->route.'.templates.getEdit', 'uses' => function($id, $templateId) {
                return (new TemplatesController)
                    ->setRoute($this->route)
                    ->callAction("getEdit", [$id, $templateId]);
            }])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

            $this->router->put($this->route.'/{id}/templates/{templateId}', ['as' => $this->route.'.templates.putUpdate', 'uses' => function($id, $templateId) {
                return (new TemplatesController)
                    ->callAction("putUpdate", [new TemplateService, $id, $templateId]);
            }])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

            $this->router->put($this->route.'/{id}/templates/{templatePivotId}/pivot', ['as' => $this->route.'.templates.putUpdatePivot', 'uses' => function($id, $templatePivotId) {
                return (new TemplatesController)
                    ->callAction("putUpdatePivot", [new TemplateService, $id, $templatePivotId]);
            }])->where('id', '[0-9]+')->where('templatePivotId', '[0-9]+');

            $this->router->put($this->route.'/{id}/templates/{templateId}/sort', ['as' => $this->route.'.templates.putSort', 'uses' => function($id, $templateId) {
                return (new TemplatesController)
                    ->callAction("putSort", [new TemplateService, $id, $templateId]);
            }])->where('id', '[0-9]+')->where('templateId', '[0-9]+');

            $this->router->delete($this->route.'/{id}/templates/{templateId}', ['as' => $this->route.'.templates.delete', 'uses' => function($id, $templateId) {
                return (new TemplatesController)
                    ->callAction("delete", [new TemplateService, $id, $templateId]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/locales/create', ['as' => $this->route.'.locales.getCreate', 'uses' => function($id) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getCreate", [$id]);
            }])->where('id', '[0-9]+');

            $this->router->post($this->route.'/{id}/locales', ['as' => $this->route.'.locales.postStore', 'uses' => function($id) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("postStore", [new LocaleService, $id]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.getEdit', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getEdit", [$id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->put($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.putUpdate', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("putUpdate", [new LocaleService, $id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->delete($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.delete', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("delete", [new LocaleService, $id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route, ['as' => $this->route.'.getIndex', 'uses' => function() {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getIndex", []);
            }]);

            $this->router->get($this->route."/create", ['as' => $this->route.'.getCreate', 'uses' => function() {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getCreate", []);
            }]);

            $this->router->get($this->route."/tree", ['as' => $this->route.'.getTree', 'uses' => function() {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getTree", []);
            }]);

            $this->router->post($this->route, ['as' => $this->route.'.postStore', 'uses' => function() {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("postStore", [new ElementService]);
            }]);

            $this->router->get($this->route.'/{id}/edit', ['as' => $this->route.'.getEdit', 'uses' => function($id) {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getEdit", [$id]);
            }]);

            $this->router->get($this->route.'/{id}/{tab?}', ['as' => $this->route.'.getShow', 'uses' => function($id, $tab = null) {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("getShow", [$id, $tab]);
            }]);

            $this->router->put($this->route.'/{id}', ['as' => $this->route.'.putUpdate', 'uses' => function($id) {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("putUpdate", [new ElementService, $id]);
            }]);

            $this->router->put($this->route.'/{id}/sort', ['as' => $this->route.'.putSort', 'uses' => function($id) {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("putSort", [new ElementService, $id]);
            }]);

            $this->router->delete($this->route.'/{id}', ['as' => $this->route.'.delete', 'uses' => function($id) {
                return (new ElementsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setRootElement($this->rootElement)
                    ->callAction("delete", [new ElementService, $id]);
            }]);

        });
    }

}