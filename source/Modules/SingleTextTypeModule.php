<?php namespace Earth\Core\Modules;

use Earth\Core\Controllers\Module\Text\LocalesController;
use Earth\Core\Controllers\Module\Text\TextsController;
use Earth\Core\Services\Text\LocaleService;
use Earth\Core\Services\Text\TextService;

class SingleTextTypeModule extends CoreModule {

    /**
     * @var
     */
    protected $type;

    /**
     * @param $type
     * @return $this
     */
    public function setTextType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Register routes
     */
    public function register()
    {
        $this->router->group([
            'middleware' => ['web', 'earth.auth', 'earth.access'],
            'prefix' => config('earth.system_prefix'),
            'as' => config('earth.system_route_prefix').'.'
        ], function() {

            $this->router->get($this->route.'/{id}/locales/create', ['as' => $this->route.'.locales.getCreate', 'uses' => function($id) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getCreate", [$id]);
            }])->where('id', '[0-9]+');

            $this->router->post($this->route.'/{id}/locales', ['as' => $this->route.'.locales.postStore', 'uses' => function($id) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("postStore", [new LocaleService, $id]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.getEdit', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getEdit", [$id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->put($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.putUpdate', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("putUpdate", [new LocaleService, $id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->delete($this->route.'/{id}/locales/{localeId}', ['as' => $this->route.'.locales.delete', 'uses' => function($id, $localeId) {
                return (new LocalesController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("delete", [new LocaleService, $id, $localeId]);
            }])->where('id', '[0-9]+');

            $this->router->get($this->route, ['as' => $this->route.'.getIndex', 'uses' => function() {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getIndex", []);
            }]);

            $this->router->get($this->route."/create", ['as' => $this->route.'.getCreate', 'uses' => function() {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getCreate", []);
            }]);

            $this->router->post($this->route, ['as' => $this->route.'.postStore', 'uses' => function() {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("postStore", [new TextService]);
            }]);

            $this->router->get($this->route.'/{id}/edit', ['as' => $this->route.'.getEdit', 'uses' => function($id) {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getEdit", [$id]);
            }]);

            $this->router->get($this->route.'/{id}/{tab?}', ['as' => $this->route.'.getShow', 'uses' => function($id, $tab = null) {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("getShow", [$id, $tab]);
            }]);

            $this->router->put($this->route.'/{id}', ['as' => $this->route.'.putUpdate', 'uses' => function($id) {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("putUpdate", [new TextService, $id]);
            }]);

            $this->router->delete($this->route.'/{id}', ['as' => $this->route.'.delete', 'uses' => function($id) {
                return (new TextsController)
                    ->setTitle($this->title)
                    ->setRoute($this->route)
                    ->setType($this->type)
                    ->callAction("delete", [new TextService, $id]);
            }]);

        });
    }

}