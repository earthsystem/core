<?php namespace Earth\Core\ServiceProviders;

use Anhskohbo\NoCaptcha\NoCaptchaServiceProvider;
use Baum\Providers\BaumServiceProvider;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\SentinelServiceProvider;
use Codesleeve\LaravelStapler\Providers\L5ServiceProvider;
use Collective\Html\FormFacade;
use Collective\Html\HtmlFacade;
use Collective\Html\HtmlServiceProvider;
use Cviebrock\EloquentSluggable\SluggableServiceProvider;
use Earth\Core\Commands\InstallCommand;
use Earth\Core\Commands\PublishAssetsCommand;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use JildertMiedema\LaravelPlupload\Facades\Plupload;
use JildertMiedema\LaravelPlupload\LaravelPluploadServiceProvider;
use Spatie\Activitylog\ActivitylogFacade;
use Spatie\Activitylog\ActivitylogServiceProvider;

class CoreServiceProvider extends ServiceProvider {

    /**
     * Boot the service provider
     *
     * @return void
     */
    public function boot()
    {
        $rootPackagePath = __DIR__ . '/../..';

        // Load views
        $this->loadViewsFrom($rootPackagePath.'/resources/views', 'earth');

        // Load translations
        $this->loadTranslationsFrom($rootPackagePath.'/resources/lang', 'earth');

        // Publish the configuration file - only publish if a file does not already exist
        if ($configFilePath = config_path('earth.php') and !file_exists($configFilePath)) {
            $this->publishes([ $rootPackagePath.'/config/earth.php' => config_path('earth.php') ], 'earth-config');
        }

        // Publish assets
        $this->publishes([ $rootPackagePath.'/assets/' => public_path('vendor/earth/core') ], 'earth-assets');

        // Load PHP files
        $this->loadFiles();
    }

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerVendorServiceProviders();
        $this->registerVendorAliases();
        $this->registerCommands();
    }

    /**
     * Register vendor service providers
     */
    private function registerVendorServiceProviders()
    {
        $this->app->register(BaumServiceProvider::class);
        $this->app->register(HtmlServiceProvider::class);
        $this->app->register(SentinelServiceProvider::class);
        $this->app->register(LaravelPluploadServiceProvider::class);
        $this->app->register(L5ServiceProvider::class);
        $this->app->register(SluggableServiceProvider::class);
        $this->app->register(NoCaptchaServiceProvider::class);
        $this->app->register(ActivitylogServiceProvider::class);
    }

    /**
     * Register vendor aliases
     */
    private function registerVendorAliases()
    {
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('Activation', Activation::class);
            $loader->alias('Reminder', Reminder::class);
            $loader->alias('Sentinel', Sentinel::class);
            $loader->alias('Form', FormFacade::class);
            $loader->alias('Html', HtmlFacade::class);
            $loader->alias('Plupload', Plupload::class);
            $loader->alias('Activity', ActivitylogFacade::class);
        });
    }

    /**
     * Register Earth commands
     */
    private function registerCommands()
    {
        $this->app['earth.install'] = $this->app->share( function($app) {
            return new InstallCommand;
        });

        $this->app['earth.publish_assets'] = $this->app->share( function($app) {
            return new PublishAssetsCommand;
        });

        $this->commands([
            'earth.install',
            'earth.publish_assets'
        ]);
    }

    /**
     * Load files
     *
     * @return void
     */
    private function loadFiles()
    {
        // Include some files
        $rootSourcePath = __DIR__ . '/..';
        $files = ['helpers', 'routes', 'middleware', 'composers', 'website_helpers'];

        foreach($files as $file) {
            if (file_exists($rootSourcePath.'/'.$file.'.php')) include_once $rootSourcePath.'/'.$file.'.php';
        }

        // Website pathways
        if ($earthRoutes = app_path('Http/earthRoutes.php') and file_exists($earthRoutes)) include_once $earthRoutes;
    }

}