<?php namespace Earth\Core\ServiceProviders;

use Earth\Core\Services\RouteService;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

    /**
     * Register method
     */
    public function register()
    {
        $this->app->singleton('earthRouter', function() {
            return new RouteService;
        });
    }

}