<?php namespace Earth\Core\Middleware;

use Closure;
use Earth\Core\Entities\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Route;
use Sentinel;

class AccessMiddleware {

    /**
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        if (!session()->has("aliasUser")) {
            $user = User::find(Sentinel::getUser()->getUserId());
            session()->set("aliasUser", $user);
        }

        $route = Route::current();
        $prefix = $route->getPrefix();
        if (Str::contains($prefix, "/")) $prefix = str_replace("/", "", $prefix);

        $routeBase = str_replace('earth.' . $prefix . '.', '', $route->getName());
        $routeBase = explode(".", $routeBase);
        $routeBase = $routeBase[0];

        if ($routeBase == "home") {
            $element = element()->whereData($prefix)->whereIn('type', ['app'])->whereActive(1)->firstOrFail();
        } else {
            $element = element()->whereData($routeBase)->whereIn('type', ['link', 'link-hidden'])->whereActive(1)->firstOrFail();
        }

        view()->share('currentElement', $element);

        $access = false;
        if ($element->canRead()) $access = true;
        if ($element->accessDenied()) {
            $deny = true;
            $access = false;
        }
        $write = $element->canWrite();
        $destroy = $element->canDestroy();

        if (!$access) {

            $user = User::find( session("aliasUser")->id );
            $listOfApps = $user->getApplications();
            $routePrefix = "earth.".$prefix;

            if ($routeBase == "home") {
                return new Response( earthView("not_authorized")
                    ->with("listOfApps", $listOfApps) );
            } else {
                return new Response( earthView("not_authorized_full")
                    ->with("listOfApps", $listOfApps)
                    ->with("routePrefix", $routePrefix)
                );
            }

        }

        session()->forget("access");
        session()->put("access", [
            "read" => $access,
            "write" => $write,
            "destroy" => $destroy,
            "deny" => isset($deny)
        ]);

        return $next($request);
    }

}