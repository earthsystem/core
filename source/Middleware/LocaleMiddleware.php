<?php namespace Earth\Core\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Route;

class LocaleMiddleware {

    /**
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {

            $visitedDomain = request()->server('HTTP_HOST');

            // Find route
            if ( !$route = earthRouter()->getRouteInstance($visitedDomain) ) {
                throw new Exception("missing_route_for_this_domain");
            }

            // Find application
            $application = element()
                ->whereType('web-app')
                ->whereData($route->id)
                ->first(["id", "data", "lft", "name"]);

            if (!$application) throw new Exception("missing_application_for_this_domain");

            // Find default locale
            $defaultLocale = get_data($application, "default_locale");
            if (!$defaultLocale) throw new Exception("missing_default_locale_for_application");

            // Redirect to default locale, if there is none
            $currentRoute = Route::getCurrentRoute();
            if ( !$locale = $currentRoute->parameter("locale") ) {
                return redirect()->to($defaultLocale);
            }
            
            // Locale exists, let's check if it's in the published locales data
            $publishedLocales = get_data($application, "published_locales");
            $validLocales = [$defaultLocale];
            if ($publishedLocales) {
                $publishedLocales = explode(",", $publishedLocales);
                $validLocales = array_merge($publishedLocales, $validLocales);
            }

            // If not, redirect to the default locale
            if ( !in_array($locale, $validLocales) ) {
                return redirect()->to($defaultLocale);
            }

            // Find the "hidden" category
            $hiddenId = element()->whereParentId($application->id)
                ->whereData("hidden")
                ->whereActive(0)
                ->value("id");

            if ($hiddenId) {
                $application->hiddenId = $hiddenId;
            }

            app()->setLocale($locale);
            view()->share('locale', $locale);
            $application->locale = $locale;

            config()->set("application", $application);
            view()->share("application", $application);

            if (env('ANALYTICS_CODE') !== null and !isset($_COOKIE['hasSeenCookieNotification'])) {
                setcookie('hasSeenCookieNotification', 1, time() + (24*3600*30*6), "/");
            }

            return $next($request);
        } catch (Exception $e) {

            if ( getenv("APP_DEBUG") == true ) {
                abort(500, $e->getMessage());
            }

            // return a PageNotFound

        }
    }

}