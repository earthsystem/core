<?php namespace Earth\Core\Middleware;

use Closure;
use Earth\Core\Entities\User\User;
use Illuminate\Http\Request;
use Sentinel;

class AuthMiddleware {

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Sentinel::check()) {

            if (!session()->has('aliasUser')) {
                session()->put('aliasUser', User::find(Sentinel::getUser()->getUserId()));
            }

            return $next($request);

        }

        if ($request->ajax()) {
            return response('Unauthorized', 401);
        }

        $loginUri = route('earth.getLogin');

        if ($loginUri != $request->url()) {
            session()->flash('referer', $request->url());
        }

        return redirect()->to($loginUri);

    }

}