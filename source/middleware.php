<?php

// Web middleware
Route::middleware('earth.web', Earth\Core\Middleware\LocaleMiddleware::class);

// System middleware
Route::middleware('earth.auth', Earth\Core\Middleware\AuthMiddleware::class);
Route::middleware('earth.access', Earth\Core\Middleware\AccessMiddleware::class);