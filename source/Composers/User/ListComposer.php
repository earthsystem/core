<?php namespace Earth\Core\Composers\User;

use Earth\Core\Entities\User\User;

class ListComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $items = User::sort( request()->get('sort') )->search( request()->get('q'), ['first_name', 'last_name'] );
        $view->with( 'items', $items->paginate(12) );
    }

}