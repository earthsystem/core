<?php namespace Earth\Core\Composers\Text;

use Earth\Core\Entities\Text\Text;

class ListComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $items = Text::sort( request()->get('sort') )->search( request()->get('q'), ['name'] );

        if ( request()->has("type") or config()->has("textType") ) {
            $textType = request()->has("type") ? request()->get("type") : config("textType");
            $items = $items->whereType($textType);
        }

        $view->with( 'items', $items->paginate(10) );
    }

}