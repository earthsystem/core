<?php namespace Earth\Core\Composers;

use Earth\Core\Entities\Element\Collection;
use Earth\Core\Entities\User\User;
use Sentinel;

class HeaderComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $items = new Collection( element()->whereData('system')->first()->descendants()->whereIn('type', ['link', 'group'])->get()->toHierarchy() );
        $view->with('items', $items->showOnlyThoseUserCanAccess());

        $user = session('aliasUser');

        if (isset($user)) {
            if (method_exists($user, "getApplications")) $view->with('availableApps', $user->getApplications());
        }

        $loggedInUser = User::find(Sentinel::getUser()->getUserId());
        $view->with('loggedInUser', $loggedInUser);
    }

}