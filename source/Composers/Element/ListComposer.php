<?php namespace Earth\Core\Composers\Element;

use Earth\Core\Entities\Element\Collection;
use Route;

class ListComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $id = Route::current()->parameter("id");
        if ( !isset($id) ) {
            $id = Route::current()->parameter("elementId");
        }

        if (request()->has("q")) {
            $items = element()->where(function ($query) {
                return $query->search(request()->get('q'), ['name', 'data']);
            })->get();
            $items->each(function ($item) {
                $item->depth = 0;
            });
        } else if (config()->has("elementsRootElement")) {
            $rootElement = element()->find( config("elementsRootElement") );
            $items = element()->find( config("elementsRootElement") )->getDescendants();
            $items->each(function($item) use ($rootElement){
                $item->depth = $item->depth - $rootElement->depth;
            });
        } else {
            $items = element()->whereData('root')->first()->getDescendants();
        }

        if (request()->has("element_id") or isset($id)) {
            // Get all parent ID's to expand
            if (request()->has("element_id")) $id = request()->get("element_id");
            $element = element()->find($id);

            if (isset($element)) {
                $ancestors = element()->whereId($id)->first()->getAncestorsAndSelfWithoutRoot(['id']);
                $ancestors = $ancestors->pluck('id');
                $view->with('opened', $ancestors)->with('id', $id);
            } else {
                $view->with('opened', new Collection([]))->with('id', $id);
            }
        }

        $view->with('items', $items);
    }

}