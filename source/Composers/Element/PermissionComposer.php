<?php namespace Earth\Core\Composers\Element;

use Earth\Core\Entities\Element\Element;
use Earth\Core\Entities\Role\Role;
use Earth\Core\Entities\User\User;
use Route;

class PermissionComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $id = Route::current()->parameter('id');

        $fields = ['read', 'write', 'destroy', 'deny'];
        $fieldsWithData = [];

        foreach($fields as $field) {
            $fieldsWithData[$field] = $this->getData($id, $field);
        }

        $view->with('fields', $fieldsWithData);
    }

    /**
     * @param $id
     * @param $field
     * @return array|void
     */
    protected function getData($id, $field)
    {
        // Get all roles and just set an "active field to it"
        $users = User::get(['first_name', 'last_name', 'id']);
        $users->each( function ($user) use($id, $field) {
            $user->hasPermission = User::hasPermission($id, $field, Element::class)->whereId($user->id)->count();
        });

        $roles = Role::get(['name', 'id']);
        $roles->each( function ($role) use($id, $field) {
            $role->hasPermission = Role::hasPermission($id, $field, Element::class)->whereId($role->id)->count();
        });

        return [
            'users' => $users,
            'roles' => $roles
        ];
    }

}