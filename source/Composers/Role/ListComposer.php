<?php namespace Earth\Core\Composers\Role;

use Earth\Core\Entities\Role\Role;

class ListComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $items = Role::sort( request()->get('sort') )->search( request()->get('q'), ['name'] );
        $view->with( 'items', $items->paginate(10) );
    }

}