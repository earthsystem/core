<?php namespace Earth\Core\Composers\Web;

class NewsSidebarComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $textTypes = config('earth.website_news_text_types');

        $years = content_by_type($textTypes)->distinctYears()->latest()->get(['created_at']);
        $years->each( function($year) use($textTypes) {
            $year->months = content_by_type($textTypes)->distinctMonths($year)->latest()->get(['created_at']);
            return $year;
        });

        view()->share('years', $years);
    }

}