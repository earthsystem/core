<?php namespace Earth\Core\Composers\Web;

class PageComposer {

    public function compose($view)
    {
        $texts = content_in_category()->get();

        if ( !$texts->count() ) {
            redirect_to_first_child();
        }

        $view->with('texts', $texts);
    }

}