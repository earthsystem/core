<?php namespace Earth\Core\Composers\Web;

class NewsListComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $year = config('earth.year');
        $month = config('earth.month');
        $textTypes = config('earth.website_news_text_types');

        $texts = content_by_type($textTypes)->latest();

        if ( isset($year) ) $texts = $texts->isInYear($year);
        if ( isset($month) ) $texts = $texts->isInMonth($month);

        if ( request()->has("q") ) {
            $texts->searchByRelation( "locales", request()->get("q"), ["title"] );
        }

        view()->share('texts', $texts->paginate(10) );
    }

}