$(document).ready(function(){

    // Send ajax form
    $('body').on('submit', ".ajax-form", function(e) {

        e.preventDefault();

        var $form = $(this);
        var $button = $form.find("button[type=submit]");
        var $metaData = $form.find(".meta-data");
        var $refreshContainer = $metaData.data("refreshContainer");
        var $refreshUri = $metaData.data("refreshUri");
        var $pushState = $metaData.data("pushState");
        var formData = new FormData($form[0]);
        var validationPassed = validateForm($form);

        if (validationPassed) {

            if ($refreshContainer === undefined) {
                $refreshContainer = ".app";
                showLoadingScreen();
            } else {
                showLoadingScreen( $($refreshContainer) );
            }

            $button.button('loading');

            var $method = $form.attr("method");
            if ( $form.find("input[name=_method]").size() ) {
                $method = $form.find("input[name=_method]").val();
            }
            $method = $method == "get" ? "get" : "post"

            // IF method == get, then the url is the action plus any extra formData
            if ($method == "get") {
                var data = $form.serializeObject();
                formData = undefined;
                $url = $form.attr("action") + "&ajax=true&ajax_process=true&" + $.param(data);
                $originalUrl = $form.attr("action") + "&" + $.param(data);
            } else {
                $url = $form.attr("action");
            }

            // Trigger tinyMCE save button
            tinyMCE.triggerSave();

            $.ajax({
                type: $method,
                url: $url,
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {

                    var formResponse = data;
                    
                    if ($refreshContainer !== undefined && $refreshUri !== undefined && $method !== "get") {

                        /**
                         * POST form, make another AJAX call to load the content
                         */
                        $.ajax({
                            type: "get",
                            url: $refreshUri,
                            data: { "ajax": true },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                // Hide page loader, then display error notification
                                // If a custom refresh container, then hide that page loader
                                if ($refreshContainer == ".app") {
                                    hideLoadingScreen();
                                } else {
                                    hideLoadingScreen( $($refreshContainer) );
                                }

                                var responseMessage = parseResponseMessage(XMLHttpRequest);
                                generateErrorNotification(responseMessage);

                            },
                            success: function(data) {
                                $($refreshContainer).html(data);
                                dynamics();

                                // Hide page loader, then display success notification
                                // If a custom refresh container, then hide that page loader
                                if ($refreshContainer == ".app") {
                                    hideLoadingScreen();
                                } else {
                                    hideLoadingScreen( $($refreshContainer) );
                                }

                                if ($pushState === undefined) {
                                    history.pushState({ href: $refreshUri }, document.title, $refreshUri);
                                }

                                var responseMessage = parseResponseMessage(formResponse);
                                generateSuccessNotification(responseMessage);
                            }
                        })

                    } else {

                        /**
                         * Basically processing a GET form
                         */

                        $($refreshContainer).html(data);
                        dynamics();

                        if ($pushState === undefined && $originalUrl !== undefined) {
                            history.pushState({ href: $originalUrl }, document.title, $originalUrl);
                        }

                        // Hide page loader, then display error notification
                        // If a custom refresh container, then hide that page loader
                        if ($refreshContainer == ".app") {
                            hideLoadingScreen();
                        } else {
                            hideLoadingScreen( $($refreshContainer) );
                        }

                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $button.button("reset");

                    // Hide page loader, then display error notification
                    // If a custom refresh container, then hide that page loader
                    if ($refreshContainer == ".app") {
                        hideLoadingScreen();
                    } else {
                        hideLoadingScreen( $($refreshContainer) );
                    }

                    var responseMessage = parseResponseMessage(XMLHttpRequest);
                    generateErrorNotification(responseMessage);

                }
            })

        }

    });

    $("body").on("click", ".delete", function(e) {

        e.preventDefault();
        var $button = $(this);
        var $refreshContainer = $button.data("refreshContainer");
        var $refreshUri = $button.data("refreshUri");
        var $pushState = $button.data("pushState");

        var $title = $button.data("title");
        var $buttonText = $button.data("buttonText");
        var $cancelButtonText = $button.data("cancelButtonText");
        var $deletedTitle = $button.data("deletedTitle");
        var $notDeletedTitle = $button.data("notDeletedTitle");
        if ($title === undefined) $title = deleteAlertTitle;
        if ($buttonText === undefined) $buttonText = deleteAlertButtonText;
        if ($cancelButtonText === undefined) $cancelButtonText = deleteAlertCancelButtonText;
        if ($deletedTitle === undefined) $deletedTitle = deleteAlertItemDeletedTitle;
        if ($notDeletedTitle === undefined) $notDeletedTitle = deleteAlertItemNotDeletedTitle;

        if ($("#earthModal").hasClass("in")) {
            $("#earthModal").modal("hide").addClass("reopenAfterSwalClose");
        }

        swal({
            title: $title,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: $buttonText,
            cancelButtonText: $cancelButtonText,
            closeOnConfirm: false
        },
        function(isConfirm){

            if (!isConfirm) {
                if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                    $("#earthModal").modal("show");
                }
                return;
            }

            $(".sweet-alert button.confirm").button("loading");

            // Deletion
            $.ajax({
                type: "post",
                url: $button.attr("href"),
                data: { "_method": "DELETE", "_token": $button.data("csrfToken") },
                success: function(data) {

                    var formResponse = data;

                    $(".sweet-alert button.confirm").button('reset');
                    swal.close();

                    if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                        $("#earthModal").modal("show");
                    }

                    if ($refreshUri === undefined) {
                        return;
                    }

                    if ($refreshContainer === undefined) {
                        $refreshContainer = ".app";
                        showLoadingScreen();
                    } else {
                        showLoadingScreen( $($refreshContainer) );
                    }

                    $.ajax({
                        type: "get",
                        url: $refreshUri,
                        data: { "ajax": true },
                        success: function(data) {

                            $($refreshContainer).html(data);
                            dynamics();

                            if ($pushState === undefined) {
                                history.pushState({ href: $refreshUri }, document.title, $refreshUri);
                            }

                            if ($refreshContainer == ".app") {
                                hideLoadingScreen();
                            } else {
                                hideLoadingScreen( $($refreshContainer) );
                            }

                            generateSuccessNotification($deletedTitle);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if ($refreshContainer == ".app") {
                                hideLoadingScreen();
                            } else {
                                hideLoadingScreen( $($refreshContainer) );
                            }

                            var responseMessage = parseResponseMessage(XMLHttpRequest);
                            generateErrorNotification(responseMessage);
                        }
                    })

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // Error response
                    $(".sweet-alert button.confirm").button('reset');
                    swal.close();

                    if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                        $("#earthModal").modal("show");
                    }

                    generateErrorNotification($notDeletedTitle);
                }
            });

        });

    });

});