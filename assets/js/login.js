$(document).ready(function(){

    $("form.form-signin").submit(function(e){
        e.preventDefault();

        // Deal with submit button
        var $submitButton = $(this).find("button");
        var $originalText = $submitButton.html();
        $submitButton.button('loading');

        // Fields
        var $email = $(this).find("input[name=email]");
        var $password = $(this).find("input[name=password]");
        var $errorResponsePlaceholder = $(this).find(".errorResponsePlaceholder");
        $errorResponsePlaceholder.addClass("hidden");
        var $successResponsePlaceholder = $(this).find(".successResponsePlaceholder");
        $successResponsePlaceholder.addClass("hidden");

        if ($email.val() == "" || $password.val() == "") {
            $errorResponsePlaceholder.html($errorResponsePlaceholder.data('missingArgumentsText')).removeClass("hidden");
            $submitButton.button('reset');
            return;
        }

        var formData = new FormData($(this)[0]);

        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: formData,
            processData: false,
            contentType: false,
            error: function (XMLHttpRequest, textStatus, errorThrown) {

                var responseMessage = parseResponseMessage(XMLHttpRequest);
                if (responseMessage !== undefined) {
                    $errorResponsePlaceholder.html(responseMessage).removeClass("hidden");
                    $submitButton.button('reset');
                    return;
                }

                $errorResponsePlaceholder.html($errorResponsePlaceholder.data('errorProcessingSignInText')).removeClass("hidden");
                $submitButton.button('reset');
            },
            success: function(data) {
                $successResponsePlaceholder.removeClass("hidden");

                if (data.uri !== undefined) {
                    setTimeout(function() {
                        window.location.href = data.uri;
                    }, 1000);
                    return;
                }

                $errorResponsePlaceholder.html($errorResponsePlaceholder.data('errorProcessingSignInText')).removeClass("hidden");
                $submitButton.button('reset');
                return;
            }
        })

    });

});

/**
 * Parse error response message
 * @param request
 * @returns {*}
 */
function parseResponseMessage(request)
{
    if (request.responseText !== undefined && request.responseText != "") {
        try {
            var error = JSON.parse(request.responseText);
            if (error.message !== undefined) {
                return error.message;
            }
        } catch (e) {
            return null
        }
    }

    return null;
}