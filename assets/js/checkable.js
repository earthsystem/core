$(document).ready(function(){

    $("body").on("click", ".toggleCheckedInNearestTable", function(e) {
        var nearestTable = $(this).closest("table");
        var triggerDeletesButton = nearestTable.find(".triggerDeletesInNearestTable");

        if ($(this).prop("checked")) {
            triggerDeletesButton.removeClass('disabled');
            nearestTable.find(".selectCheckbox").prop("checked", true);
        } else {
            triggerDeletesButton.addClass('disabled');
            nearestTable.find(".selectCheckbox").prop("checked", false);
        }
    });

    $("body").on("click", ".selectCheckbox", function(e) {
        var nearestTable = $(this).closest("table");
        var triggerDeletesButton = nearestTable.find(".triggerDeletesInNearestTable");

        if ($(this).prop("checked")) {
            triggerDeletesButton.removeClass('disabled');
        } else {
            if (nearestTable.find(".selectCheckbox:checked").size() == 0) {
                triggerDeletesButton.addClass('disabled');
            }
        }
    });

    $("body").on("click", ".triggerDeletesInNearestTable", function(e) {
        e.preventDefault();

        var $button = $(this);
        var $nearestTable = $(this).closest("table");
        var $checked = $nearestTable.find(".selectCheckbox:checked").map(function(_, el) {
            return $(el).val();
        }).get().join(',');
        var $refreshUri = $button.data("refreshUri");
        var $refreshContainer = $button.data("refreshContainer");
        var $pushState = $button.data('pushState');
        var $title = deleteItemsAlertTitle;
        var $buttonText = deleteAlertButtonText;
        var $cancelButtonText = deleteAlertCancelButtonText;
        var $deletedTitle = $button.data("deletedTitle");
        var $notDeletedTitle = $button.data("notDeletedTitle");
        if ($deletedTitle === undefined) $deletedTitle = deleteAlertItemDeletedTitle;
        if ($notDeletedTitle === undefined) $notDeletedTitle = deleteAlertItemNotDeletedTitle;

        if ($("#earthModal").hasClass("in")) {
            $("#earthModal").modal("hide").addClass("reopenAfterSwalClose");
        }
        
        swal({
            title: $title,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: $buttonText,
            cancelButtonText: $cancelButtonText,
            closeOnConfirm: false
        }, function(isConfirm) {

            if (!isConfirm) {
                if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                    $("#earthModal").modal("show");
                }
                return;
            }

            $(".sweet-alert button.confirm").button("loading");
            var $href = $button.attr("href");
            $href = $href.replace(":id_placeholder", $checked);

            // Deletion
            $.ajax({
                type: "post",
                url: $href,
                data: { "_method": "DELETE", "_token": $button.data("csrfToken") },
                success: function(data) {

                    var formResponse = data;

                    $(".sweet-alert button.confirm").button('reset');
                    swal.close();

                    if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                        $("#earthModal").modal("show");
                    }

                    if ($refreshUri === undefined) {
                        return;
                    }

                    if ($refreshContainer === undefined) {
                        $refreshContainer = ".app";
                        showLoadingScreen();
                    } else {
                        showLoadingScreen( $($refreshContainer) );
                    }

                    $.ajax({
                        type: "get",
                        url: $refreshUri,
                        data: { "ajax": true },
                        success: function(data) {

                            $($refreshContainer).html(data);
                            dynamics();

                            if ($pushState === undefined) {
                                history.pushState({ href: $refreshUri }, document.title, $refreshUri);
                            }

                            if ($refreshContainer == ".app") {
                                hideLoadingScreen();
                            } else {
                                hideLoadingScreen( $($refreshContainer) );
                            }

                            generateSuccessNotification($deletedTitle);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if ($refreshContainer == ".app") {
                                hideLoadingScreen();
                            } else {
                                hideLoadingScreen( $($refreshContainer) );
                            }

                            var responseMessage = parseResponseMessage(XMLHttpRequest);
                            generateErrorNotification(responseMessage);
                        }
                    })

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // Error response
                    $(".sweet-alert button.confirm").button('reset');
                    swal.close();

                    if ($("#earthModal").hasClass("reopenAfterSwalClose")) {
                        $("#earthModal").modal("show");
                    }

                    generateErrorNotification($notDeletedTitle);
                }
            });
        });
        

    });
    
});