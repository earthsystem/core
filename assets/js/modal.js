$(document).ready(function(){

    /**
     * Open in modal
     */
    $('body').on('click', 'a[data-open-in-modal=true]', function(e){

        e.preventDefault();

        $("#earthModal .modal-title h4").html(modalTitle);
        $("#earthModal .modal-footer").html(modalCloseButton);
        $("#earthModal .modal-dialog").removeClass("modal-lg").removeClass("modal-sm"); // Remove previous classes

        var $href = $(this).attr("href");

        var $refreshType = $(this).attr('data-refresh-type');
        if ($refreshType === undefined) {
            $("#earthModal").attr('data-refresh-type', 'none');
        } else {
            $("#earthModal").attr('data-refresh-type', $refreshType);
        }

        var $refreshUri = $(this).attr('data-refresh-uri');
        if ($refreshUri !== undefined) {
            $("#earthModal").attr('data-refresh-uri', $refreshUri);
        }

        var $modalSize = $(this).attr('data-modal-size');
        if ($modalSize !== undefined) {
            $("#earthModal .modal-dialog").addClass($modalSize);
        }

        var $pushState = $(this).attr('data-push-state');
        if ($pushState !== undefined) {
            $("#earthModal .modal-dialog").attr('data-push-state', $pushState);
        }

        var $refreshContainer = $(this).attr('data-refresh-container');
        if ($refreshContainer !== undefined) {
            $("#earthModal").attr('data-refresh-container', $refreshContainer);
        } else {
            $("#earthModal").attr('data-refresh-container', '.app');
        }

        $("#earthModal").modal('show');
        $("#earthModal .modal-body").css({ textAlign: 'center' }).html('<i class="fa fa-spin fa-spinner fa-3x fa-fw"></i>');

        $.ajax({
            type: "get",
            url: $href,
            data: {},
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#earthModal .modal-header h4").html(modalErrorTitle);
                $("#earthModal .modal-body").html(modalErrorContent);
                $("#earthModal .modal-footer").html(modalCloseButton);
            },
            success: function (data) {
                $("#earthModal .modal-content").html(data);
                dynamics();
            }
        })

    });

    /**
     * Close modal and perform an action
     */
    $('body').on('click', '#earthModal .close-modal', function(e) {
        e.preventDefault();

        var $modal = $("#earthModal");
        var $refreshType = $modal.attr("data-refresh-type");
        var $refreshUri = $modal.attr("data-refresh-uri");
        var $refreshContainer = $modal.attr("data-refresh-container");
        var $pushState = $modal.attr("data-push-state");
        var $button = $(this);

        if ($refreshType == "hard") {
            if ($refreshUri !== undefined) {
                window.location.href = $refreshUri;
            } else {
                window.location.reload();
            }
            return;
        }

        if ($refreshType == "soft" && $refreshUri !== undefined) {

            $("#earthModal").modal('hide');

            if ($refreshContainer == ".app") {
                showLoadingScreen();
            } else {
                showLoadingScreen( $($refreshContainer) );
            }

            $.ajax({
                type: "get",
                url: $refreshUri,
                data: { "ajax": true },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // Hide page loader, then display error notification
                    // If a custom refresh container, then hide that page loader
                    if ($refreshContainer == ".app") {
                        hideLoadingScreen();
                    } else {
                        hideLoadingScreen( $($refreshContainer) );
                    }

                    var responseMessage = parseResponseMessage(XMLHttpRequest);
                    generateErrorNotification(responseMessage);

                },
                success: function(data) {
                    $($refreshContainer).html(data);
                    dynamics();

                    // Hide page loader, then display success notification
                    // If a custom refresh container, then hide that page loader
                    if ($refreshContainer == ".app") {
                        hideLoadingScreen();
                    } else {
                        hideLoadingScreen( $($refreshContainer) );
                    }

                    if ($pushState === undefined) {
                        history.pushState({ href: $refreshUri }, document.title, $refreshUri);
                    }
                }
            });

            return;
        }

        /**
         * No refresh, just close
         */
        $("#earthModal").modal('hide');
        return;
    });

    /**
     * Send form from a modal
     */
    $('body').on('click', '#earthModal #submit-modal-form', function(e) {
        e.preventDefault();

        var $modal = $("#earthModal");
        var $refreshType = $modal.attr("data-refresh-type");
        var $refreshUri = $modal.attr("data-refresh-uri");
        var $refreshContainer = $modal.attr("data-refresh-container");
        var $pushState = $modal.attr("data-push-state");
        var $button = $(this);
        var $form = $modal.find("form");
        var validationPassed = validateForm($form);

        if (validationPassed) {
            $button.button('loading');

            var $method = $form.attr("method");
            if ( $form.find("input[name=_method]").size() ) {
                $method = $form.find("input[name=_method]").val();
            }
            $method = $method == "get" ? "get" : "post"

            // TinyMCE Save
            tinyMCE.triggerSave();
            var formData = new FormData($form[0]);

            $.ajax({
                type: $method,
                url: $form.attr('action'),
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    var formResponse = data;

                    if ($refreshType == "hard") {

                        if (data.message !== undefined) {
                            var $alert = generateSuccessAlertDiv(data.message);
                            $("#earthModal .modal-body").html($alert);
                        }

                        setTimeout(function(){
                            if ($refreshUri !== undefined) {
                                window.location.href = $refreshUri;
                            } else {
                                window.location.reload();
                            }
                        }, 1000);
                        return;
                    }

                    if ($refreshType == "soft" && $refreshUri !== undefined) {

                        $("#earthModal").modal("hide");

                        if ($refreshContainer == ".app") {
                            showLoadingScreen();
                        } else {
                            showLoadingScreen( $($refreshContainer) );
                        }

                        $.ajax({
                            type: "get",
                            url: $refreshUri,
                            data: { "ajax": true },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                // Hide page loader, then display error notification
                                // If a custom refresh container, then hide that page loader
                                if ($refreshContainer == ".app") {
                                    hideLoadingScreen();
                                } else {
                                    hideLoadingScreen( $($refreshContainer) );
                                }

                                var responseMessage = parseResponseMessage(XMLHttpRequest);
                                generateErrorNotification(responseMessage);

                            },
                            success: function(data) {
                                $($refreshContainer).html(data);
                                dynamics();

                                // Hide page loader, then display success notification
                                // If a custom refresh container, then hide that page loader
                                if ($refreshContainer == ".app") {
                                    hideLoadingScreen();
                                } else {
                                    hideLoadingScreen( $($refreshContainer) );
                                }

                                if ($pushState === undefined) {
                                    history.pushState({ href: $refreshUri }, document.title, $refreshUri);
                                }

                                var responseMessage = parseResponseMessage(formResponse);
                                generateSuccessNotification(responseMessage);
                            }
                        });

                        return;
                    }

                    /**
                     * No refresh, just close
                     */
                    if (data.message !== undefined) {
                        var $alert = generateSuccessAlertDiv(data.message);
                        $("#earthModal .modal-body").html($alert);
                    }

                    setTimeout(function(){
                        $("#earthModal").modal('hide');
                    }, 1000);

                    return;

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    var responseMessage = parseResponseMessage(XMLHttpRequest);
                    if (responseMessage !== undefined) {
                        var $alert = generateErrorAlertDiv(responseMessage);
                        $("#earthModal .modal-body").prepend($alert);
                    }

                    $button.button("reset");
                }

            });
        }
    });

});