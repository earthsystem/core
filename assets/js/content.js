window.addEventListener('popstate', function(event) {
    if (window.history.state !== null) {
        if (window.history.state.href !== null) {
            $("#popstatePlaceholder").attr("href", window.history.state.href).click();
        }
    }

});

$(document).ready(function() {

    $('body').on("click", ".triggerPopstatePlaceholder", function(e){
        window.history.back();
    });

    $.LoadingOverlaySetup({
        fontawesome : "fa fa-circle-o-notch fa-spin",
        image: null
    });

    // Toggle content
    $('body').on("click", "a[data-toggle=content]", function(e) {

        e.preventDefault();

        var $href = $(this).attr("href");
        var $id = $(this).attr("id");
        var $refreshContainer = $(this).data("refreshContainer");
        var $pushState = $(this).data("pushState");
        var $showLoading = $(this).data("showLoading");
        var $loadDynamics = $(this).data("loadDynamics");

        if ($refreshContainer === undefined) {
            $refreshContainer = ".app";
            if ($showLoading === undefined) showLoadingScreen();
        } else {
            if ($showLoading === undefined) showLoadingScreen( $($refreshContainer) );
        }

        $.ajax({
            type: "GET",
            url: $href,
            data: { "ajax": true, "ajax_process": true },
            success: function(data) {

                $($refreshContainer).html(data);

                if ($loadDynamics === undefined) {
                    dynamics();
                }

                if ($pushState === undefined) {
                    history.pushState({ href: $href }, document.title, $href);
                }

                if ($refreshContainer == ".app") {
                    hideLoadingScreen();
                } else {
                    hideLoadingScreen( $($refreshContainer) );
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

                if ($refreshContainer == ".app") {
                    hideLoadingScreen();
                } else {
                    hideLoadingScreen( $($refreshContainer) );
                }

                var responseMessage = parseResponseMessage(XMLHttpRequest);
                generateErrorNotification(responseMessage);

            }
        })

    });

});

