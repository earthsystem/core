$(document).ready(function(){
    dynamics();
    toggleSidebar();
    dropdownSubmenuIsClickable();
});

/**
 * Dropdown submenu
 */
function dropdownSubmenuIsClickable()
{
    $( ".dropdown-submenu" ).click(function(event) {
        // stop bootstrap.js to hide the parents
        event.stopPropagation();
        // hide the open children
        $( this ).find(".dropdown-submenu").removeClass('open');
        // add 'open' class to all parents with class 'dropdown-submenu'
        $( this ).parents(".dropdown-submenu").addClass('open');
        // this is also open (or was)
        $( this ).toggleClass('open');
    });
}

/**
 * Toggle the sidebar
 * @param selector
 */
function toggleSidebar(selector)
{
    if (selector == null) {
        selector = ".toggle-sidebar";
    }

    $('body').on('click', selector, function(e) {
        e.preventDefault();
        $(".sidebar.toggleable").toggleClass("hidden-sm").toggleClass("hidden-xs");
    });
}

/**
 * Show loading screen
 * @param refreshContainer
 */
function showLoadingScreen(refreshContainer)
{
    if (refreshContainer !== undefined) {
        refreshContainer.LoadingOverlay("show");
        return;
    }

    $.LoadingOverlay("show");
}

/**
 * Hide loading screen
 * @param refreshContainer
 */
function hideLoadingScreen(refreshContainer)
{
    if (refreshContainer !== undefined) {
        refreshContainer.LoadingOverlay("hide");
        return;
    }

    $.LoadingOverlay("hide");
}

/**
 * Full editor
 * @param selector
 */
function initializeFullEditor(selector)
{
    if (selector == null) {
        selector = "textarea.wysiwyg";
    }

    if ($(window).width() < 768) {
        initializeSimpleEditor(selector);
    }

    tinymce.init({
        selector: selector,
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code | image media link fontawesome',
        plugins: [
            "fontawesome advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor codemirror imagetools image"
        ],
        codemirror: {
            path: "CodeMirror",
            indentOnInit: true
        },
        extended_valid_elements: 'span[class]',
        content_css: '//netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        image_advtab: true,
        convert_urls: false,
        relative_urls: false,
        file_browser_callback: function(field_name, url, type, win) {

            var field = win.document.getElementById(field_name);

            hideTinyMCEPanel();

            $.ajax({
                type: "get",
                url: fileManagerUrl + "?type=" + type + "&field_name=" + field_name,
                data: {},
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#earthModal .modal-header h4").html(modalErrorTitle);
                    $("#earthModal .modal-body").html(modalErrorContent);
                    $("#earthModal .modal-footer").html(modalCloseButton);
                    $("#earthModal").modal("show");

                    closeModalClick();
                },
                success: function (data) {
                    $("#earthModal .modal-content").html(data);
                    $("#earthModal .modal-dialog").addClass("modal-lg");
                    $("#earthModal").modal("show");
                    initializeFileInput();
                    closeModalClick();

                    $("body").on("click", "#earthModal .list-group .select-file", function(e){
                        e.preventDefault();
                        field.value = $(this).attr("href");
                        $("#earthModal").modal("hide");
                        showTinyMCEPanel();
                    });

                    $("#earthModal .modal-footer button").click(function(e){
                        e.preventDefault();

                        var $form = $("#earthModal form");
                        var formData = new FormData($form[0]);

                        // Do an ajax call for file upload, then process based on response
                        $.ajax({
                            type: "post",
                            url: $form.attr("action"),
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (data) {

                                if (data.value !== undefined) {
                                    field.value = data.value;
                                    $("#earthModal").modal("hide");
                                    showTinyMCEPanel();
                                }

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                var responseMessage = parseResponseMessage(XMLHttpRequest);
                                if (responseMessage !== undefined) {
                                    var $alert = generateErrorAlertDiv(responseMessage);
                                    $("#earthModal .modal-body").prepend($alert);
                                }

                            }
                        });

                    });
                }
            });

        },
        link_list: tinyMceElementLinkListUri
    });
}

/**
 * Close modal click
 */
function closeModalClick()
{
    $("#earthModal .modal-footer [data-dismiss=modal]").click(function(e){
        e.preventDefault();
        showTinyMCEPanel();
        $("#earthModal .modal-dialog").removeClass("modal-lg");
    });
}

/**
 * Hide TinyMCE panel
 */
function hideTinyMCEPanel()
{
    $(".mce-floatpanel").hide();
    $("#mce-modal-block").hide();
}

/**
 * Show TinyMCE panel
 */
function showTinyMCEPanel()
{
    $(".mce-floatpanel").show();
    $("#mce-modal-block").show();
}

/**
 * Simple editor
 * @param selector
 */
function initializeSimpleEditor(selector)
{
    if (selector == null) {
        selector = "textarea.wysiwygSimple";
    }

    tinymce.init({
        selector: selector,
        menubar: false,
        statusbar: false,
        toolbar: 'undo redo | bold italic | link',
        convert_urls: false,
        relative_urls: false
    });
}

/**
 * Popup (fancybox)
 * @param selector
 */
function initializePopup(selector)
{
    if (selector == null) {
        selector = ".popup";
    }

    $(selector).fancybox({
        padding: 0,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            thumbs: true
        }
    });
}

/**
 * Date picker
 * @param selector
 */
function initializeDatePicker(selector)
{
    if (selector == null) {
        selector = ".date-picker";
    }

    if ($(selector)[0]) {
        $(selector).datetimepicker({
            format: 'D.M.YYYY',
            keepOpen: true,
            useCurrent: false
        });
    }
}

/**
 * Time picker
 * @param selector
 */
function initializeTimePicker(selector)
{
    if (selector == null) {
        selector = ".time-picker";
    }

    if ($(selector)[0]) {
        $(selector).datetimepicker({
            format: 'HH:ss',
            keepOpen: true
        });
    }
}

/**
 * File input
 * @param selector
 */
function initializeFileInput(selector)
{
    if (selector == null) {
        selector = ".fileInput";
    }

    // initialize with defaults
    $(selector).fileinput({
        'showUpload': false,
        'showRemove': false,
        'previewSettings': {
            image: { width: '100%', height: 'auto' }
        }
    });
}

/**
 * Editable
 */
function initializeEditables()
{
    $.fn.editable.defaults.params = function (params) {
        params._token = $("meta[name=token]").attr("content");
        return params;
    };
    $(".editable-post").editable();
    $(".editable-put").editable({ ajaxOptions: { type: "put" } });
    $(".editable-select").editable({ ajaxOptions: { type: "put" }, type: "select" });
}

/**
 * Bootstrap toggle
 * @param selector
 */
function initializeBootstrapToggle(selector)
{
    if (selector == null) {
        selector = ".bsToggle";
    }

    $(selector).bootstrapToggle({
        onstyle: "success",
        size: "mini"
    });
    $(selector).on('change', function(){

        if (!$(this).hasClass("alreadyChecked")) {

            var $toggleable = $(this);
            var $putUpdate = $(this).data('putUpdate');

            if ($putUpdate !== undefined) {
                $toggleable.bootstrapToggle("disable").addClass("alreadyChecked");

                $.ajax({
                    type: "put",
                    url: $putUpdate,
                    data: {
                        value: $toggleable.prop("checked"),
                        name: $toggleable.attr("name"),
                        _token: $toggleable.attr("data-token")
                    },
                    success: function(data) {
                        $toggleable.removeClass("alreadyChecked").bootstrapToggle("enable");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var responseMessage = parseResponseMessage(XMLHttpRequest);
                        generateErrorNotification(responseMessage);

                        $toggleable.bootstrapToggle("toggle");
                        $toggleable.bootstrapToggle("enable");
                        $toggleable.removeClass("alreadyChecked");
                    }
                });
            }

        }

    });
}

/**
 * Selectize
 * @param selector
 */
function initializeSelectize(selector)
{
    if (selector == null) {
        selector = "select[data-selectize=true]";
    }

    $(selector).selectize({
        create: false,
        sortField: 'text'
    });
}

/**
 * Treegrid
 * @param selector
 */
function initializeTree(selector)
{
    if (selector == null) {
        selector = ".tree";
    }

    if ($(selector).size()) {
        $(selector).treegrid({
            initialState: 'collapsed'
        });
        if ($(selector).hasClass("sort-this")) {
            $(selector).find("tbody").addClass("sortable");
        }
        // $('.tree').find("tbody").addClass("selectable");
        $(selector + " .treegrid-opened").treegrid('expand');
    }
}

/**
 * Dynamic functions that load on every ajax request
 */
function dynamics()
{
    tinymce.remove();
    initializeFullEditor();
    initializeSimpleEditor();
    initializePopup();
    initializeDatePicker();
    initializeFileInput();
    initializeEditables();
    initializeBootstrapToggle();
    initializeSelectize();
    initializeTree();
    initializeSortable();

    // Bootstrap tooltip
    $('[data-tooltip="true"]').tooltip();

    // Hide or remove sidebar toggle
    if ($(".sidebar.toggleable").size()) {
        $(".toggle-sidebar").removeClass("hidden");
    } else {
        $(".toggle-sidebar").addClass("hidden");
    }

    /**
     * Sortable
     */

}

/**
 * Sortable
 * @param selector
 */
function initializeSortable(selector)
{
    if (selector == null) {
        selector = ".sortable";
    }

    if ($(selector).size()) {
        $(selector).sortable({
            placeholder: "placeholder",
            helper: function (e, tr) {
                var $helper = tr.clone();
                $helper.width(tr.width());
                return $helper;
            },
            update: function(event, ui) {
                var $item = ui.item;
                var $direction = (ui.originalPosition.top > ui.position.top) ? "left" : "right";
                var $newPosition = $item.index() + 1;
                var $sortable = $(this);
                var $itemId = ui.item.data("id");
                var $relItem = $direction == "left" ? ui.item.next() : ui.item.prev();
                var $relItemId = $relItem.data('id');
                var $href = $item.data("href");
                var $csrfToken = $item.data("csrfToken");
                var $sortableDiv = $item.closest(".sortable");
                var $shouldReloadTree = $item.data("shouldReloadTree");

                if ($direction !== undefined &&
                    $relItemId !== undefined &&
                    $href !== undefined &&
                    $relItemId != '' &&
                    $href != '') {

                    showLoadingScreen($sortableDiv);

                    $.ajax({
                        type: "put",
                        url: $href + "?ajax=true&ajax_process=true",
                        data: {
                            relItemId: $relItemId,
                            direction: $direction,
                            newPosition: $newPosition,
                            _token: $csrfToken
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var responseMessage = parseResponseMessage(XMLHttpRequest);
                            generateErrorNotification(responseMessage);
                            $sortable.sortable("cancel");
                            hideLoadingScreen($sortableDiv);
                        },
                        success: function (data) {
                            var responseMessage = parseResponseMessage(data);
                            generateSuccessNotification(responseMessage);

                            if ($shouldReloadTree !== undefined) {
                                $(".refreshElementsTreeList").click();
                            } else {
                                hideLoadingScreen($sortableDiv);
                            }
                        }
                    })

                } else {
                    generateErrorNotification(itemNotSortedTitle);
                    $sortable.sortable('cancel');
                }
            }
        });
    }
}

/**
 *
 * @param message
 */
function generateErrorNotification(message)
{
    if (message === null) message = errorNotificationTitle;

    $.notify({
        icon: 'fa fa-fw fa-exclamation-circle',
        message: message
    }, {
        delay: 2000,
        timer: 500,
        type: 'danger',
        allow_dismiss: false,
        z_index: 9999
    });
}

/**
 * Generate error alert div
 * @param html
 * @returns {*|jQuery|HTMLElement}
 */
function generateErrorAlertDiv(html)
{
    var div = $('<div>');
    var button = $('<button>');
    button.attr('type', 'button').addClass('close').attr('data-dismiss', 'alert').html('<span>&times;</span>');
    div.addClass("alert alert-danger alert-dismissable").html(html + "").prepend(button);
    return div;
}

/**
 *
 * @param message
 */
function generateSuccessNotification(message)
{
    if (message === null) message = successNotificationTitle;

    $.notify({
        icon: 'fa fa-fw fa-check',
        message: message
    }, {
        delay: 2000,
        timer: 500,
        type: 'success',
        allow_dismiss: false,
        z_index: 9999
    });
}

/**
 * Generate success alert div
 * @param html
 * @returns {*|jQuery|HTMLElement}
 */
function generateSuccessAlertDiv(html)
{
    var div = $('<div>');
    div.addClass("alert alert-success alert-dismissable").html("<i class='fa fa-spinner fa-spin fa-fw'></i> " + html);
    return div;
}

/**
 * Parse error response message
 * @param request
 * @returns {*}
 */
function parseResponseMessage(request)
{
    if (request.message !== undefined) {
        return request.message;
    }

    if (request.responseText !== undefined && request.responseText != "") {
        try {
            var response = JSON.parse(request.responseText);
            if (response.message !== undefined) {
                return response.message;
            }
        } catch (e) {
            return null
        }
    }

    return null;
}

/**
 * Validate a form
 * @param form
 * @returns {boolean}
 */
function validateForm(form)
{
    form.find(".form-group.has-error").removeClass("has-error"); // Remove has-error on all form-groups
    form.find("small.help-block").remove(); // Remove all help blocks
    form.removeClass("validation-not-passed");

    var data = form.serializeObject();
    var rules = form.find("input.form-rules");
    var validationPassed = false;

    if (rules.size() == 0) {
        validationPassed = true;
    } else {
        rules = $.parseJSON(rules.val());
        var validation = new Validator(data, rules, { required: thisFieldIsRequiredText });
        var preValidation = true;

        if (form.hasClass("require-file")) {
            var file = $(this).find("input[name=file]");
            if (file.val() == "") {
                preValidation = false;
                file.closest('.form-group').addClass('has-error');
            }
        }

        if (validation.passes() && preValidation) validationPassed = true;
    }

    if (!validationPassed) {
        form.addClass('validation-not-passed').find('.form-group input,select').each(function(){
            if (validation.errors.has($(this).attr("name"))) {
                $(this).closest(".form-group").addClass("has-error");
                var helpBlock = $("<small/>").addClass("help-block").html(validation.errors.first($(this).attr("name")));
                $(this).closest('.form-group').append(helpBlock);
            }
        });

        return false;
    }

    return true;
}

$.fn.serializeObject = function () {
    "use strict";

    var result = {};
    var extend = function (i, element) {
        var node = result[element.name];

        // If node with same name exists already, need to convert it to an array as it
        // is a multi-value field (i.e., checkboxes)

        if ('undefined' !== typeof node && node !== null) {
            if ($.isArray(node)) {
                node.push(element.value);
            } else {
                result[element.name] = [node, element.value];
            }
        } else {
            result[element.name] = element.value;
        }
    };

    $.each(this.serializeArray(), extend);
    return result;
};